-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:8889
-- Thời gian đã tạo: Th4 07, 2017 lúc 03:06 AM
-- Phiên bản máy phục vụ: 5.6.28
-- Phiên bản PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `thiet_bi_y_te`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_commentmeta`
--

INSERT INTO `wp_commentmeta` (`meta_id`, `comment_id`, `meta_key`, `meta_value`) VALUES
(1, 5, 'rating', '4'),
(2, 5, 'verified', '1');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-03-16 16:01:27', '2017-03-16 16:01:27', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 50, 'WooCommerce', 'woocommerce@tbyt.thang.dev', '', '', '2017-04-02 00:23:29', '2017-04-01 17:23:29', 'Thực hiện thanh toán khi giao hàng Trạng thái đơn hàng đã được chuyển từ Thanh toán chờ xử lý sang Đang xử lý.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(3, 52, 'WooCommerce', 'woocommerce@tbyt.thang.dev', '', '', '2017-04-02 09:25:51', '2017-04-02 02:25:51', 'Thực hiện thanh toán khi giao hàng Trạng thái đơn hàng đã được chuyển từ Thanh toán chờ xử lý sang Đang xử lý.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(4, 52, 'yte@tamduc', 'kqthang1505@gmail.com', '', '', '2017-04-02 09:26:21', '2017-04-02 02:26:21', 'Trạng thái đơn hàng đã được chuyển từ Đang xử lý sang Đã hoàn thành.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(5, 20, 'yte@tamduc', 'kqthang1505@gmail.com', '', '127.0.0.1', '2017-04-02 09:34:07', '2017-04-02 02:34:07', 'ssaassddsd', 0, 'post-trashed', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '', 0, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://thang.dev/thiet-bi-y-te/', 'yes'),
(2, 'home', 'http://thang.dev/thiet-bi-y-te/', 'yes'),
(3, 'blogname', 'Thiết bị y tế Tâm Đức', 'yes'),
(4, 'blogdescription', 'Healthy &amp; Beauty Care', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'kqthang1505@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'closed', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'd/m/Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:167:{s:24:"^wc-auth/v([1]{1})/(.*)?";s:63:"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]";s:22:"^wc-api/v([1-3]{1})/?$";s:51:"index.php?wc-api-version=$matches[1]&wc-api-route=/";s:24:"^wc-api/v([1-3]{1})(.*)?";s:61:"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]";s:7:"shop/?$";s:27:"index.php?post_type=product";s:37:"shop/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:32:"shop/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:24:"shop/page/([0-9]{1,})/?$";s:45:"index.php?post_type=product&paged=$matches[1]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:32:"category/(.+?)/wc-api(/(.*))?/?$";s:54:"index.php?category_name=$matches[1]&wc-api=$matches[3]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:29:"tag/([^/]+)/wc-api(/(.*))?/?$";s:44:"index.php?tag=$matches[1]&wc-api=$matches[3]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:47:"danh-muc/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:42:"danh-muc/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:23:"danh-muc/(.+?)/embed/?$";s:44:"index.php?product_cat=$matches[1]&embed=true";s:35:"danh-muc/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?product_cat=$matches[1]&paged=$matches[2]";s:17:"danh-muc/(.+?)/?$";s:33:"index.php?product_cat=$matches[1]";s:48:"tu-khoa/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:43:"tu-khoa/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:24:"tu-khoa/([^/]+)/embed/?$";s:44:"index.php?product_tag=$matches[1]&embed=true";s:36:"tu-khoa/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?product_tag=$matches[1]&paged=$matches[2]";s:18:"tu-khoa/([^/]+)/?$";s:33:"index.php?product_tag=$matches[1]";s:32:"shop/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:42:"shop/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:62:"shop/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:57:"shop/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:57:"shop/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:38:"shop/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:21:"shop/([^/]+)/embed/?$";s:40:"index.php?product=$matches[1]&embed=true";s:25:"shop/([^/]+)/trackback/?$";s:34:"index.php?product=$matches[1]&tb=1";s:45:"shop/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:40:"shop/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:33:"shop/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&paged=$matches[2]";s:40:"shop/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&cpage=$matches[2]";s:30:"shop/([^/]+)/wc-api(/(.*))?/?$";s:48:"index.php?product=$matches[1]&wc-api=$matches[3]";s:36:"shop/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:47:"shop/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:29:"shop/([^/]+)(?:/([0-9]+))?/?$";s:46:"index.php?product=$matches[1]&page=$matches[2]";s:21:"shop/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:31:"shop/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:51:"shop/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:46:"shop/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:46:"shop/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:27:"shop/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:38:"index.php?&page_id=2&cpage=$matches[1]";s:17:"wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:20:"order-pay(/(.*))?/?$";s:32:"index.php?&order-pay=$matches[2]";s:25:"order-received(/(.*))?/?$";s:37:"index.php?&order-received=$matches[2]";s:17:"orders(/(.*))?/?$";s:29:"index.php?&orders=$matches[2]";s:21:"view-order(/(.*))?/?$";s:33:"index.php?&view-order=$matches[2]";s:20:"downloads(/(.*))?/?$";s:32:"index.php?&downloads=$matches[2]";s:23:"edit-account(/(.*))?/?$";s:35:"index.php?&edit-account=$matches[2]";s:23:"edit-address(/(.*))?/?$";s:35:"index.php?&edit-address=$matches[2]";s:26:"payment-methods(/(.*))?/?$";s:38:"index.php?&payment-methods=$matches[2]";s:24:"lost-password(/(.*))?/?$";s:36:"index.php?&lost-password=$matches[2]";s:26:"customer-logout(/(.*))?/?$";s:38:"index.php?&customer-logout=$matches[2]";s:29:"add-payment-method(/(.*))?/?$";s:41:"index.php?&add-payment-method=$matches[2]";s:32:"delete-payment-method(/(.*))?/?$";s:44:"index.php?&delete-payment-method=$matches[2]";s:37:"set-default-payment-method(/(.*))?/?$";s:49:"index.php?&set-default-payment-method=$matches[2]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:26:"comments/wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:29:"search/(.+)/wc-api(/(.*))?/?$";s:42:"index.php?s=$matches[1]&wc-api=$matches[3]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:32:"author/([^/]+)/wc-api(/(.*))?/?$";s:52:"index.php?author_name=$matches[1]&wc-api=$matches[3]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:54:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:82:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:41:"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:66:"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:28:"([0-9]{4})/wc-api(/(.*))?/?$";s:45:"index.php?year=$matches[1]&wc-api=$matches[3]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:25:"(.?.+?)/wc-api(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&wc-api=$matches[3]";s:28:"(.?.+?)/order-pay(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&order-pay=$matches[3]";s:33:"(.?.+?)/order-received(/(.*))?/?$";s:57:"index.php?pagename=$matches[1]&order-received=$matches[3]";s:25:"(.?.+?)/orders(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&orders=$matches[3]";s:29:"(.?.+?)/view-order(/(.*))?/?$";s:53:"index.php?pagename=$matches[1]&view-order=$matches[3]";s:28:"(.?.+?)/downloads(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&downloads=$matches[3]";s:31:"(.?.+?)/edit-account(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-account=$matches[3]";s:31:"(.?.+?)/edit-address(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-address=$matches[3]";s:34:"(.?.+?)/payment-methods(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&payment-methods=$matches[3]";s:32:"(.?.+?)/lost-password(/(.*))?/?$";s:56:"index.php?pagename=$matches[1]&lost-password=$matches[3]";s:34:"(.?.+?)/customer-logout(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&customer-logout=$matches[3]";s:37:"(.?.+?)/add-payment-method(/(.*))?/?$";s:61:"index.php?pagename=$matches[1]&add-payment-method=$matches[3]";s:40:"(.?.+?)/delete-payment-method(/(.*))?/?$";s:64:"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]";s:45:"(.?.+?)/set-default-payment-method(/(.*))?/?$";s:69:"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]";s:31:".?.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:25:"([^/]+)/wc-api(/(.*))?/?$";s:45:"index.php?name=$matches[1]&wc-api=$matches[3]";s:31:"[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:5:{i:0;s:36:"contact-form-7/wp-contact-form-7.php";i:1;s:33:"custom-widgets/custom-widgets.php";i:2;s:21:"flamingo/flamingo.php";i:3;s:35:"redux-framework/redux-framework.php";i:4;s:27:"woocommerce/woocommerce.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '7', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:4:{i:0;s:84:"/Users/thang/projects/php/thiet-bi-y-te/wp-content/themes/storefront-child/style.css";i:2;s:84:"/Applications/MAMP/htdocs/thiet-bi-y-te/wp-content/themes/storefront-child/style.css";i:3;s:111:"/Applications/MAMP/htdocs/thiet-bi-y-te/wp-content/plugins/theme-customisations-master/theme-customisations.php";i:4;s:0:"";}', 'no'),
(40, 'template', 'storefront', 'yes'),
(41, 'stylesheet', 'storefront-child', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '7', 'yes'),
(84, 'page_on_front', '2', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '66', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:131:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:8:"customer";a:2:{s:4:"name";s:8:"Customer";s:12:"capabilities";a:1:{s:4:"read";b:1;}}s:12:"shop_manager";a:2:{s:4:"name";s:12:"Shop Manager";s:12:"capabilities";a:110:{s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:4:"read";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:10:"edit_users";b:1;s:10:"edit_posts";b:1;s:10:"edit_pages";b:1;s:20:"edit_published_posts";b:1;s:20:"edit_published_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:17:"edit_others_posts";b:1;s:17:"edit_others_pages";b:1;s:13:"publish_posts";b:1;s:13:"publish_pages";b:1;s:12:"delete_posts";b:1;s:12:"delete_pages";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:19:"delete_others_posts";b:1;s:19:"delete_others_pages";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:17:"moderate_comments";b:1;s:15:"unfiltered_html";b:1;s:12:"upload_files";b:1;s:6:"export";b:1;s:6:"import";b:1;s:10:"list_users";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:3:{s:5:"title";s:9:"Tin tức";s:6:"number";i:3;s:9:"show_date";b:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:9:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:5:{i:0;s:32:"woocommerce_product_categories-2";i:1;s:26:"woocommerce_price_filter-2";i:2;s:14:"recent-posts-2";i:3;s:22:"woocommerce_products-2";i:4;s:15:"policy_widget-5";}s:8:"header-1";a:1:{i:0;s:15:"slider_widget-2";}s:8:"footer-1";a:0:{}s:8:"footer-2";a:0:{}s:8:"footer-3";a:0:{}s:8:"footer-4";a:0:{}s:12:"footer-bar-1";a:1:{i:0;s:18:"wp_editor_widget-2";}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'cron', 'a:10:{i:1491530635;a:1:{s:32:"woocommerce_cancel_unpaid_orders";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:2:{s:8:"schedule";b:0;s:4:"args";a:0:{}}}}i:1491537688;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1491570235;a:1:{s:28:"woocommerce_cleanup_sessions";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1491580910;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1491583113;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1491584400;a:1:{s:27:"woocommerce_scheduled_sales";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1491586259;a:1:{s:24:"grunion_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1491613435;a:1:{s:30:"woocommerce_tracker_send_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1493683200;a:1:{s:25:"woocommerce_geoip_updater";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:7:"monthly";s:4:"args";a:0:{}s:8:"interval";i:2635200;}}}s:7:"version";i:2;}', 'yes'),
(105, 'nonce_key', '4I;r`Hel<G7ly;Es5nTd{0>FjD@[+/p]nv@M9?%i2V^0i1uw(!,31V.P;;!*J88u', 'no'),
(106, 'nonce_salt', '*%l,%Ty_ec`!_@9nF^x)<^EeO8b{my2_uvUoaAeenu?hm;%3P4NEk{8#7o!OOShr', 'no'),
(107, 'theme_mods_twentyseventeen', 'a:3:{s:18:"custom_css_post_id";i:-1;s:18:"nav_menu_locations";a:2:{s:7:"primary";i:18;s:3:"top";i:18;}s:16:"sidebars_widgets";a:2:{s:4:"time";i:1491131645;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:6:{i:0;s:15:"slider_widget-2";i:1;s:26:"woocommerce_price_filter-2";i:2;s:32:"woocommerce_product_categories-2";i:3;s:22:"woocommerce_products-2";i:4;s:18:"wp_editor_widget-2";i:5;s:18:"wp_editor_widget-3";}s:9:"sidebar-1";a:1:{i:0;s:14:"recent-posts-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(110, 'auth_key', '(H[J]dfC!~hju>Y,u(J{A7[]xlEn+Y}3k?|+7>;kP5$l:a>YbC)k5MC!v.eU|^Di', 'no'),
(111, 'auth_salt', '%!BGF<G]_t+I#E dqvj8geg.hY+~-mhx8>JGw{&O./:}U#H{~?5F52[KmUBF[7Bk', 'no'),
(112, 'logged_in_key', 'g>eefe5^<a+20|AI(=[Xh^7(a,aNP{aBjs=OnS_UrT2vGj8i/iIef_eq]}UbwtJ^', 'no'),
(113, 'logged_in_salt', '}i:@~JO[p,>c<eU}E&.Vl@zF:c#2UVLsy~t0$;8TZ4WG/@$PF4#x~UyZt6R/E`R;', 'no'),
(121, '_site_transient_timeout_browser_9bc134e93ca434cc48bb120cdc5e1606', '1490284911', 'no'),
(122, '_site_transient_browser_9bc134e93ca434cc48bb120cdc5e1606', 'a:9:{s:8:"platform";s:9:"Macintosh";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"56.0.2924.87";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'no'),
(124, 'can_compress_scripts', '1', 'no'),
(141, 'recently_activated', 'a:8:{s:47:"storefront-footer-bar/storefront-footer-bar.php";i:1491523957;s:37:"wp-editor-widget/wp-editor-widget.php";i:1491498084;s:24:"wpforms-lite/wpforms.php";i:1491447681;s:19:"jetpack/jetpack.php";i:1491446783;s:75:"storefront-homepage-contact-section/storefront-homepage-contact-section.php";i:1491446770;s:51:"wp-limit-login-attempts/wp-limit-login-attempts.php";i:1491311885;s:52:"theme-customisations-master/theme-customisations.php";i:1491233111;s:57:"storefront-product-sharing/storefront-product-sharing.php";i:1491066033;}', 'yes'),
(146, 'woocommerce_default_country', 'VN', 'yes'),
(147, 'woocommerce_allowed_countries', 'specific', 'yes'),
(148, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(149, 'woocommerce_specific_allowed_countries', 'a:1:{i:0;s:2:"VN";}', 'yes'),
(150, 'woocommerce_ship_to_countries', '', 'yes'),
(151, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(152, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(153, 'woocommerce_calc_taxes', 'no', 'yes'),
(154, 'woocommerce_demo_store', 'no', 'yes'),
(155, 'woocommerce_demo_store_notice', 'This is a demo store for testing purposes &mdash; no orders shall be fulfilled.', 'no'),
(156, 'woocommerce_currency', 'VND', 'yes'),
(157, 'woocommerce_currency_pos', 'right', 'yes'),
(158, 'woocommerce_price_thousand_sep', '.', 'yes'),
(159, 'woocommerce_price_decimal_sep', ',', 'yes'),
(160, 'woocommerce_price_num_decimals', '2', 'yes'),
(161, 'woocommerce_weight_unit', 'kg', 'yes'),
(162, 'woocommerce_dimension_unit', 'cm', 'yes'),
(163, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(164, 'woocommerce_review_rating_required', 'yes', 'no'),
(165, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(166, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(167, 'woocommerce_shop_page_id', '4', 'yes'),
(168, 'woocommerce_shop_page_display', '', 'yes'),
(169, 'woocommerce_category_archive_display', '', 'yes'),
(170, 'woocommerce_default_catalog_orderby', 'menu_order', 'yes'),
(171, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(172, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(173, 'shop_catalog_image_size', 'a:3:{s:5:"width";s:3:"300";s:6:"height";s:3:"300";s:4:"crop";i:1;}', 'yes'),
(174, 'shop_single_image_size', 'a:3:{s:5:"width";s:3:"600";s:6:"height";s:3:"600";s:4:"crop";i:1;}', 'yes'),
(175, 'shop_thumbnail_image_size', 'a:3:{s:5:"width";s:3:"180";s:6:"height";s:3:"180";s:4:"crop";i:1;}', 'yes'),
(176, 'woocommerce_enable_lightbox', 'yes', 'yes'),
(177, 'woocommerce_manage_stock', 'yes', 'yes'),
(178, 'woocommerce_hold_stock_minutes', '60', 'no'),
(179, 'woocommerce_notify_low_stock', 'yes', 'no'),
(180, 'woocommerce_notify_no_stock', 'yes', 'no'),
(181, 'woocommerce_stock_email_recipient', 'kqthang1505@gmail.com', 'no'),
(182, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(183, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(184, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(185, 'woocommerce_stock_format', '', 'yes'),
(186, 'woocommerce_file_download_method', 'force', 'no'),
(187, 'woocommerce_downloads_require_login', 'no', 'no'),
(188, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(189, 'woocommerce_prices_include_tax', 'no', 'yes'),
(190, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(191, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(192, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(193, 'woocommerce_tax_classes', 'Reduced Rate\nZero Rate', 'yes'),
(194, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(195, 'woocommerce_tax_display_cart', 'excl', 'no'),
(196, 'woocommerce_price_display_suffix', '', 'yes'),
(197, 'woocommerce_tax_total_display', 'itemized', 'no'),
(198, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(199, 'woocommerce_shipping_cost_requires_address', 'no', 'no'),
(200, 'woocommerce_ship_to_destination', 'billing', 'no'),
(201, 'woocommerce_enable_coupons', 'yes', 'yes'),
(202, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(203, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(204, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(205, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(206, 'woocommerce_cart_page_id', '5', 'yes'),
(207, 'woocommerce_checkout_page_id', '6', 'yes'),
(208, 'woocommerce_terms_page_id', '', 'no'),
(209, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(210, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(211, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(212, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(213, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(214, 'woocommerce_myaccount_page_id', '2', 'yes'),
(215, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(216, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(217, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no'),
(218, 'woocommerce_registration_generate_username', 'yes', 'no'),
(219, 'woocommerce_registration_generate_password', 'no', 'no'),
(220, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(221, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(222, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(223, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(224, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(225, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(226, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(227, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(228, 'woocommerce_email_from_name', 'Thiết bị y tế Tâm Đức', 'no'),
(229, 'woocommerce_email_from_address', 'kqthang1505@gmail.com', 'no'),
(230, 'woocommerce_email_header_image', '', 'no'),
(231, 'woocommerce_email_footer_text', 'Thiết bị y tế Tâm Đức', 'no'),
(232, 'woocommerce_email_base_color', '#248de9', 'no'),
(233, 'woocommerce_email_background_color', '#f5f5f5', 'no'),
(234, 'woocommerce_email_body_background_color', '#fdfdfd', 'no'),
(235, 'woocommerce_email_text_color', '#505050', 'no'),
(236, 'woocommerce_api_enabled', 'yes', 'yes'),
(242, 'woocommerce_admin_notices', 'a:0:{}', 'yes'),
(244, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(245, 'widget_woocommerce_widget_cart', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(246, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(247, 'widget_woocommerce_layered_nav', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(248, 'widget_woocommerce_price_filter', 'a:2:{i:2;a:1:{s:5:"title";s:4:"Giá";}s:12:"_multiwidget";i:1;}', 'yes'),
(249, 'widget_woocommerce_product_categories', 'a:2:{i:2;a:7:{s:5:"title";s:23:"Danh mục sản phẩm";s:7:"orderby";s:4:"name";s:8:"dropdown";i:0;s:5:"count";i:0;s:12:"hierarchical";i:1;s:18:"show_children_only";i:0;s:10:"hide_empty";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(250, 'widget_woocommerce_product_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(251, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(252, 'widget_woocommerce_products', 'a:2:{i:2;a:7:{s:5:"title";s:24:"Sản phẩm nổi bật";s:6:"number";i:4;s:4:"show";s:8:"featured";s:7:"orderby";s:5:"price";s:5:"order";s:4:"desc";s:9:"hide_free";i:0;s:11:"show_hidden";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(253, 'widget_woocommerce_rating_filter', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(254, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(255, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(256, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(260, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(268, 'woocommerce_paypal-ec_settings', 'a:1:{s:7:"enabled";s:2:"no";}', 'yes'),
(269, 'woocommerce_stripe_settings', 'a:1:{s:7:"enabled";s:2:"no";}', 'yes'),
(270, 'woocommerce_paypal_settings', 'a:2:{s:7:"enabled";s:2:"no";s:5:"email";s:21:"kqthang1505@gmail.com";}', 'yes'),
(271, 'woocommerce_cheque_settings', 'a:1:{s:7:"enabled";s:2:"no";}', 'yes'),
(272, 'woocommerce_bacs_settings', 'a:5:{s:7:"enabled";s:2:"no";s:5:"title";s:20:"Direct bank transfer";s:11:"description";s:314:"Thực hiện thanh toán vào ngay tài khoản ngân hàng của chúng tôi. Vui lòng sử dụng ID Đơn hàng của bạn như một phần để tham khảo khi thanh toán. Đơn hàng của bạn sẽ không được vận chuyển cho tới khi tiền được gửi vào tài khoản của chúng tôi.";s:12:"instructions";s:0:"";s:15:"account_details";s:0:"";}', 'yes'),
(273, 'woocommerce_cod_settings', 'a:1:{s:7:"enabled";s:3:"yes";}', 'yes'),
(274, '_transient_shipping-transient-version', '1489682313', 'yes'),
(275, '_transient_timeout_wc_shipping_method_count_0_1489682313', '1492274313', 'no'),
(276, '_transient_wc_shipping_method_count_0_1489682313', '0', 'no'),
(278, '_transient_product_query-transient-version', '1491314162', 'yes'),
(279, '_transient_product-transient-version', '1491314162', 'yes'),
(282, '_site_transient_timeout_wporg_theme_feature_list', '1489693354', 'no'),
(283, '_site_transient_wporg_theme_feature_list', 'a:3:{s:6:"Layout";a:7:{i:0;s:11:"grid-layout";i:1;s:10:"one-column";i:2;s:11:"two-columns";i:3;s:13:"three-columns";i:4;s:12:"four-columns";i:5;s:12:"left-sidebar";i:6;s:13:"right-sidebar";}s:8:"Features";a:20:{i:0;s:19:"accessibility-ready";i:1;s:10:"buddypress";i:2;s:17:"custom-background";i:3;s:13:"custom-colors";i:4;s:13:"custom-header";i:5;s:11:"custom-menu";i:6;s:12:"editor-style";i:7;s:21:"featured-image-header";i:8;s:15:"featured-images";i:9;s:15:"flexible-header";i:10;s:14:"footer-widgets";i:11;s:20:"front-page-post-form";i:12;s:19:"full-width-template";i:13;s:12:"microformats";i:14;s:12:"post-formats";i:15;s:20:"rtl-language-support";i:16;s:11:"sticky-post";i:17;s:13:"theme-options";i:18;s:17:"threaded-comments";i:19;s:17:"translation-ready";}s:7:"Subject";a:9:{i:0;s:4:"blog";i:1;s:10:"e-commerce";i:2;s:9:"education";i:3;s:13:"entertainment";i:4;s:14:"food-and-drink";i:5;s:7:"holiday";i:6;s:4:"news";i:7;s:11:"photography";i:8;s:9:"portfolio";}}', 'no'),
(286, 'current_theme', 'Storefront Child', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(287, 'theme_mods_storefront', 'a:10:{i:0;b:0;s:17:"storefront_styles";s:4645:"\n			.main-navigation ul li a,\n			.site-title a,\n			ul.menu li a,\n			.site-branding h1 a,\n			.site-footer .storefront-handheld-footer-bar a:not(.button),\n			button.menu-toggle,\n			button.menu-toggle:hover {\n				color: #d5d9db;\n			}\n\n			button.menu-toggle,\n			button.menu-toggle:hover {\n				border-color: #d5d9db;\n			}\n\n			.main-navigation ul li a:hover,\n			.main-navigation ul li:hover > a,\n			.site-title a:hover,\n			a.cart-contents:hover,\n			.site-header-cart .widget_shopping_cart a:hover,\n			.site-header-cart:hover > li > a,\n			.site-header ul.menu li.current-menu-item > a {\n				color: #ffffff;\n			}\n\n			table th {\n				background-color: #f8f8f8;\n			}\n\n			table tbody td {\n				background-color: #fdfdfd;\n			}\n\n			table tbody tr:nth-child(2n) td {\n				background-color: #fbfbfb;\n			}\n\n			.site-header,\n			.secondary-navigation ul ul,\n			.main-navigation ul.menu > li.menu-item-has-children:after,\n			.secondary-navigation ul.menu ul,\n			.storefront-handheld-footer-bar,\n			.storefront-handheld-footer-bar ul li > a,\n			.storefront-handheld-footer-bar ul li.search .site-search,\n			button.menu-toggle,\n			button.menu-toggle:hover {\n				background-color: #2c2d33;\n			}\n\n			p.site-description,\n			.site-header,\n			.storefront-handheld-footer-bar {\n				color: #9aa0a7;\n			}\n\n			.storefront-handheld-footer-bar ul li.cart .count,\n			button.menu-toggle:after,\n			button.menu-toggle:before,\n			button.menu-toggle span:before {\n				background-color: #d5d9db;\n			}\n\n			.storefront-handheld-footer-bar ul li.cart .count {\n				color: #2c2d33;\n			}\n\n			.storefront-handheld-footer-bar ul li.cart .count {\n				border-color: #2c2d33;\n			}\n\n			h1, h2, h3, h4, h5, h6 {\n				color: #484c51;\n			}\n\n			.widget h1 {\n				border-bottom-color: #484c51;\n			}\n\n			body,\n			.secondary-navigation a,\n			.onsale,\n			.pagination .page-numbers li .page-numbers:not(.current), .woocommerce-pagination .page-numbers li .page-numbers:not(.current) {\n				color: #43454b;\n			}\n\n			.widget-area .widget a,\n			.hentry .entry-header .posted-on a,\n			.hentry .entry-header .byline a {\n				color: #75777d;\n			}\n\n			a  {\n				color: #96588a;\n			}\n\n			a:focus,\n			.button:focus,\n			.button.alt:focus,\n			.button.added_to_cart:focus,\n			.button.wc-forward:focus,\n			button:focus,\n			input[type="button"]:focus,\n			input[type="reset"]:focus,\n			input[type="submit"]:focus {\n				outline-color: #96588a;\n			}\n\n			button, input[type="button"], input[type="reset"], input[type="submit"], .button, .added_to_cart, .widget a.button, .site-header-cart .widget_shopping_cart a.button {\n				background-color: #96588a;\n				border-color: #96588a;\n				color: #ffffff;\n			}\n\n			button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, .button:hover, .added_to_cart:hover, .widget a.button:hover, .site-header-cart .widget_shopping_cart a.button:hover {\n				background-color: #7d3f71;\n				border-color: #7d3f71;\n				color: #ffffff;\n			}\n\n			button.alt, input[type="button"].alt, input[type="reset"].alt, input[type="submit"].alt, .button.alt, .added_to_cart.alt, .widget-area .widget a.button.alt, .added_to_cart, .pagination .page-numbers li .page-numbers.current, .woocommerce-pagination .page-numbers li .page-numbers.current, .widget a.button.checkout {\n				background-color: #2c2d33;\n				border-color: #2c2d33;\n				color: #ffffff;\n			}\n\n			button.alt:hover, input[type="button"].alt:hover, input[type="reset"].alt:hover, input[type="submit"].alt:hover, .button.alt:hover, .added_to_cart.alt:hover, .widget-area .widget a.button.alt:hover, .added_to_cart:hover, .widget a.button.checkout:hover {\n				background-color: #13141a;\n				border-color: #13141a;\n				color: #ffffff;\n			}\n\n			#comments .comment-list .comment-content .comment-text {\n				background-color: #f8f8f8;\n			}\n\n			.site-footer {\n				background-color: #f0f0f0;\n				color: #61656b;\n			}\n\n			.site-footer a:not(.button) {\n				color: #2c2d33;\n			}\n\n			.site-footer h1, .site-footer h2, .site-footer h3, .site-footer h4, .site-footer h5, .site-footer h6 {\n				color: #494c50;\n			}\n\n			#order_review,\n			#payment .payment_methods > li .payment_box {\n				background-color: #ffffff;\n			}\n\n			#payment .payment_methods > li {\n				background-color: #fafafa;\n			}\n\n			#payment .payment_methods > li:hover {\n				background-color: #f5f5f5;\n			}\n\n			@media screen and ( min-width: 768px ) {\n				.secondary-navigation ul.menu a:hover {\n					color: #b3b9c0;\n				}\n\n				.secondary-navigation ul.menu a {\n					color: #9aa0a7;\n				}\n\n				.site-header-cart .widget_shopping_cart,\n				.main-navigation ul.menu ul.sub-menu,\n				.main-navigation ul.nav-menu ul.children {\n					background-color: #24252b;\n				}\n			}";s:29:"storefront_woocommerce_styles";s:2233:"\n			a.cart-contents,\n			.site-header-cart .widget_shopping_cart a {\n				color: #d5d9db;\n			}\n\n			table.cart td.product-remove,\n			table.cart td.actions {\n				border-top-color: #ffffff;\n			}\n\n			.woocommerce-tabs ul.tabs li.active a,\n			ul.products li.product .price,\n			.onsale,\n			.widget_search form:before,\n			.widget_product_search form:before {\n				color: #43454b;\n			}\n\n			.woocommerce-breadcrumb a,\n			a.woocommerce-review-link,\n			.product_meta a {\n				color: #75777d;\n			}\n\n			.onsale {\n				border-color: #43454b;\n			}\n\n			.star-rating span:before,\n			.quantity .plus, .quantity .minus,\n			p.stars a:hover:after,\n			p.stars a:after,\n			.star-rating span:before,\n			#payment .payment_methods li input[type=radio]:first-child:checked+label:before {\n				color: #96588a;\n			}\n\n			.widget_price_filter .ui-slider .ui-slider-range,\n			.widget_price_filter .ui-slider .ui-slider-handle {\n				background-color: #96588a;\n			}\n\n			.woocommerce-breadcrumb,\n			#reviews .commentlist li .comment_container {\n				background-color: #f8f8f8;\n			}\n\n			.order_details {\n				background-color: #f8f8f8;\n			}\n\n			.order_details > li {\n				border-bottom: 1px dotted #e3e3e3;\n			}\n\n			.order_details:before,\n			.order_details:after {\n				background: -webkit-linear-gradient(transparent 0,transparent 0),-webkit-linear-gradient(135deg,#f8f8f8 33.33%,transparent 33.33%),-webkit-linear-gradient(45deg,#f8f8f8 33.33%,transparent 33.33%)\n			}\n\n			p.stars a:before,\n			p.stars a:hover~a:before,\n			p.stars.selected a.active~a:before {\n				color: #43454b;\n			}\n\n			p.stars.selected a.active:before,\n			p.stars:hover a:before,\n			p.stars.selected a:not(.active):before,\n			p.stars.selected a.active:before {\n				color: #96588a;\n			}\n\n			.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger {\n				background-color: #96588a;\n				color: #ffffff;\n			}\n\n			.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger:hover {\n				background-color: #7d3f71;\n				border-color: #7d3f71;\n				color: #ffffff;\n			}\n\n			@media screen and ( min-width: 768px ) {\n				.site-header-cart .widget_shopping_cart,\n				.site-header .product_list_widget li .quantity {\n					color: #9aa0a7;\n				}\n			}";s:39:"storefront_woocommerce_extension_styles";s:0:"";s:18:"nav_menu_locations";a:1:{s:7:"primary";i:18;}s:18:"custom_css_post_id";i:-1;s:20:"shcs_contact_address";s:47:"125 DC11, phường Sơn Kỳ, quận Tân Phú";s:25:"shcs_contact_phone_number";s:10:"0962054200";s:26:"shcs_contact_email_address";s:21:"kqthang1505@gmail.com";s:16:"sidebars_widgets";a:2:{s:4:"time";i:1491130466;s:4:"data";a:7:{s:19:"wp_inactive_widgets";a:6:{i:0;s:15:"slider_widget-2";i:1;s:18:"wp_editor_widget-2";i:2;s:18:"wp_editor_widget-3";i:3;s:26:"woocommerce_price_filter-2";i:4;s:32:"woocommerce_product_categories-2";i:5;s:22:"woocommerce_products-2";}s:9:"sidebar-1";a:1:{i:0;s:14:"recent-posts-2";}s:8:"header-1";a:0:{}s:8:"footer-1";a:0:{}s:8:"footer-2";N;s:8:"footer-3";N;s:8:"footer-4";N;}}}', 'yes'),
(288, 'theme_switched', '', 'yes'),
(299, 'theme_mods_storefront-child', 'a:19:{i:0;b:0;s:17:"storefront_styles";s:4645:"\n			.main-navigation ul li a,\n			.site-title a,\n			ul.menu li a,\n			.site-branding h1 a,\n			.site-footer .storefront-handheld-footer-bar a:not(.button),\n			button.menu-toggle,\n			button.menu-toggle:hover {\n				color: #ffffff;\n			}\n\n			button.menu-toggle,\n			button.menu-toggle:hover {\n				border-color: #ffffff;\n			}\n\n			.main-navigation ul li a:hover,\n			.main-navigation ul li:hover > a,\n			.site-title a:hover,\n			a.cart-contents:hover,\n			.site-header-cart .widget_shopping_cart a:hover,\n			.site-header-cart:hover > li > a,\n			.site-header ul.menu li.current-menu-item > a {\n				color: #ffffff;\n			}\n\n			table th {\n				background-color: #f1f1f1;\n			}\n\n			table tbody td {\n				background-color: #f6f6f6;\n			}\n\n			table tbody tr:nth-child(2n) td {\n				background-color: #f4f4f4;\n			}\n\n			.site-header,\n			.secondary-navigation ul ul,\n			.main-navigation ul.menu > li.menu-item-has-children:after,\n			.secondary-navigation ul.menu ul,\n			.storefront-handheld-footer-bar,\n			.storefront-handheld-footer-bar ul li > a,\n			.storefront-handheld-footer-bar ul li.search .site-search,\n			button.menu-toggle,\n			button.menu-toggle:hover {\n				background-color: #2c2d33;\n			}\n\n			p.site-description,\n			.site-header,\n			.storefront-handheld-footer-bar {\n				color: #212121;\n			}\n\n			.storefront-handheld-footer-bar ul li.cart .count,\n			button.menu-toggle:after,\n			button.menu-toggle:before,\n			button.menu-toggle span:before {\n				background-color: #ffffff;\n			}\n\n			.storefront-handheld-footer-bar ul li.cart .count {\n				color: #2c2d33;\n			}\n\n			.storefront-handheld-footer-bar ul li.cart .count {\n				border-color: #2c2d33;\n			}\n\n			h1, h2, h3, h4, h5, h6 {\n				color: #484c51;\n			}\n\n			.widget h1 {\n				border-bottom-color: #484c51;\n			}\n\n			body,\n			.secondary-navigation a,\n			.onsale,\n			.pagination .page-numbers li .page-numbers:not(.current), .woocommerce-pagination .page-numbers li .page-numbers:not(.current) {\n				color: #43454b;\n			}\n\n			.widget-area .widget a,\n			.hentry .entry-header .posted-on a,\n			.hentry .entry-header .byline a {\n				color: #75777d;\n			}\n\n			a  {\n				color: #c0392b;\n			}\n\n			a:focus,\n			.button:focus,\n			.button.alt:focus,\n			.button.added_to_cart:focus,\n			.button.wc-forward:focus,\n			button:focus,\n			input[type="button"]:focus,\n			input[type="reset"]:focus,\n			input[type="submit"]:focus {\n				outline-color: #c0392b;\n			}\n\n			button, input[type="button"], input[type="reset"], input[type="submit"], .button, .added_to_cart, .widget a.button, .site-header-cart .widget_shopping_cart a.button {\n				background-color: #dd4f4f;\n				border-color: #dd4f4f;\n				color: #ffffff;\n			}\n\n			button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, .button:hover, .added_to_cart:hover, .widget a.button:hover, .site-header-cart .widget_shopping_cart a.button:hover {\n				background-color: #c43636;\n				border-color: #c43636;\n				color: #ffffff;\n			}\n\n			button.alt, input[type="button"].alt, input[type="reset"].alt, input[type="submit"].alt, .button.alt, .added_to_cart.alt, .widget-area .widget a.button.alt, .added_to_cart, .pagination .page-numbers li .page-numbers.current, .woocommerce-pagination .page-numbers li .page-numbers.current, .widget a.button.checkout {\n				background-color: #2c2d33;\n				border-color: #2c2d33;\n				color: #ffffff;\n			}\n\n			button.alt:hover, input[type="button"].alt:hover, input[type="reset"].alt:hover, input[type="submit"].alt:hover, .button.alt:hover, .added_to_cart.alt:hover, .widget-area .widget a.button.alt:hover, .added_to_cart:hover, .widget a.button.checkout:hover {\n				background-color: #13141a;\n				border-color: #13141a;\n				color: #ffffff;\n			}\n\n			#comments .comment-list .comment-content .comment-text {\n				background-color: #f1f1f1;\n			}\n\n			.site-footer {\n				background-color: #f0f0f0;\n				color: #61656b;\n			}\n\n			.site-footer a:not(.button) {\n				color: #2c2d33;\n			}\n\n			.site-footer h1, .site-footer h2, .site-footer h3, .site-footer h4, .site-footer h5, .site-footer h6 {\n				color: #494c50;\n			}\n\n			#order_review,\n			#payment .payment_methods > li .payment_box {\n				background-color: #f8f8f8;\n			}\n\n			#payment .payment_methods > li {\n				background-color: #f3f3f3;\n			}\n\n			#payment .payment_methods > li:hover {\n				background-color: #eeeeee;\n			}\n\n			@media screen and ( min-width: 768px ) {\n				.secondary-navigation ul.menu a:hover {\n					color: #3a3a3a;\n				}\n\n				.secondary-navigation ul.menu a {\n					color: #212121;\n				}\n\n				.site-header-cart .widget_shopping_cart,\n				.main-navigation ul.menu ul.sub-menu,\n				.main-navigation ul.nav-menu ul.children {\n					background-color: #24252b;\n				}\n			}";s:29:"storefront_woocommerce_styles";s:2233:"\n			a.cart-contents,\n			.site-header-cart .widget_shopping_cart a {\n				color: #ffffff;\n			}\n\n			table.cart td.product-remove,\n			table.cart td.actions {\n				border-top-color: #f8f8f8;\n			}\n\n			.woocommerce-tabs ul.tabs li.active a,\n			ul.products li.product .price,\n			.onsale,\n			.widget_search form:before,\n			.widget_product_search form:before {\n				color: #43454b;\n			}\n\n			.woocommerce-breadcrumb a,\n			a.woocommerce-review-link,\n			.product_meta a {\n				color: #75777d;\n			}\n\n			.onsale {\n				border-color: #43454b;\n			}\n\n			.star-rating span:before,\n			.quantity .plus, .quantity .minus,\n			p.stars a:hover:after,\n			p.stars a:after,\n			.star-rating span:before,\n			#payment .payment_methods li input[type=radio]:first-child:checked+label:before {\n				color: #c0392b;\n			}\n\n			.widget_price_filter .ui-slider .ui-slider-range,\n			.widget_price_filter .ui-slider .ui-slider-handle {\n				background-color: #c0392b;\n			}\n\n			.woocommerce-breadcrumb,\n			#reviews .commentlist li .comment_container {\n				background-color: #f1f1f1;\n			}\n\n			.order_details {\n				background-color: #f1f1f1;\n			}\n\n			.order_details > li {\n				border-bottom: 1px dotted #dcdcdc;\n			}\n\n			.order_details:before,\n			.order_details:after {\n				background: -webkit-linear-gradient(transparent 0,transparent 0),-webkit-linear-gradient(135deg,#f1f1f1 33.33%,transparent 33.33%),-webkit-linear-gradient(45deg,#f1f1f1 33.33%,transparent 33.33%)\n			}\n\n			p.stars a:before,\n			p.stars a:hover~a:before,\n			p.stars.selected a.active~a:before {\n				color: #43454b;\n			}\n\n			p.stars.selected a.active:before,\n			p.stars:hover a:before,\n			p.stars.selected a:not(.active):before,\n			p.stars.selected a.active:before {\n				color: #c0392b;\n			}\n\n			.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger {\n				background-color: #dd4f4f;\n				color: #ffffff;\n			}\n\n			.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger:hover {\n				background-color: #c43636;\n				border-color: #c43636;\n				color: #ffffff;\n			}\n\n			@media screen and ( min-width: 768px ) {\n				.site-header-cart .widget_shopping_cart,\n				.site-header .product_list_widget li .quantity {\n					color: #212121;\n				}\n			}";s:39:"storefront_woocommerce_extension_styles";s:0:"";s:18:"custom_css_post_id";i:-1;s:17:"storefront_layout";s:4:"left";s:18:"nav_menu_locations";a:1:{s:7:"primary";i:18;}s:20:"shcs_contact_address";s:47:"125 DC11, phường Sơn Kỳ, quận Tân Phú";s:25:"shcs_contact_phone_number";s:10:"0962054200";s:26:"shcs_contact_email_address";s:21:"kqthang1505@gmail.com";s:17:"shcs_contact_form";i:1;s:12:"header_image";s:69:"http://tbyt.thang.dev/wp-content/uploads/2017/03/cropped-IMG_6629.jpg";s:17:"header_image_data";O:8:"stdClass":5:{s:13:"attachment_id";i:39;s:3:"url";s:69:"http://tbyt.thang.dev/wp-content/uploads/2017/03/cropped-IMG_6629.jpg";s:13:"thumbnail_url";s:69:"http://tbyt.thang.dev/wp-content/uploads/2017/03/cropped-IMG_6629.jpg";s:6:"height";i:500;s:5:"width";i:1949;}s:28:"storefront_header_text_color";s:7:"#212121";s:28:"storefront_header_link_color";s:7:"#ffffff";s:16:"background_color";s:6:"f8f8f8";s:34:"storefront_button_background_color";s:7:"#dd4f4f";s:23:"storefront_accent_color";s:7:"#c0392b";s:11:"custom_logo";i:65;}', 'yes'),
(320, '_transient_timeout_wc_shipping_method_count_1_1489682313', '1492275559', 'no'),
(321, '_transient_wc_shipping_method_count_1_1489682313', '0', 'no'),
(363, 'product_cat_children', 'a:4:{i:6;a:2:{i:0;i:10;i:1;i:11;}i:7;a:2:{i:0;i:12;i:1;i:13;}i:8;a:2:{i:0;i:14;i:1;i:15;}i:9;a:2:{i:0;i:16;i:1;i:17;}}', 'yes'),
(365, 'category_children', 'a:0:{}', 'yes'),
(368, '_transient_timeout_wc_var_prices_8', '1493904130', 'no'),
(369, '_transient_wc_var_prices_8', '{"version":"1489821612","c6534391efde8a29761fa26774f165c4":{"price":{"16":"9000.00","17":"6000.00"},"regular_price":{"16":"9000.00","17":"6000.00"},"sale_price":{"16":"9000.00","17":"6000.00"}},"d1aa9c25b90b49f04cbf821f9277877e":{"price":{"16":"9000.00","17":"6000.00"},"regular_price":{"16":"9000.00","17":"6000.00"},"sale_price":{"16":"9000.00","17":"6000.00"}}}', 'no'),
(391, 'WPLANG', 'vi', 'yes'),
(408, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(410, 'widget_wpb_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(411, 'widget_policy_widget', 'a:2:{i:5;a:2:{s:5:"title";s:28:"Chính sách và quy định";s:5:"pages";s:2:"27";}s:12:"_multiwidget";i:1;}', 'yes'),
(456, 'storefront-footer-bar-version', '1.0.3', 'yes'),
(459, 'storefront-homepage-contact-section-version', '1.0.2', 'yes'),
(466, 'storefront-product-sharing-version', '1.0.3', 'yes'),
(468, 'woocommerce_permalinks', 'a:5:{s:13:"category_base";s:0:"";s:8:"tag_base";s:0:"";s:14:"attribute_base";s:0:"";s:12:"product_base";s:5:"/shop";s:22:"use_verbose_page_rules";b:1;}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(486, 'jetpack_file_data', 'a:2:{s:5:"4.7.1";a:52:{s:32:"31e5b9ae08b62c2b0cd8a7792242298b";a:14:{s:4:"name";s:20:"Spelling and Grammar";s:11:"description";s:40:"Check your spelling, style, and grammar.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"6";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:7:"Writing";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:115:"after the deadline, afterthedeadline, spell, spellchecker, spelling, grammar, proofreading, style, language, cliche";}s:32:"3f41b2d629265b5de8108b463abbe8e2";a:14:{s:4:"name";s:8:"Carousel";s:11:"description";s:76:"Display images and galleries in a gorgeous, full-screen browsing experience.";s:14:"jumpstart_desc";s:79:"Brings your photos and images to life as full-size, easily navigable galleries.";s:4:"sort";s:2:"22";s:20:"recommendation_order";s:2:"12";s:10:"introduced";s:3:"1.5";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:17:"Photos and Videos";s:7:"feature";s:21:"Appearance, Jumpstart";s:25:"additional_search_queries";s:80:"gallery, carousel, diaporama, slideshow, images, lightbox, exif, metadata, image";}s:32:"c6ebb418dde302de09600a6025370583";a:14:{s:4:"name";s:8:"Comments";s:11:"description";s:65:"Allow comments with WordPress.com, Twitter, Facebook, or Google+.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"20";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.4";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:6:"Social";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:53:"comments, comment, facebook, twitter, google+, social";}s:32:"836f9485669e1bbb02920cb474730df0";a:14:{s:4:"name";s:12:"Contact Form";s:11:"description";s:57:"Insert a customizable contact form anywhere on your site.";s:14:"jumpstart_desc";s:111:"Adds a button to your post and page editors, allowing you to build simple forms to help visitors stay in touch.";s:4:"sort";s:2:"15";s:20:"recommendation_order";s:2:"14";s:10:"introduced";s:3:"1.3";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:5:"Other";s:7:"feature";s:18:"Writing, Jumpstart";s:25:"additional_search_queries";s:44:"contact, form, grunion, feedback, submission";}s:32:"ea3970eebf8aac55fc3eca5dca0e0157";a:14:{s:4:"name";s:20:"Custom Content Types";s:11:"description";s:61:"Organize and display different types of content on your site.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"34";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"3.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:7:"Writing";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:72:"cpt, custom post types, portfolio, portfolios, testimonial, testimonials";}s:32:"d2bb05ccad3d8789df40ca3abb97336c";a:14:{s:4:"name";s:10:"Custom CSS";s:11:"description";s:53:"Tweak your site’s CSS without modifying your theme.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"2";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.7";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:10:"Appearance";s:7:"feature";s:10:"Appearance";s:25:"additional_search_queries";s:108:"css, customize, custom, style, editor, less, sass, preprocessor, font, mobile, appearance, theme, stylesheet";}s:32:"a2064eec5b9c7e0d816af71dee7a715f";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"53a4ec755022ef3953699734c343da02";a:14:{s:4:"name";s:21:"Enhanced Distribution";s:11:"description";s:27:"Increase reach and traffic.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"5";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.2";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:6:"Public";s:11:"module_tags";s:7:"Writing";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:54:"google, seo, firehose, search, broadcast, broadcasting";}s:32:"e1f1f6e3689fc31c477e64b06e2f8fbf";a:14:{s:4:"name";s:16:"Google Analytics";s:11:"description";s:56:"Set up Google Analytics without touching a line of code.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"37";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"4.5";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:0:"";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:37:"webmaster, google, analytics, console";}s:32:"72fecb67ee6704ba0a33e0225316ad06";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"d56e2886185a9eace719cc57d46770df";a:14:{s:4:"name";s:19:"Gravatar Hovercards";s:11:"description";s:58:"Enable pop-up business cards over commenters’ Gravatars.";s:14:"jumpstart_desc";s:131:"Let commenters link their profiles to their Gravatar accounts, making it easy for your visitors to learn more about your community.";s:4:"sort";s:2:"11";s:20:"recommendation_order";s:2:"13";s:10:"introduced";s:3:"1.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:18:"Social, Appearance";s:7:"feature";s:21:"Appearance, Jumpstart";s:25:"additional_search_queries";s:20:"gravatar, hovercards";}s:32:"e391e760617bd0e0736550e34a73d7fe";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:8:"2.0.3 ??";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"2e345370766346c616b3c5046e817720";a:14:{s:4:"name";s:15:"Infinite Scroll";s:11:"description";s:54:"Automatically load new content when a visitor scrolls.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"26";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.0";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:10:"Appearance";s:7:"feature";s:10:"Appearance";s:25:"additional_search_queries";s:33:"scroll, infinite, infinite scroll";}s:32:"bd69edbf134de5fae8fdcf2e70a45b56";a:14:{s:4:"name";s:8:"JSON API";s:11:"description";s:51:"Allow applications to securely access your content.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"19";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.9";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:6:"Public";s:11:"module_tags";s:19:"Writing, Developers";s:7:"feature";s:7:"General";s:25:"additional_search_queries";s:50:"api, rest, develop, developers, json, klout, oauth";}s:32:"8110b7a4423aaa619dfa46b8843e10d1";a:14:{s:4:"name";s:14:"Beautiful Math";s:11:"description";s:57:"Use LaTeX markup for complex equations and other geekery.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"12";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:7:"Writing";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:47:"latex, math, equation, equations, formula, code";}s:32:"fd7e85d3b4887fa6b6f997d6592c1f33";a:14:{s:4:"name";s:5:"Likes";s:11:"description";s:63:"Give visitors an easy way to show they appreciate your content.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"23";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.2";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:6:"Social";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:26:"like, likes, wordpress.com";}s:32:"c5dfef41fad5bcdcaae8e315e5cfc420";a:14:{s:4:"name";s:6:"Manage";s:11:"description";s:54:"Manage all of your sites from a centralized dashboard.";s:14:"jumpstart_desc";s:151:"Helps you remotely manage plugins, turn on automated updates, and more from <a href="https://wordpress.com/plugins/" target="_blank">wordpress.com</a>.";s:4:"sort";s:1:"1";s:20:"recommendation_order";s:1:"3";s:10:"introduced";s:3:"3.4";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:35:"Centralized Management, Recommended";s:7:"feature";s:7:"General";s:25:"additional_search_queries";s:26:"manage, management, remote";}s:32:"fd6dc399b92bce76013427e3107c314f";a:14:{s:4:"name";s:8:"Markdown";s:11:"description";s:51:"Write posts or pages in plain-text Markdown syntax.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"31";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.8";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:7:"Writing";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:12:"md, markdown";}s:32:"c49a35b6482b0426cb07ad28ecf5d7df";a:14:{s:4:"name";s:12:"Mobile Theme";s:11:"description";s:47:"Optimize your site for smartphones and tablets.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"21";s:20:"recommendation_order";s:2:"11";s:10:"introduced";s:3:"1.8";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:31:"Appearance, Mobile, Recommended";s:7:"feature";s:10:"Appearance";s:25:"additional_search_queries";s:24:"mobile, theme, minileven";}s:32:"b42e38f6fafd2e4104ebe5bf39b4be47";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"771cfeeba0d3d23ec344d5e781fb0ae2";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"54f0661d27c814fc8bde39580181d939";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"46c4c413b5c72bbd3c3dbd14ff8f8adc";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"9ea52fa25783e5ceeb6bfaed3268e64e";a:14:{s:4:"name";s:7:"Monitor";s:11:"description";s:61:"Receive immediate notifications if your site goes down, 24/7.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"28";s:20:"recommendation_order";s:2:"10";s:10:"introduced";s:3:"2.6";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:11:"Recommended";s:7:"feature";s:8:"Security";s:25:"additional_search_queries";s:37:"monitor, uptime, downtime, monitoring";}s:32:"cfcaafd0fcad087899d715e0b877474d";a:14:{s:4:"name";s:13:"Notifications";s:11:"description";s:57:"Receive instant notifications of site comments and likes.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"13";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.9";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:5:"Other";s:7:"feature";s:7:"General";s:25:"additional_search_queries";s:62:"notification, notifications, toolbar, adminbar, push, comments";}s:32:"0d18bfa69bec61550c1d813ce64149b0";a:14:{s:4:"name";s:10:"Omnisearch";s:11:"description";s:66:"Search your entire database from a single field in your dashboard.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"16";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.3";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:10:"Developers";s:7:"feature";s:7:"General";s:25:"additional_search_queries";s:6:"search";}s:32:"3f0a11e23118f0788d424b646a6d465f";a:14:{s:4:"name";s:6:"Photon";s:11:"description";s:27:"Speed up images and photos.";s:14:"jumpstart_desc";s:141:"Mirrors and serves your images from our free and fast image CDN, improving your site’s performance with no additional load on your servers.";s:4:"sort";s:2:"25";s:20:"recommendation_order";s:1:"1";s:10:"introduced";s:3:"2.0";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:42:"Photos and Videos, Appearance, Recommended";s:7:"feature";s:34:"Recommended, Jumpstart, Appearance";s:25:"additional_search_queries";s:38:"photon, image, cdn, performance, speed";}s:32:"e37cfbcb72323fb1fe8255a2edb4d738";a:14:{s:4:"name";s:13:"Post by Email";s:11:"description";s:34:"Publish posts by sending an email.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"14";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.0";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:7:"Writing";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:20:"post by email, email";}s:32:"728290d131a480bfe7b9e405d7cd925f";a:14:{s:4:"name";s:7:"Protect";s:11:"description";s:43:"Prevent and block malicious login attempts.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"1";s:20:"recommendation_order";s:1:"4";s:10:"introduced";s:3:"3.4";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:11:"Recommended";s:7:"feature";s:8:"Security";s:25:"additional_search_queries";s:65:"security, secure, protection, botnet, brute force, protect, login";}s:32:"f9ce784babbbf4dcca99b8cd2ceb420c";a:14:{s:4:"name";s:9:"Publicize";s:11:"description";s:27:"Automated social marketing.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"10";s:20:"recommendation_order";s:1:"7";s:10:"introduced";s:3:"2.0";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:19:"Social, Recommended";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:107:"facebook, twitter, google+, googleplus, google, path, tumblr, linkedin, social, tweet, connections, sharing";}s:32:"052c03877dd3d296a71531cb07ad939a";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"52edecb2a75222e75b2dce4356a4efce";a:14:{s:4:"name";s:13:"Related Posts";s:11:"description";s:64:"Increase page views by showing related content to your visitors.";s:14:"jumpstart_desc";s:113:"Keep visitors engaged on your blog by highlighting relevant and new content at the bottom of each published post.";s:4:"sort";s:2:"29";s:20:"recommendation_order";s:1:"9";s:10:"introduced";s:3:"2.9";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:11:"Recommended";s:7:"feature";s:21:"Engagement, Jumpstart";s:25:"additional_search_queries";s:22:"related, related posts";}s:32:"68b0d01689803c0ea7e4e60a86de2519";a:14:{s:4:"name";s:9:"SEO tools";s:11:"description";s:50:"Better results on search engines and social media.";s:14:"jumpstart_desc";s:50:"Better results on search engines and social media.";s:4:"sort";s:2:"35";s:20:"recommendation_order";s:2:"15";s:10:"introduced";s:3:"4.4";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:18:"Social, Appearance";s:7:"feature";s:18:"Traffic, Jumpstart";s:25:"additional_search_queries";s:81:"search engine optimization, social preview, meta description, custom title format";}s:32:"8b059cb50a66b717f1ec842e736b858c";a:14:{s:4:"name";s:7:"Sharing";s:11:"description";s:37:"Allow visitors to share your content.";s:14:"jumpstart_desc";s:116:"Twitter, Facebook and Google+ buttons at the bottom of each post, making it easy for visitors to share your content.";s:4:"sort";s:1:"7";s:20:"recommendation_order";s:1:"6";s:10:"introduced";s:3:"1.1";s:7:"changed";s:3:"1.2";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:19:"Social, Recommended";s:7:"feature";s:21:"Engagement, Jumpstart";s:25:"additional_search_queries";s:141:"share, sharing, sharedaddy, buttons, icons, email, facebook, twitter, google+, linkedin, pinterest, pocket, press this, print, reddit, tumblr";}s:32:"a6d2394329871857401255533a9873f7";a:14:{s:4:"name";s:16:"Shortcode Embeds";s:11:"description";s:50:"Embed media from popular sites without any coding.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"3";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.1";s:7:"changed";s:3:"1.2";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:46:"Photos and Videos, Social, Writing, Appearance";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:236:"shortcodes, shortcode, embeds, media, bandcamp, dailymotion, facebook, flickr, google calendars, google maps, google+, polldaddy, recipe, recipes, scribd, slideshare, slideshow, slideshows, soundcloud, ted, twitter, vimeo, vine, youtube";}s:32:"21496e2897ea5f81605e2f2ac3beb921";a:14:{s:4:"name";s:16:"WP.me Shortlinks";s:11:"description";s:54:"Create short and simple links for all posts and pages.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"8";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:6:"Social";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:17:"shortlinks, wp.me";}s:32:"e2a54a5d7879a4162709e6ffb540dd08";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"f5c537bc304f55b29c1a87e30be0cd24";a:14:{s:4:"name";s:8:"Sitemaps";s:11:"description";s:50:"Make it easy for search engines to find your site.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"13";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"3.9";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:6:"Public";s:11:"module_tags";s:20:"Recommended, Traffic";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:39:"sitemap, traffic, search, site map, seo";}s:32:"59a23643437358a9b557f1d1e20ab040";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"6a90f97c3194cfca5671728eaaeaf15e";a:14:{s:4:"name";s:14:"Single Sign On";s:11:"description";s:46:"Secure user authentication with WordPress.com.";s:14:"jumpstart_desc";s:98:"Lets you log in to all your Jetpack-enabled sites with one click using your WordPress.com account.";s:4:"sort";s:2:"30";s:20:"recommendation_order";s:1:"5";s:10:"introduced";s:3:"2.6";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:10:"Developers";s:7:"feature";s:19:"Security, Jumpstart";s:25:"additional_search_queries";s:34:"sso, single sign on, login, log in";}s:32:"b65604e920392e2f7134b646760b75e8";a:14:{s:4:"name";s:10:"Site Stats";s:11:"description";s:44:"Collect valuable traffic stats and insights.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"1";s:20:"recommendation_order";s:1:"2";s:10:"introduced";s:3:"1.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:23:"Site Stats, Recommended";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:54:"statistics, tracking, analytics, views, traffic, stats";}s:32:"23a586dd7ead00e69ec53eb32ef740e4";a:14:{s:4:"name";s:13:"Subscriptions";s:11:"description";s:55:"Notify your readers of new posts and comments by email.";s:14:"jumpstart_desc";s:126:"Give visitors two easy subscription options — while commenting, or via a separate email subscription widget you can display.";s:4:"sort";s:1:"9";s:20:"recommendation_order";s:1:"8";s:10:"introduced";s:3:"1.2";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:6:"Social";s:7:"feature";s:21:"Engagement, Jumpstart";s:25:"additional_search_queries";s:74:"subscriptions, subscription, email, follow, followers, subscribers, signup";}s:32:"1d978b8d84d2f378fe1a702a67633b6d";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"b3b983461d7f3d27322a3551ed8a9405";a:14:{s:4:"name";s:15:"Tiled Galleries";s:11:"description";s:61:"Display image galleries in a variety of elegant arrangements.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"24";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:17:"Photos and Videos";s:7:"feature";s:10:"Appearance";s:25:"additional_search_queries";s:43:"gallery, tiles, tiled, grid, mosaic, images";}s:32:"d924e5b05722b0e104448543598f54c0";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"36741583b10c521997e563ad8e1e8b77";a:14:{s:4:"name";s:12:"Data Backups";s:11:"description";s:54:"Off-site backups, security scans, and automatic fixes.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"32";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:5:"0:1.2";s:7:"changed";s:0:"";s:10:"deactivate";s:5:"false";s:4:"free";s:5:"false";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:0:"";s:7:"feature";s:16:"Security, Health";s:25:"additional_search_queries";s:28:"vaultpress, backup, security";}s:32:"2b9b44f09b5459617d68dd82ee17002a";a:14:{s:4:"name";s:17:"Site Verification";s:11:"description";s:58:"Establish your site\'s authenticity with external services.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"33";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"3.0";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:0:"";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:56:"webmaster, seo, google, bing, pinterest, search, console";}s:32:"5ab4c0db7c42e10e646342da0274c491";a:14:{s:4:"name";s:10:"VideoPress";s:11:"description";s:45:"Powerful, simple video hosting for WordPress.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"27";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.5";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:5:"false";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:17:"Photos and Videos";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:25:"video, videos, videopress";}s:32:"60a1d3aa38bc0fe1039e59dd60888543";a:14:{s:4:"name";s:17:"Widget Visibility";s:11:"description";s:42:"Control where widgets appear on your site.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"17";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.4";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:10:"Appearance";s:7:"feature";s:10:"Appearance";s:25:"additional_search_queries";s:54:"widget visibility, logic, conditional, widgets, widget";}s:32:"174ed3416476c2cb9ff5b0f671280b15";a:14:{s:4:"name";s:21:"Extra Sidebar Widgets";s:11:"description";s:54:"Add images, Twitter streams, and more to your sidebar.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"4";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.2";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:18:"Social, Appearance";s:7:"feature";s:10:"Appearance";s:25:"additional_search_queries";s:65:"widget, widgets, facebook, gallery, twitter, gravatar, image, rss";}s:32:"a668bc9418d6de87409f867892fcdd7f";a:14:{s:4:"name";s:3:"Ads";s:11:"description";s:60:"Earn income by allowing Jetpack to display high quality ads.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"1";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:5:"4.5.0";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:19:"Traffic, Appearance";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:26:"advertising, ad codes, ads";}s:32:"28b931a1db19bd24869bd54b14e733d5";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}}s:5:"4.8.1";a:53:{s:32:"31e5b9ae08b62c2b0cd8a7792242298b";a:14:{s:4:"name";s:20:"Spelling and Grammar";s:11:"description";s:39:"Check your spelling, style, and grammar";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"6";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:7:"Writing";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:115:"after the deadline, afterthedeadline, spell, spellchecker, spelling, grammar, proofreading, style, language, cliche";}s:32:"3f41b2d629265b5de8108b463abbe8e2";a:14:{s:4:"name";s:8:"Carousel";s:11:"description";s:75:"Display images and galleries in a gorgeous, full-screen browsing experience";s:14:"jumpstart_desc";s:79:"Brings your photos and images to life as full-size, easily navigable galleries.";s:4:"sort";s:2:"22";s:20:"recommendation_order";s:2:"12";s:10:"introduced";s:3:"1.5";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:17:"Photos and Videos";s:7:"feature";s:21:"Appearance, Jumpstart";s:25:"additional_search_queries";s:80:"gallery, carousel, diaporama, slideshow, images, lightbox, exif, metadata, image";}s:32:"c6ebb418dde302de09600a6025370583";a:14:{s:4:"name";s:8:"Comments";s:11:"description";s:80:"Let readers use WordPress.com, Twitter, Facebook, or Google+ accounts to comment";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"20";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.4";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:6:"Social";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:53:"comments, comment, facebook, twitter, google+, social";}s:32:"836f9485669e1bbb02920cb474730df0";a:14:{s:4:"name";s:12:"Contact Form";s:11:"description";s:57:"Insert a customizable contact form anywhere on your site.";s:14:"jumpstart_desc";s:111:"Adds a button to your post and page editors, allowing you to build simple forms to help visitors stay in touch.";s:4:"sort";s:2:"15";s:20:"recommendation_order";s:2:"14";s:10:"introduced";s:3:"1.3";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:5:"Other";s:7:"feature";s:18:"Writing, Jumpstart";s:25:"additional_search_queries";s:44:"contact, form, grunion, feedback, submission";}s:32:"ea3970eebf8aac55fc3eca5dca0e0157";a:14:{s:4:"name";s:20:"Custom content types";s:11:"description";s:74:"Display different types of content on your site with custom content types.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"34";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"3.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:7:"Writing";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:72:"cpt, custom post types, portfolio, portfolios, testimonial, testimonials";}s:32:"d2bb05ccad3d8789df40ca3abb97336c";a:14:{s:4:"name";s:10:"Custom CSS";s:11:"description";s:53:"Tweak your site’s CSS without modifying your theme.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"2";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.7";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:10:"Appearance";s:7:"feature";s:10:"Appearance";s:25:"additional_search_queries";s:108:"css, customize, custom, style, editor, less, sass, preprocessor, font, mobile, appearance, theme, stylesheet";}s:32:"a2064eec5b9c7e0d816af71dee7a715f";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"53a4ec755022ef3953699734c343da02";a:14:{s:4:"name";s:21:"Enhanced Distribution";s:11:"description";s:27:"Increase reach and traffic.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"5";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.2";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:6:"Public";s:11:"module_tags";s:7:"Writing";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:54:"google, seo, firehose, search, broadcast, broadcasting";}s:32:"e1f1f6e3689fc31c477e64b06e2f8fbf";a:14:{s:4:"name";s:16:"Google Analytics";s:11:"description";s:56:"Set up Google Analytics without touching a line of code.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"37";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"4.5";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:0:"";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:37:"webmaster, google, analytics, console";}s:32:"72fecb67ee6704ba0a33e0225316ad06";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"d56e2886185a9eace719cc57d46770df";a:14:{s:4:"name";s:19:"Gravatar Hovercards";s:11:"description";s:58:"Enable pop-up business cards over commenters’ Gravatars.";s:14:"jumpstart_desc";s:131:"Let commenters link their profiles to their Gravatar accounts, making it easy for your visitors to learn more about your community.";s:4:"sort";s:2:"11";s:20:"recommendation_order";s:2:"13";s:10:"introduced";s:3:"1.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:18:"Social, Appearance";s:7:"feature";s:21:"Appearance, Jumpstart";s:25:"additional_search_queries";s:20:"gravatar, hovercards";}s:32:"e391e760617bd0e0736550e34a73d7fe";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:8:"2.0.3 ??";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"2e345370766346c616b3c5046e817720";a:14:{s:4:"name";s:15:"Infinite Scroll";s:11:"description";s:53:"Automatically load new content when a visitor scrolls";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"26";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.0";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:10:"Appearance";s:7:"feature";s:10:"Appearance";s:25:"additional_search_queries";s:33:"scroll, infinite, infinite scroll";}s:32:"bd69edbf134de5fae8fdcf2e70a45b56";a:14:{s:4:"name";s:8:"JSON API";s:11:"description";s:51:"Allow applications to securely access your content.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"19";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.9";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:6:"Public";s:11:"module_tags";s:19:"Writing, Developers";s:7:"feature";s:7:"General";s:25:"additional_search_queries";s:50:"api, rest, develop, developers, json, klout, oauth";}s:32:"8110b7a4423aaa619dfa46b8843e10d1";a:14:{s:4:"name";s:14:"Beautiful Math";s:11:"description";s:57:"Use LaTeX markup for complex equations and other geekery.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"12";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:7:"Writing";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:47:"latex, math, equation, equations, formula, code";}s:32:"fd7e85d3b4887fa6b6f997d6592c1f33";a:14:{s:4:"name";s:5:"Likes";s:11:"description";s:63:"Give visitors an easy way to show they appreciate your content.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"23";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.2";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:6:"Social";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:26:"like, likes, wordpress.com";}s:32:"c5dfef41fad5bcdcaae8e315e5cfc420";a:14:{s:4:"name";s:6:"Manage";s:11:"description";s:54:"Manage all of your sites from a centralized dashboard.";s:14:"jumpstart_desc";s:151:"Helps you remotely manage plugins, turn on automated updates, and more from <a href="https://wordpress.com/plugins/" target="_blank">wordpress.com</a>.";s:4:"sort";s:1:"1";s:20:"recommendation_order";s:1:"3";s:10:"introduced";s:3:"3.4";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:35:"Centralized Management, Recommended";s:7:"feature";s:7:"General";s:25:"additional_search_queries";s:26:"manage, management, remote";}s:32:"fd6dc399b92bce76013427e3107c314f";a:14:{s:4:"name";s:8:"Markdown";s:11:"description";s:50:"Write posts or pages in plain-text Markdown syntax";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"31";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.8";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:7:"Writing";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:12:"md, markdown";}s:32:"614679778a7db6d8129c9f69ac8e10a5";a:14:{s:4:"name";s:21:"WordPress.com Toolbar";s:11:"description";s:91:"Replaces the admin bar with a useful toolbar to quickly manage your site via WordPress.com.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"38";s:20:"recommendation_order";s:2:"16";s:10:"introduced";s:3:"4.8";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:7:"General";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:19:"adminbar, masterbar";}s:32:"c49a35b6482b0426cb07ad28ecf5d7df";a:14:{s:4:"name";s:12:"Mobile Theme";s:11:"description";s:34:"Optimize your site for smartphones";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"21";s:20:"recommendation_order";s:2:"11";s:10:"introduced";s:3:"1.8";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:31:"Appearance, Mobile, Recommended";s:7:"feature";s:10:"Appearance";s:25:"additional_search_queries";s:24:"mobile, theme, minileven";}s:32:"b42e38f6fafd2e4104ebe5bf39b4be47";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"771cfeeba0d3d23ec344d5e781fb0ae2";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"54f0661d27c814fc8bde39580181d939";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"46c4c413b5c72bbd3c3dbd14ff8f8adc";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"9ea52fa25783e5ceeb6bfaed3268e64e";a:14:{s:4:"name";s:7:"Monitor";s:11:"description";s:61:"Receive immediate notifications if your site goes down, 24/7.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"28";s:20:"recommendation_order";s:2:"10";s:10:"introduced";s:3:"2.6";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:11:"Recommended";s:7:"feature";s:8:"Security";s:25:"additional_search_queries";s:37:"monitor, uptime, downtime, monitoring";}s:32:"cfcaafd0fcad087899d715e0b877474d";a:14:{s:4:"name";s:13:"Notifications";s:11:"description";s:57:"Receive instant notifications of site comments and likes.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"13";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.9";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:5:"Other";s:7:"feature";s:7:"General";s:25:"additional_search_queries";s:62:"notification, notifications, toolbar, adminbar, push, comments";}s:32:"0d18bfa69bec61550c1d813ce64149b0";a:14:{s:4:"name";s:10:"Omnisearch";s:11:"description";s:66:"Search your entire database from a single field in your dashboard.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"16";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.3";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:10:"Developers";s:7:"feature";s:7:"General";s:25:"additional_search_queries";s:6:"search";}s:32:"3f0a11e23118f0788d424b646a6d465f";a:14:{s:4:"name";s:6:"Photon";s:11:"description";s:26:"Speed up images and photos";s:14:"jumpstart_desc";s:141:"Mirrors and serves your images from our free and fast image CDN, improving your site’s performance with no additional load on your servers.";s:4:"sort";s:2:"25";s:20:"recommendation_order";s:1:"1";s:10:"introduced";s:3:"2.0";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:42:"Photos and Videos, Appearance, Recommended";s:7:"feature";s:34:"Recommended, Jumpstart, Appearance";s:25:"additional_search_queries";s:38:"photon, image, cdn, performance, speed";}s:32:"e37cfbcb72323fb1fe8255a2edb4d738";a:14:{s:4:"name";s:13:"Post by email";s:11:"description";s:33:"Publish posts by sending an email";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"14";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.0";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:7:"Writing";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:20:"post by email, email";}s:32:"728290d131a480bfe7b9e405d7cd925f";a:14:{s:4:"name";s:7:"Protect";s:11:"description";s:41:"Block suspicious-looking sign in activity";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"1";s:20:"recommendation_order";s:1:"4";s:10:"introduced";s:3:"3.4";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:11:"Recommended";s:7:"feature";s:8:"Security";s:25:"additional_search_queries";s:65:"security, secure, protection, botnet, brute force, protect, login";}s:32:"f9ce784babbbf4dcca99b8cd2ceb420c";a:14:{s:4:"name";s:9:"Publicize";s:11:"description";s:27:"Automated social marketing.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"10";s:20:"recommendation_order";s:1:"7";s:10:"introduced";s:3:"2.0";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:19:"Social, Recommended";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:107:"facebook, twitter, google+, googleplus, google, path, tumblr, linkedin, social, tweet, connections, sharing";}s:32:"052c03877dd3d296a71531cb07ad939a";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"52edecb2a75222e75b2dce4356a4efce";a:14:{s:4:"name";s:13:"Related posts";s:11:"description";s:64:"Increase page views by showing related content to your visitors.";s:14:"jumpstart_desc";s:113:"Keep visitors engaged on your blog by highlighting relevant and new content at the bottom of each published post.";s:4:"sort";s:2:"29";s:20:"recommendation_order";s:1:"9";s:10:"introduced";s:3:"2.9";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:11:"Recommended";s:7:"feature";s:21:"Engagement, Jumpstart";s:25:"additional_search_queries";s:22:"related, related posts";}s:32:"68b0d01689803c0ea7e4e60a86de2519";a:14:{s:4:"name";s:9:"SEO Tools";s:11:"description";s:50:"Better results on search engines and social media.";s:14:"jumpstart_desc";s:50:"Better results on search engines and social media.";s:4:"sort";s:2:"35";s:20:"recommendation_order";s:2:"15";s:10:"introduced";s:3:"4.4";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:18:"Social, Appearance";s:7:"feature";s:18:"Traffic, Jumpstart";s:25:"additional_search_queries";s:81:"search engine optimization, social preview, meta description, custom title format";}s:32:"8b059cb50a66b717f1ec842e736b858c";a:14:{s:4:"name";s:7:"Sharing";s:11:"description";s:37:"Allow visitors to share your content.";s:14:"jumpstart_desc";s:116:"Twitter, Facebook and Google+ buttons at the bottom of each post, making it easy for visitors to share your content.";s:4:"sort";s:1:"7";s:20:"recommendation_order";s:1:"6";s:10:"introduced";s:3:"1.1";s:7:"changed";s:3:"1.2";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:19:"Social, Recommended";s:7:"feature";s:21:"Engagement, Jumpstart";s:25:"additional_search_queries";s:141:"share, sharing, sharedaddy, buttons, icons, email, facebook, twitter, google+, linkedin, pinterest, pocket, press this, print, reddit, tumblr";}s:32:"a6d2394329871857401255533a9873f7";a:14:{s:4:"name";s:16:"Shortcode Embeds";s:11:"description";s:50:"Embed media from popular sites without any coding.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"3";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.1";s:7:"changed";s:3:"1.2";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:46:"Photos and Videos, Social, Writing, Appearance";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:236:"shortcodes, shortcode, embeds, media, bandcamp, dailymotion, facebook, flickr, google calendars, google maps, google+, polldaddy, recipe, recipes, scribd, slideshare, slideshow, slideshows, soundcloud, ted, twitter, vimeo, vine, youtube";}s:32:"21496e2897ea5f81605e2f2ac3beb921";a:14:{s:4:"name";s:16:"WP.me Shortlinks";s:11:"description";s:54:"Create short and simple links for all posts and pages.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"8";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:6:"Social";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:17:"shortlinks, wp.me";}s:32:"e2a54a5d7879a4162709e6ffb540dd08";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"f5c537bc304f55b29c1a87e30be0cd24";a:14:{s:4:"name";s:8:"Sitemaps";s:11:"description";s:50:"Make it easy for search engines to find your site.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"13";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"3.9";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:6:"Public";s:11:"module_tags";s:20:"Recommended, Traffic";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:39:"sitemap, traffic, search, site map, seo";}s:32:"59a23643437358a9b557f1d1e20ab040";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"6a90f97c3194cfca5671728eaaeaf15e";a:14:{s:4:"name";s:14:"Single Sign On";s:11:"description";s:62:"Allow users to log into this site using WordPress.com accounts";s:14:"jumpstart_desc";s:98:"Lets you log in to all your Jetpack-enabled sites with one click using your WordPress.com account.";s:4:"sort";s:2:"30";s:20:"recommendation_order";s:1:"5";s:10:"introduced";s:3:"2.6";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:10:"Developers";s:7:"feature";s:19:"Security, Jumpstart";s:25:"additional_search_queries";s:34:"sso, single sign on, login, log in";}s:32:"b65604e920392e2f7134b646760b75e8";a:14:{s:4:"name";s:10:"Site Stats";s:11:"description";s:44:"Collect valuable traffic stats and insights.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"1";s:20:"recommendation_order";s:1:"2";s:10:"introduced";s:3:"1.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:23:"Site Stats, Recommended";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:54:"statistics, tracking, analytics, views, traffic, stats";}s:32:"23a586dd7ead00e69ec53eb32ef740e4";a:14:{s:4:"name";s:13:"Subscriptions";s:11:"description";s:87:"Allow users to subscribe to your posts and comments and receive notifications via email";s:14:"jumpstart_desc";s:126:"Give visitors two easy subscription options — while commenting, or via a separate email subscription widget you can display.";s:4:"sort";s:1:"9";s:20:"recommendation_order";s:1:"8";s:10:"introduced";s:3:"1.2";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:6:"Social";s:7:"feature";s:21:"Engagement, Jumpstart";s:25:"additional_search_queries";s:74:"subscriptions, subscription, email, follow, followers, subscribers, signup";}s:32:"1d978b8d84d2f378fe1a702a67633b6d";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"b3b983461d7f3d27322a3551ed8a9405";a:14:{s:4:"name";s:15:"Tiled Galleries";s:11:"description";s:61:"Display image galleries in a variety of elegant arrangements.";s:14:"jumpstart_desc";s:61:"Display image galleries in a variety of elegant arrangements.";s:4:"sort";s:2:"24";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.1";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:17:"Photos and Videos";s:7:"feature";s:21:"Appearance, Jumpstart";s:25:"additional_search_queries";s:43:"gallery, tiles, tiled, grid, mosaic, images";}s:32:"d924e5b05722b0e104448543598f54c0";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}s:32:"36741583b10c521997e563ad8e1e8b77";a:14:{s:4:"name";s:12:"Data Backups";s:11:"description";s:54:"Off-site backups, security scans, and automatic fixes.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"32";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:5:"0:1.2";s:7:"changed";s:0:"";s:10:"deactivate";s:5:"false";s:4:"free";s:5:"false";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:0:"";s:7:"feature";s:16:"Security, Health";s:25:"additional_search_queries";s:28:"vaultpress, backup, security";}s:32:"2b9b44f09b5459617d68dd82ee17002a";a:14:{s:4:"name";s:17:"Site verification";s:11:"description";s:58:"Establish your site\'s authenticity with external services.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"33";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"3.0";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:0:"";s:7:"feature";s:10:"Engagement";s:25:"additional_search_queries";s:56:"webmaster, seo, google, bing, pinterest, search, console";}s:32:"5ab4c0db7c42e10e646342da0274c491";a:14:{s:4:"name";s:10:"VideoPress";s:11:"description";s:27:"Fast, ad-free video hosting";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"27";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.5";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:5:"false";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:17:"Photos and Videos";s:7:"feature";s:7:"Writing";s:25:"additional_search_queries";s:25:"video, videos, videopress";}s:32:"60a1d3aa38bc0fe1039e59dd60888543";a:14:{s:4:"name";s:17:"Widget Visibility";s:11:"description";s:42:"Control where widgets appear on your site.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:2:"17";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"2.4";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:10:"Appearance";s:7:"feature";s:10:"Appearance";s:25:"additional_search_queries";s:54:"widget visibility, logic, conditional, widgets, widget";}s:32:"174ed3416476c2cb9ff5b0f671280b15";a:14:{s:4:"name";s:21:"Extra Sidebar Widgets";s:11:"description";s:54:"Add images, Twitter streams, and more to your sidebar.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"4";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:3:"1.2";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:2:"No";s:13:"auto_activate";s:3:"Yes";s:11:"module_tags";s:18:"Social, Appearance";s:7:"feature";s:10:"Appearance";s:25:"additional_search_queries";s:65:"widget, widgets, facebook, gallery, twitter, gravatar, image, rss";}s:32:"a668bc9418d6de87409f867892fcdd7f";a:14:{s:4:"name";s:3:"Ads";s:11:"description";s:60:"Earn income by allowing Jetpack to display high quality ads.";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:1:"1";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:5:"4.5.0";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:3:"Yes";s:13:"auto_activate";s:2:"No";s:11:"module_tags";s:19:"Traffic, Appearance";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:26:"advertising, ad codes, ads";}s:32:"28b931a1db19bd24869bd54b14e733d5";a:14:{s:4:"name";s:0:"";s:11:"description";s:0:"";s:14:"jumpstart_desc";s:0:"";s:4:"sort";s:0:"";s:20:"recommendation_order";s:0:"";s:10:"introduced";s:0:"";s:7:"changed";s:0:"";s:10:"deactivate";s:0:"";s:4:"free";s:0:"";s:19:"requires_connection";s:0:"";s:13:"auto_activate";s:0:"";s:11:"module_tags";s:0:"";s:7:"feature";s:0:"";s:25:"additional_search_queries";s:0:"";}}}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(487, 'jetpack_available_modules', 'a:1:{s:5:"4.8.1";a:40:{s:18:"after-the-deadline";s:3:"1.1";s:8:"carousel";s:3:"1.5";s:8:"comments";s:3:"1.4";s:12:"contact-form";s:3:"1.3";s:20:"custom-content-types";s:3:"3.1";s:10:"custom-css";s:3:"1.7";s:21:"enhanced-distribution";s:3:"1.2";s:16:"google-analytics";s:3:"4.5";s:19:"gravatar-hovercards";s:3:"1.1";s:15:"infinite-scroll";s:3:"2.0";s:8:"json-api";s:3:"1.9";s:5:"latex";s:3:"1.1";s:5:"likes";s:3:"2.2";s:6:"manage";s:3:"3.4";s:8:"markdown";s:3:"2.8";s:9:"masterbar";s:3:"4.8";s:9:"minileven";s:3:"1.8";s:7:"monitor";s:3:"2.6";s:5:"notes";s:3:"1.9";s:10:"omnisearch";s:3:"2.3";s:6:"photon";s:3:"2.0";s:13:"post-by-email";s:3:"2.0";s:7:"protect";s:3:"3.4";s:9:"publicize";s:3:"2.0";s:13:"related-posts";s:3:"2.9";s:9:"seo-tools";s:3:"4.4";s:10:"sharedaddy";s:3:"1.1";s:10:"shortcodes";s:3:"1.1";s:10:"shortlinks";s:3:"1.1";s:8:"sitemaps";s:3:"3.9";s:3:"sso";s:3:"2.6";s:5:"stats";s:3:"1.1";s:13:"subscriptions";s:3:"1.2";s:13:"tiled-gallery";s:3:"2.1";s:10:"vaultpress";s:5:"0:1.2";s:18:"verification-tools";s:3:"3.0";s:10:"videopress";s:3:"2.5";s:17:"widget-visibility";s:3:"2.4";s:7:"widgets";s:3:"1.2";s:7:"wordads";s:5:"4.5.0";}}', 'yes'),
(490, 'jetpack_testimonial', '0', 'yes'),
(491, 'do_activate', '0', 'yes'),
(494, '_transient_timeout_jetpack_https_test_message', '1491532012', 'no'),
(495, '_transient_jetpack_https_test_message', '', 'no'),
(498, 'sharing-options', 'a:1:{s:6:"global";a:5:{s:12:"button_style";s:9:"icon-text";s:13:"sharing_label";s:10:"Chia sẻ:";s:10:"open_links";s:4:"same";s:4:"show";a:0:{}s:6:"custom";a:0:{}}}', 'yes'),
(499, 'stats_options', 'a:7:{s:9:"admin_bar";b:1;s:5:"roles";a:1:{i:0;s:13:"administrator";}s:11:"count_roles";a:0:{}s:7:"blog_id";b:0;s:12:"do_not_track";b:1;s:10:"hide_smile";b:1;s:7:"version";s:1:"9";}', 'yes'),
(509, 'feedback_unread_count', '0', 'yes'),
(614, 'widget_slider_widget', 'a:2:{i:2;a:2:{s:5:"title";s:12:"Home Sliders";s:5:"pages";s:2:"68";}s:12:"_multiwidget";i:1;}', 'yes'),
(668, 'widget_wyswyg_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(692, 'widget_wp_editor_widget', 'a:2:{i:2;a:3:{s:5:"title";s:6:"Footer";s:7:"content";s:887:"<p> </p><p><strong>MST</strong> : 0309537103</p><p><strong>Đăng ký lần đầu</strong> : ngày 05 tháng 1 năm 2010</p><p><strong>Đăng ký thay đổi lần thứ 1</strong> : ngày 24 tháng 11 năm 2014. Do sở kế hoạch và đầu tư TP Hồ Chí Minh , phòng Đăng Ký Kinh Doanh cấp.</p><p><strong>Đại diện</strong> : Nguyễn Thị Ngọc Phượng</p><p><strong>Địa chỉ</strong>: 245 Nhật Tảo - Phường 8 - Quận 10 - Tp HCM</p><p><strong>Điện thoại</strong>: (08) 6271 3730 - 0902 333 345 | Fax: (08) 6290 9152</p><p><strong>Email</strong>: hoada175@gmail.com</p><p><strong>Website</strong>: hoadamedical.com</p><p><a href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=21172"><img class="" src="http://hoadamedical.com/upload/news/20150827110756-dathongbao.png" alt="Sở Công Thương" width="147" height="56" /></a></p>";s:12:"output_title";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(697, 'widget_pages_cus', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(698, 'widget_cus_pages', 'a:2:{i:2;a:4:{s:5:"title";s:12:"Chính sách";s:6:"sortby";s:10:"menu_order";s:7:"exclude";s:0:"";s:7:"include";s:2:"27";}s:12:"_multiwidget";i:1;}', 'yes'),
(699, 'widget_pages_list', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(741, '_transient_timeout_plugin_slugs', '1491613436', 'no'),
(742, '_transient_plugin_slugs', 'a:6:{i:0;s:19:"akismet/akismet.php";i:1;s:36:"contact-form-7/wp-contact-form-7.php";i:2;s:33:"custom-widgets/custom-widgets.php";i:3;s:21:"flamingo/flamingo.php";i:4;s:35:"redux-framework/redux-framework.php";i:5;s:27:"woocommerce/woocommerce.php";}', 'no'),
(769, '_transient_orders-transient-version', '1491099981', 'yes'),
(772, '_site_transient_timeout_browser_cc5d8203ce9b96784a0bea9c5a67de54', '1491672224', 'no'),
(773, '_site_transient_browser_cc5d8203ce9b96784a0bea9c5a67de54', 'a:9:{s:8:"platform";s:9:"Macintosh";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"56.0.2924.87";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'no'),
(800, '_transient_timeout_wc_cbp_b021a9b02bdfba13cdbdf167441911a3', '1493692447', 'no'),
(801, '_transient_wc_cbp_b021a9b02bdfba13cdbdf167441911a3', 'a:8:{i:0;i:8;i:1;i:8;i:2;i:17;i:3;i:17;i:4;i:20;i:5;i:20;i:6;i:20;i:7;i:20;}', 'no'),
(802, '_transient_wc_count_comments', 'O:8:"stdClass":7:{s:8:"approved";s:1:"2";s:14:"total_comments";i:2;s:3:"all";i:2;s:9:"moderated";i:0;s:4:"spam";i:0;s:5:"trash";i:0;s:12:"post-trashed";i:0;}', 'yes'),
(870, 'jpsq_sync_checkout', '0:0', 'no'),
(872, 'jetpack_next_sync_time_full-sync-enqueue', '1491446793', 'yes'),
(874, 'jetpack_log', 'a:4:{i:0;a:4:{s:4:"time";i:1491101149;s:7:"user_id";i:1;s:7:"blog_id";b:0;s:4:"code";s:8:"register";}i:1;a:4:{s:4:"time";i:1491233060;s:7:"user_id";i:1;s:7:"blog_id";b:0;s:4:"code";s:8:"register";}i:2;a:4:{s:4:"time";i:1491445754;s:7:"user_id";i:1;s:7:"blog_id";b:0;s:4:"code";s:8:"register";}i:3;a:4:{s:4:"time";i:1491445770;s:7:"user_id";i:1;s:7:"blog_id";b:0;s:4:"code";s:8:"register";}}', 'no'),
(875, 'jetpack_private_options', 'a:0:{}', 'yes'),
(924, '_transient_timeout_external_ip_address_::1', '1491739051', 'no'),
(925, '_transient_external_ip_address_::1', '183.81.10.108', 'no'),
(1035, 'limit_login_captcha', 'checked', 'yes'),
(1036, 'limit_login_install_date', '2017-04-03 15:50:43', 'yes'),
(1059, 'woocommerce_shipping_debug_mode', 'no', 'no'),
(1077, 'woocommerce_bacs_accounts', 'a:1:{i:0;a:6:{s:12:"account_name";s:0:"";s:14:"account_number";s:0:"";s:9:"bank_name";s:0:"";s:9:"sort_code";s:0:"";s:4:"iban";s:0:"";s:3:"bic";s:0:"";}}', 'yes'),
(1082, '_transient_woocommerce_cache_excluded_uris', 'a:6:{i:0;s:3:"p=5";i:1;s:6:"/cart/";i:2;s:3:"p=6";i:3;s:10:"/checkout/";i:4;s:3:"p=2";i:5;s:13:"/sample-page/";}', 'yes'),
(1089, '_transient_timeout_wc_term_counts', '1493906133', 'no'),
(1090, '_transient_wc_term_counts', 'a:12:{i:11;s:0:"";i:10;s:0:"";i:7;s:0:"";i:14;s:0:"";i:17;s:1:"0";i:9;s:1:"0";i:13;s:0:"";i:6;s:0:"";i:12;s:0:"";i:15;s:0:"";i:8;s:0:"";i:16;s:0:"";}', 'no'),
(1094, '_transient_timeout_wc_featured_products', '1493906183', 'no'),
(1095, '_transient_wc_featured_products', 'a:0:{}', 'no'),
(1096, '_transient_timeout_wc_products_onsale', '1493906183', 'no'),
(1097, '_transient_wc_products_onsale', 'a:0:{}', 'no'),
(1098, '_transient_timeout_wc_loop2e751491314162', '1493906183', 'no'),
(1099, '_transient_wc_loop2e751491314162', 'O:8:"WP_Query":49:{s:5:"query";a:8:{s:9:"post_type";s:7:"product";s:11:"post_status";s:7:"publish";s:19:"ignore_sticky_posts";i:1;s:14:"posts_per_page";s:1:"2";s:7:"orderby";s:4:"date";s:5:"order";s:4:"desc";s:10:"meta_query";a:0:{}s:9:"tax_query";a:1:{i:0;a:4:{s:8:"taxonomy";s:18:"product_visibility";s:5:"field";s:16:"term_taxonomy_id";s:5:"terms";a:1:{i:0;i:20;}s:8:"operator";s:6:"NOT IN";}}}s:10:"query_vars";a:68:{s:9:"post_type";s:7:"product";s:11:"post_status";s:7:"publish";s:19:"ignore_sticky_posts";i:1;s:14:"posts_per_page";i:2;s:7:"orderby";s:4:"date";s:5:"order";s:4:"DESC";s:10:"meta_query";a:0:{}s:9:"tax_query";a:1:{i:0;a:4:{s:8:"taxonomy";s:18:"product_visibility";s:5:"field";s:16:"term_taxonomy_id";s:5:"terms";a:1:{i:0;i:20;}s:8:"operator";s:6:"NOT IN";}}s:5:"error";s:0:"";s:1:"m";s:0:"";s:1:"p";i:0;s:11:"post_parent";s:0:"";s:7:"subpost";s:0:"";s:10:"subpost_id";s:0:"";s:10:"attachment";s:0:"";s:13:"attachment_id";i:0;s:4:"name";s:0:"";s:6:"static";s:0:"";s:8:"pagename";s:0:"";s:7:"page_id";i:0;s:6:"second";s:0:"";s:6:"minute";s:0:"";s:4:"hour";s:0:"";s:3:"day";i:0;s:8:"monthnum";i:0;s:4:"year";i:0;s:1:"w";i:0;s:13:"category_name";s:0:"";s:3:"tag";s:0:"";s:3:"cat";s:0:"";s:6:"tag_id";s:0:"";s:6:"author";s:0:"";s:11:"author_name";s:0:"";s:4:"feed";s:0:"";s:2:"tb";s:0:"";s:5:"paged";i:0;s:8:"meta_key";s:0:"";s:10:"meta_value";s:0:"";s:7:"preview";s:0:"";s:1:"s";s:0:"";s:8:"sentence";s:0:"";s:5:"title";s:0:"";s:6:"fields";s:0:"";s:10:"menu_order";s:0:"";s:5:"embed";s:0:"";s:12:"category__in";a:0:{}s:16:"category__not_in";a:0:{}s:13:"category__and";a:0:{}s:8:"post__in";a:0:{}s:12:"post__not_in";a:0:{}s:13:"post_name__in";a:0:{}s:7:"tag__in";a:0:{}s:11:"tag__not_in";a:0:{}s:8:"tag__and";a:0:{}s:12:"tag_slug__in";a:0:{}s:13:"tag_slug__and";a:0:{}s:15:"post_parent__in";a:0:{}s:19:"post_parent__not_in";a:0:{}s:10:"author__in";a:0:{}s:14:"author__not_in";a:0:{}s:16:"suppress_filters";b:0;s:13:"cache_results";b:1;s:22:"update_post_term_cache";b:1;s:19:"lazy_load_term_meta";b:1;s:22:"update_post_meta_cache";b:1;s:8:"nopaging";b:0;s:17:"comments_per_page";s:2:"50";s:13:"no_found_rows";b:0;}s:9:"tax_query";O:12:"WP_Tax_Query":6:{s:7:"queries";a:1:{i:0;a:5:{s:8:"taxonomy";s:18:"product_visibility";s:5:"terms";a:1:{i:0;i:20;}s:5:"field";s:16:"term_taxonomy_id";s:8:"operator";s:6:"NOT IN";s:16:"include_children";b:1;}}s:8:"relation";s:3:"AND";s:16:"\0*\0table_aliases";a:0:{}s:13:"queried_terms";a:0:{}s:13:"primary_table";s:8:"wp_posts";s:17:"primary_id_column";s:2:"ID";}s:10:"meta_query";O:13:"WP_Meta_Query":9:{s:7:"queries";a:0:{}s:8:"relation";N;s:10:"meta_table";N;s:14:"meta_id_column";N;s:13:"primary_table";N;s:17:"primary_id_column";N;s:16:"\0*\0table_aliases";a:0:{}s:10:"\0*\0clauses";a:0:{}s:18:"\0*\0has_or_relation";b:0;}s:10:"date_query";b:0;s:7:"request";s:330:"SELECT SQL_CALC_FOUND_ROWS  wp_posts.ID FROM wp_posts  WHERE 1=1  AND ( \n  wp_posts.ID NOT IN (\n				SELECT object_id\n				FROM wp_term_relationships\n				WHERE term_taxonomy_id IN (20)\n			)\n) AND wp_posts.post_type = \'product\' AND ((wp_posts.post_status = \'publish\')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT 0, 2";s:5:"posts";a:1:{i:0;O:7:"WP_Post":24:{s:2:"ID";i:70;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-04-04 20:55:29";s:13:"post_date_gmt";s:19:"2017-04-04 13:55:29";s:12:"post_content";s:126:"Massage cơ và các huyệt đạo với 6 con lăn. Tích hợp chức năng nhiệt nhẹ nhàng bằng tia hồng ngoại.";s:10:"post_title";s:32:"Massage lưng vai gáy - Mas 632";s:12:"post_excerpt";s:126:"Massage cơ và các huyệt đạo với 6 con lăn. Tích hợp chức năng nhiệt nhẹ nhàng bằng tia hồng ngoại.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:28:"massage-lung-vai-gay-mas-632";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-04-04 20:56:01";s:17:"post_modified_gmt";s:19:"2017-04-04 13:56:01";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:59:"http://thang.dev/thiet-bi-y-te/?post_type=product&#038;p=70";s:10:"menu_order";i:0;s:9:"post_type";s:7:"product";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}s:10:"post_count";i:1;s:12:"current_post";i:-1;s:11:"in_the_loop";b:0;s:4:"post";r:119;s:13:"comment_count";i:0;s:15:"current_comment";i:-1;s:11:"found_posts";s:1:"1";s:13:"max_num_pages";d:1;s:21:"max_num_comment_pages";i:0;s:9:"is_single";b:0;s:10:"is_preview";b:0;s:7:"is_page";b:0;s:10:"is_archive";b:1;s:7:"is_date";b:0;s:7:"is_year";b:0;s:8:"is_month";b:0;s:6:"is_day";b:0;s:7:"is_time";b:0;s:9:"is_author";b:0;s:11:"is_category";b:0;s:6:"is_tag";b:0;s:6:"is_tax";b:0;s:9:"is_search";b:0;s:7:"is_feed";b:0;s:15:"is_comment_feed";b:0;s:12:"is_trackback";b:0;s:7:"is_home";b:0;s:6:"is_404";b:0;s:8:"is_embed";b:0;s:8:"is_paged";b:0;s:8:"is_admin";b:0;s:13:"is_attachment";b:0;s:11:"is_singular";b:0;s:9:"is_robots";b:0;s:13:"is_posts_page";b:0;s:20:"is_post_type_archive";b:1;s:25:"\0WP_Query\0query_vars_hash";s:32:"8c928075bd553014080bd8c5f3a60cda";s:28:"\0WP_Query\0query_vars_changed";b:0;s:17:"thumbnails_cached";b:0;s:19:"\0WP_Query\0stopwords";N;s:23:"\0WP_Query\0compat_fields";a:2:{i:0;s:15:"query_vars_hash";i:1;s:18:"query_vars_changed";}s:24:"\0WP_Query\0compat_methods";a:2:{i:0;s:16:"init_query_flags";i:1;s:15:"parse_tax_query";}}', 'no'),
(1100, '_transient_timeout_wc_loopdc3d1491314162', '1493906183', 'no'),
(1101, '_transient_wc_loopdc3d1491314162', 'O:8:"WP_Query":49:{s:5:"query";a:8:{s:9:"post_type";s:7:"product";s:11:"post_status";s:7:"publish";s:19:"ignore_sticky_posts";i:1;s:14:"posts_per_page";i:4;s:8:"meta_key";s:11:"total_sales";s:7:"orderby";s:14:"meta_value_num";s:10:"meta_query";a:0:{}s:9:"tax_query";a:1:{i:0;a:4:{s:8:"taxonomy";s:18:"product_visibility";s:5:"field";s:16:"term_taxonomy_id";s:5:"terms";a:1:{i:0;i:20;}s:8:"operator";s:6:"NOT IN";}}}s:10:"query_vars";a:68:{s:9:"post_type";s:7:"product";s:11:"post_status";s:7:"publish";s:19:"ignore_sticky_posts";i:1;s:14:"posts_per_page";i:4;s:8:"meta_key";s:11:"total_sales";s:7:"orderby";s:14:"meta_value_num";s:10:"meta_query";a:0:{}s:9:"tax_query";a:1:{i:0;a:4:{s:8:"taxonomy";s:18:"product_visibility";s:5:"field";s:16:"term_taxonomy_id";s:5:"terms";a:1:{i:0;i:20;}s:8:"operator";s:6:"NOT IN";}}s:5:"error";s:0:"";s:1:"m";s:0:"";s:1:"p";i:0;s:11:"post_parent";s:0:"";s:7:"subpost";s:0:"";s:10:"subpost_id";s:0:"";s:10:"attachment";s:0:"";s:13:"attachment_id";i:0;s:4:"name";s:0:"";s:6:"static";s:0:"";s:8:"pagename";s:0:"";s:7:"page_id";i:0;s:6:"second";s:0:"";s:6:"minute";s:0:"";s:4:"hour";s:0:"";s:3:"day";i:0;s:8:"monthnum";i:0;s:4:"year";i:0;s:1:"w";i:0;s:13:"category_name";s:0:"";s:3:"tag";s:0:"";s:3:"cat";s:0:"";s:6:"tag_id";s:0:"";s:6:"author";s:0:"";s:11:"author_name";s:0:"";s:4:"feed";s:0:"";s:2:"tb";s:0:"";s:5:"paged";i:0;s:10:"meta_value";s:0:"";s:7:"preview";s:0:"";s:1:"s";s:0:"";s:8:"sentence";s:0:"";s:5:"title";s:0:"";s:6:"fields";s:0:"";s:10:"menu_order";s:0:"";s:5:"embed";s:0:"";s:12:"category__in";a:0:{}s:16:"category__not_in";a:0:{}s:13:"category__and";a:0:{}s:8:"post__in";a:0:{}s:12:"post__not_in";a:0:{}s:13:"post_name__in";a:0:{}s:7:"tag__in";a:0:{}s:11:"tag__not_in";a:0:{}s:8:"tag__and";a:0:{}s:12:"tag_slug__in";a:0:{}s:13:"tag_slug__and";a:0:{}s:15:"post_parent__in";a:0:{}s:19:"post_parent__not_in";a:0:{}s:10:"author__in";a:0:{}s:14:"author__not_in";a:0:{}s:16:"suppress_filters";b:0;s:13:"cache_results";b:1;s:22:"update_post_term_cache";b:1;s:19:"lazy_load_term_meta";b:1;s:22:"update_post_meta_cache";b:1;s:8:"nopaging";b:0;s:17:"comments_per_page";s:2:"50";s:13:"no_found_rows";b:0;s:5:"order";s:4:"DESC";}s:9:"tax_query";O:12:"WP_Tax_Query":6:{s:7:"queries";a:1:{i:0;a:5:{s:8:"taxonomy";s:18:"product_visibility";s:5:"terms";a:1:{i:0;i:20;}s:5:"field";s:16:"term_taxonomy_id";s:8:"operator";s:6:"NOT IN";s:16:"include_children";b:1;}}s:8:"relation";s:3:"AND";s:16:"\0*\0table_aliases";a:0:{}s:13:"queried_terms";a:0:{}s:13:"primary_table";s:8:"wp_posts";s:17:"primary_id_column";s:2:"ID";}s:10:"meta_query";O:13:"WP_Meta_Query":9:{s:7:"queries";a:2:{i:0;a:1:{s:3:"key";s:11:"total_sales";}s:8:"relation";s:2:"OR";}s:8:"relation";s:3:"AND";s:10:"meta_table";s:11:"wp_postmeta";s:14:"meta_id_column";s:7:"post_id";s:13:"primary_table";s:8:"wp_posts";s:17:"primary_id_column";s:2:"ID";s:16:"\0*\0table_aliases";a:1:{i:0;s:11:"wp_postmeta";}s:10:"\0*\0clauses";a:1:{s:11:"wp_postmeta";a:4:{s:3:"key";s:11:"total_sales";s:7:"compare";s:1:"=";s:5:"alias";s:11:"wp_postmeta";s:4:"cast";s:4:"CHAR";}}s:18:"\0*\0has_or_relation";b:0;}s:10:"date_query";b:0;s:7:"request";s:448:"SELECT SQL_CALC_FOUND_ROWS  wp_posts.ID FROM wp_posts  INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id ) WHERE 1=1  AND ( \n  wp_posts.ID NOT IN (\n				SELECT object_id\n				FROM wp_term_relationships\n				WHERE term_taxonomy_id IN (20)\n			)\n) AND ( \n  wp_postmeta.meta_key = \'total_sales\'\n) AND wp_posts.post_type = \'product\' AND ((wp_posts.post_status = \'publish\')) GROUP BY wp_posts.ID ORDER BY wp_postmeta.meta_value+0 DESC LIMIT 0, 4";s:5:"posts";a:1:{i:0;O:7:"WP_Post":24:{s:2:"ID";i:70;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-04-04 20:55:29";s:13:"post_date_gmt";s:19:"2017-04-04 13:55:29";s:12:"post_content";s:126:"Massage cơ và các huyệt đạo với 6 con lăn. Tích hợp chức năng nhiệt nhẹ nhàng bằng tia hồng ngoại.";s:10:"post_title";s:32:"Massage lưng vai gáy - Mas 632";s:12:"post_excerpt";s:126:"Massage cơ và các huyệt đạo với 6 con lăn. Tích hợp chức năng nhiệt nhẹ nhàng bằng tia hồng ngoại.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:28:"massage-lung-vai-gay-mas-632";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-04-04 20:56:01";s:17:"post_modified_gmt";s:19:"2017-04-04 13:56:01";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:59:"http://thang.dev/thiet-bi-y-te/?post_type=product&#038;p=70";s:10:"menu_order";i:0;s:9:"post_type";s:7:"product";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}s:10:"post_count";i:1;s:12:"current_post";i:-1;s:11:"in_the_loop";b:0;s:4:"post";r:128;s:13:"comment_count";i:0;s:15:"current_comment";i:-1;s:11:"found_posts";s:1:"1";s:13:"max_num_pages";d:1;s:21:"max_num_comment_pages";i:0;s:9:"is_single";b:0;s:10:"is_preview";b:0;s:7:"is_page";b:0;s:10:"is_archive";b:1;s:7:"is_date";b:0;s:7:"is_year";b:0;s:8:"is_month";b:0;s:6:"is_day";b:0;s:7:"is_time";b:0;s:9:"is_author";b:0;s:11:"is_category";b:0;s:6:"is_tag";b:0;s:6:"is_tax";b:0;s:9:"is_search";b:0;s:7:"is_feed";b:0;s:15:"is_comment_feed";b:0;s:12:"is_trackback";b:0;s:7:"is_home";b:0;s:6:"is_404";b:0;s:8:"is_embed";b:0;s:8:"is_paged";b:0;s:8:"is_admin";b:0;s:13:"is_attachment";b:0;s:11:"is_singular";b:0;s:9:"is_robots";b:0;s:13:"is_posts_page";b:0;s:20:"is_post_type_archive";b:1;s:25:"\0WP_Query\0query_vars_hash";s:32:"58883fac048d7566b3f7559c36be5701";s:28:"\0WP_Query\0query_vars_changed";b:0;s:17:"thumbnails_cached";b:0;s:19:"\0WP_Query\0stopwords";N;s:23:"\0WP_Query\0compat_fields";a:2:{i:0;s:15:"query_vars_hash";i:1;s:18:"query_vars_changed";}s:24:"\0WP_Query\0compat_methods";a:2:{i:0;s:16:"init_query_flags";i:1;s:15:"parse_tax_query";}}', 'no'),
(1103, '_transient_timeout_wc_loopda8b1491314162', '1493908034', 'no'),
(1104, '_transient_wc_loopda8b1491314162', 'O:8:"WP_Query":49:{s:5:"query";a:8:{s:9:"post_type";s:7:"product";s:11:"post_status";s:7:"publish";s:19:"ignore_sticky_posts";i:1;s:14:"posts_per_page";i:4;s:7:"orderby";s:4:"date";s:5:"order";s:4:"desc";s:10:"meta_query";a:0:{}s:9:"tax_query";a:1:{i:0;a:4:{s:8:"taxonomy";s:18:"product_visibility";s:5:"field";s:16:"term_taxonomy_id";s:5:"terms";a:1:{i:0;i:20;}s:8:"operator";s:6:"NOT IN";}}}s:10:"query_vars";a:68:{s:9:"post_type";s:7:"product";s:11:"post_status";s:7:"publish";s:19:"ignore_sticky_posts";i:1;s:14:"posts_per_page";i:4;s:7:"orderby";s:4:"date";s:5:"order";s:4:"DESC";s:10:"meta_query";a:0:{}s:9:"tax_query";a:1:{i:0;a:4:{s:8:"taxonomy";s:18:"product_visibility";s:5:"field";s:16:"term_taxonomy_id";s:5:"terms";a:1:{i:0;i:20;}s:8:"operator";s:6:"NOT IN";}}s:5:"error";s:0:"";s:1:"m";s:0:"";s:1:"p";i:0;s:11:"post_parent";s:0:"";s:7:"subpost";s:0:"";s:10:"subpost_id";s:0:"";s:10:"attachment";s:0:"";s:13:"attachment_id";i:0;s:4:"name";s:0:"";s:6:"static";s:0:"";s:8:"pagename";s:0:"";s:7:"page_id";i:0;s:6:"second";s:0:"";s:6:"minute";s:0:"";s:4:"hour";s:0:"";s:3:"day";i:0;s:8:"monthnum";i:0;s:4:"year";i:0;s:1:"w";i:0;s:13:"category_name";s:0:"";s:3:"tag";s:0:"";s:3:"cat";s:0:"";s:6:"tag_id";s:0:"";s:6:"author";s:0:"";s:11:"author_name";s:0:"";s:4:"feed";s:0:"";s:2:"tb";s:0:"";s:5:"paged";i:0;s:8:"meta_key";s:0:"";s:10:"meta_value";s:0:"";s:7:"preview";s:0:"";s:1:"s";s:0:"";s:8:"sentence";s:0:"";s:5:"title";s:0:"";s:6:"fields";s:0:"";s:10:"menu_order";s:0:"";s:5:"embed";s:0:"";s:12:"category__in";a:0:{}s:16:"category__not_in";a:0:{}s:13:"category__and";a:0:{}s:8:"post__in";a:0:{}s:12:"post__not_in";a:0:{}s:13:"post_name__in";a:0:{}s:7:"tag__in";a:0:{}s:11:"tag__not_in";a:0:{}s:8:"tag__and";a:0:{}s:12:"tag_slug__in";a:0:{}s:13:"tag_slug__and";a:0:{}s:15:"post_parent__in";a:0:{}s:19:"post_parent__not_in";a:0:{}s:10:"author__in";a:0:{}s:14:"author__not_in";a:0:{}s:16:"suppress_filters";b:0;s:13:"cache_results";b:1;s:22:"update_post_term_cache";b:1;s:19:"lazy_load_term_meta";b:1;s:22:"update_post_meta_cache";b:1;s:8:"nopaging";b:0;s:17:"comments_per_page";s:2:"50";s:13:"no_found_rows";b:0;}s:9:"tax_query";O:12:"WP_Tax_Query":6:{s:7:"queries";a:1:{i:0;a:5:{s:8:"taxonomy";s:18:"product_visibility";s:5:"terms";a:1:{i:0;i:20;}s:5:"field";s:16:"term_taxonomy_id";s:8:"operator";s:6:"NOT IN";s:16:"include_children";b:1;}}s:8:"relation";s:3:"AND";s:16:"\0*\0table_aliases";a:0:{}s:13:"queried_terms";a:0:{}s:13:"primary_table";s:8:"wp_posts";s:17:"primary_id_column";s:2:"ID";}s:10:"meta_query";O:13:"WP_Meta_Query":9:{s:7:"queries";a:0:{}s:8:"relation";N;s:10:"meta_table";N;s:14:"meta_id_column";N;s:13:"primary_table";N;s:17:"primary_id_column";N;s:16:"\0*\0table_aliases";a:0:{}s:10:"\0*\0clauses";a:0:{}s:18:"\0*\0has_or_relation";b:0;}s:10:"date_query";b:0;s:7:"request";s:330:"SELECT SQL_CALC_FOUND_ROWS  wp_posts.ID FROM wp_posts  WHERE 1=1  AND ( \n  wp_posts.ID NOT IN (\n				SELECT object_id\n				FROM wp_term_relationships\n				WHERE term_taxonomy_id IN (20)\n			)\n) AND wp_posts.post_type = \'product\' AND ((wp_posts.post_status = \'publish\')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT 0, 4";s:5:"posts";a:1:{i:0;O:7:"WP_Post":24:{s:2:"ID";i:70;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-04-04 20:55:29";s:13:"post_date_gmt";s:19:"2017-04-04 13:55:29";s:12:"post_content";s:126:"Massage cơ và các huyệt đạo với 6 con lăn. Tích hợp chức năng nhiệt nhẹ nhàng bằng tia hồng ngoại.";s:10:"post_title";s:32:"Massage lưng vai gáy - Mas 632";s:12:"post_excerpt";s:126:"Massage cơ và các huyệt đạo với 6 con lăn. Tích hợp chức năng nhiệt nhẹ nhàng bằng tia hồng ngoại.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:28:"massage-lung-vai-gay-mas-632";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-04-04 20:56:01";s:17:"post_modified_gmt";s:19:"2017-04-04 13:56:01";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:59:"http://thang.dev/thiet-bi-y-te/?post_type=product&#038;p=70";s:10:"menu_order";i:0;s:9:"post_type";s:7:"product";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}s:10:"post_count";i:1;s:12:"current_post";i:-1;s:11:"in_the_loop";b:0;s:4:"post";r:119;s:13:"comment_count";i:0;s:15:"current_comment";i:-1;s:11:"found_posts";s:1:"1";s:13:"max_num_pages";d:1;s:21:"max_num_comment_pages";i:0;s:9:"is_single";b:0;s:10:"is_preview";b:0;s:7:"is_page";b:0;s:10:"is_archive";b:1;s:7:"is_date";b:0;s:7:"is_year";b:0;s:8:"is_month";b:0;s:6:"is_day";b:0;s:7:"is_time";b:0;s:9:"is_author";b:0;s:11:"is_category";b:0;s:6:"is_tag";b:0;s:6:"is_tax";b:0;s:9:"is_search";b:0;s:7:"is_feed";b:0;s:15:"is_comment_feed";b:0;s:12:"is_trackback";b:0;s:7:"is_home";b:0;s:6:"is_404";b:0;s:8:"is_embed";b:0;s:8:"is_paged";b:0;s:8:"is_admin";b:0;s:13:"is_attachment";b:0;s:11:"is_singular";b:0;s:9:"is_robots";b:0;s:13:"is_posts_page";b:0;s:20:"is_post_type_archive";b:1;s:25:"\0WP_Query\0query_vars_hash";s:32:"d5ab2573f161e5a36c1942c075a00041";s:28:"\0WP_Query\0query_vars_changed";b:0;s:17:"thumbnails_cached";b:0;s:19:"\0WP_Query\0stopwords";N;s:23:"\0WP_Query\0compat_fields";a:2:{i:0;s:15:"query_vars_hash";i:1;s:18:"query_vars_changed";}s:24:"\0WP_Query\0compat_methods";a:2:{i:0;s:16:"init_query_flags";i:1;s:15:"parse_tax_query";}}', 'no'),
(1105, '_transient_timeout_wc_low_stock_count', '1493910614', 'no'),
(1106, '_transient_wc_low_stock_count', '0', 'no'),
(1107, '_transient_timeout_wc_outofstock_count', '1493910614', 'no'),
(1108, '_transient_wc_outofstock_count', '0', 'no'),
(1109, '_site_transient_timeout_available_translations', '1491330014', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1110, '_site_transient_available_translations', 'a:108:{s:2:"af";a:8:{s:8:"language";s:2:"af";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-27 04:32:49";s:12:"english_name";s:9:"Afrikaans";s:11:"native_name";s:9:"Afrikaans";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/af.zip";s:3:"iso";a:2:{i:1;s:2:"af";i:2;s:3:"afr";}s:7:"strings";a:1:{s:8:"continue";s:10:"Gaan voort";}}s:3:"ary";a:8:{s:8:"language";s:3:"ary";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:42:35";s:12:"english_name";s:15:"Moroccan Arabic";s:11:"native_name";s:31:"العربية المغربية";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.3/ary.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:3;s:3:"ary";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:2:"ar";a:8:{s:8:"language";s:2:"ar";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:49:08";s:12:"english_name";s:6:"Arabic";s:11:"native_name";s:14:"العربية";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/ar.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:2;s:3:"ara";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:2:"as";a:8:{s:8:"language";s:2:"as";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-22 18:59:07";s:12:"english_name";s:8:"Assamese";s:11:"native_name";s:21:"অসমীয়া";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/as.zip";s:3:"iso";a:3:{i:1;s:2:"as";i:2;s:3:"asm";i:3;s:3:"asm";}s:7:"strings";a:1:{s:8:"continue";s:0:"";}}s:3:"azb";a:8:{s:8:"language";s:3:"azb";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-12 20:34:31";s:12:"english_name";s:17:"South Azerbaijani";s:11:"native_name";s:29:"گؤنئی آذربایجان";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:3;s:3:"azb";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:2:"az";a:8:{s:8:"language";s:2:"az";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-06 00:09:27";s:12:"english_name";s:11:"Azerbaijani";s:11:"native_name";s:16:"Azərbaycan dili";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/az.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:2;s:3:"aze";}s:7:"strings";a:1:{s:8:"continue";s:5:"Davam";}}s:3:"bel";a:8:{s:8:"language";s:3:"bel";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-23 04:36:52";s:12:"english_name";s:10:"Belarusian";s:11:"native_name";s:29:"Беларуская мова";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.3/bel.zip";s:3:"iso";a:2:{i:1;s:2:"be";i:2;s:3:"bel";}s:7:"strings";a:1:{s:8:"continue";s:20:"Працягнуць";}}s:5:"bg_BG";a:8:{s:8:"language";s:5:"bg_BG";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-06 09:18:57";s:12:"english_name";s:9:"Bulgarian";s:11:"native_name";s:18:"Български";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/bg_BG.zip";s:3:"iso";a:2:{i:1;s:2:"bg";i:2;s:3:"bul";}s:7:"strings";a:1:{s:8:"continue";s:12:"Напред";}}s:5:"bn_BD";a:8:{s:8:"language";s:5:"bn_BD";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-04 16:58:43";s:12:"english_name";s:7:"Bengali";s:11:"native_name";s:15:"বাংলা";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/bn_BD.zip";s:3:"iso";a:1:{i:1;s:2:"bn";}s:7:"strings";a:1:{s:8:"continue";s:23:"এগিয়ে চল.";}}s:2:"bo";a:8:{s:8:"language";s:2:"bo";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-05 09:44:12";s:12:"english_name";s:7:"Tibetan";s:11:"native_name";s:21:"བོད་ཡིག";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/bo.zip";s:3:"iso";a:2:{i:1;s:2:"bo";i:2;s:3:"tib";}s:7:"strings";a:1:{s:8:"continue";s:24:"མུ་མཐུད།";}}s:5:"bs_BA";a:8:{s:8:"language";s:5:"bs_BA";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-04 20:20:28";s:12:"english_name";s:7:"Bosnian";s:11:"native_name";s:8:"Bosanski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/bs_BA.zip";s:3:"iso";a:2:{i:1;s:2:"bs";i:2;s:3:"bos";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:2:"ca";a:8:{s:8:"language";s:2:"ca";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-05 11:34:47";s:12:"english_name";s:7:"Catalan";s:11:"native_name";s:7:"Català";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/ca.zip";s:3:"iso";a:2:{i:1;s:2:"ca";i:2;s:3:"cat";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:3:"ceb";a:8:{s:8:"language";s:3:"ceb";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-03-02 17:25:51";s:12:"english_name";s:7:"Cebuano";s:11:"native_name";s:7:"Cebuano";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip";s:3:"iso";a:2:{i:2;s:3:"ceb";i:3;s:3:"ceb";}s:7:"strings";a:1:{s:8:"continue";s:7:"Padayun";}}s:5:"cs_CZ";a:8:{s:8:"language";s:5:"cs_CZ";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-12 08:46:26";s:12:"english_name";s:5:"Czech";s:11:"native_name";s:12:"Čeština‎";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/cs_CZ.zip";s:3:"iso";a:2:{i:1;s:2:"cs";i:2;s:3:"ces";}s:7:"strings";a:1:{s:8:"continue";s:11:"Pokračovat";}}s:2:"cy";a:8:{s:8:"language";s:2:"cy";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:49:29";s:12:"english_name";s:5:"Welsh";s:11:"native_name";s:7:"Cymraeg";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/cy.zip";s:3:"iso";a:2:{i:1;s:2:"cy";i:2;s:3:"cym";}s:7:"strings";a:1:{s:8:"continue";s:6:"Parhau";}}s:5:"da_DK";a:8:{s:8:"language";s:5:"da_DK";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-02-28 00:33:54";s:12:"english_name";s:6:"Danish";s:11:"native_name";s:5:"Dansk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/da_DK.zip";s:3:"iso";a:2:{i:1;s:2:"da";i:2;s:3:"dan";}s:7:"strings";a:1:{s:8:"continue";s:12:"Forts&#230;t";}}s:5:"de_DE";a:8:{s:8:"language";s:5:"de_DE";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-18 13:57:42";s:12:"english_name";s:6:"German";s:11:"native_name";s:7:"Deutsch";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/de_DE.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:12:"de_DE_formal";a:8:{s:8:"language";s:12:"de_DE_formal";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-18 13:57:53";s:12:"english_name";s:15:"German (Formal)";s:11:"native_name";s:13:"Deutsch (Sie)";s:7:"package";s:71:"https://downloads.wordpress.org/translation/core/4.7.3/de_DE_formal.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:14:"de_CH_informal";a:8:{s:8:"language";s:14:"de_CH_informal";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:39:59";s:12:"english_name";s:30:"German (Switzerland, Informal)";s:11:"native_name";s:21:"Deutsch (Schweiz, Du)";s:7:"package";s:73:"https://downloads.wordpress.org/translation/core/4.7.3/de_CH_informal.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:5:"de_CH";a:8:{s:8:"language";s:5:"de_CH";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:40:03";s:12:"english_name";s:20:"German (Switzerland)";s:11:"native_name";s:17:"Deutsch (Schweiz)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/de_CH.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:3:"dzo";a:8:{s:8:"language";s:3:"dzo";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-06-29 08:59:03";s:12:"english_name";s:8:"Dzongkha";s:11:"native_name";s:18:"རྫོང་ཁ";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip";s:3:"iso";a:2:{i:1;s:2:"dz";i:2;s:3:"dzo";}s:7:"strings";a:1:{s:8:"continue";s:0:"";}}s:2:"el";a:8:{s:8:"language";s:2:"el";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-30 00:08:09";s:12:"english_name";s:5:"Greek";s:11:"native_name";s:16:"Ελληνικά";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/el.zip";s:3:"iso";a:2:{i:1;s:2:"el";i:2;s:3:"ell";}s:7:"strings";a:1:{s:8:"continue";s:16:"Συνέχεια";}}s:5:"en_NZ";a:8:{s:8:"language";s:5:"en_NZ";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:54:30";s:12:"english_name";s:21:"English (New Zealand)";s:11:"native_name";s:21:"English (New Zealand)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/en_NZ.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_CA";a:8:{s:8:"language";s:5:"en_CA";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:49:34";s:12:"english_name";s:16:"English (Canada)";s:11:"native_name";s:16:"English (Canada)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/en_CA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_AU";a:8:{s:8:"language";s:5:"en_AU";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-27 00:40:28";s:12:"english_name";s:19:"English (Australia)";s:11:"native_name";s:19:"English (Australia)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/en_AU.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_GB";a:8:{s:8:"language";s:5:"en_GB";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-28 03:10:25";s:12:"english_name";s:12:"English (UK)";s:11:"native_name";s:12:"English (UK)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/en_GB.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_ZA";a:8:{s:8:"language";s:5:"en_ZA";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:53:43";s:12:"english_name";s:22:"English (South Africa)";s:11:"native_name";s:22:"English (South Africa)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/en_ZA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:2:"eo";a:8:{s:8:"language";s:2:"eo";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:47:07";s:12:"english_name";s:9:"Esperanto";s:11:"native_name";s:9:"Esperanto";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/eo.zip";s:3:"iso";a:2:{i:1;s:2:"eo";i:2;s:3:"epo";}s:7:"strings";a:1:{s:8:"continue";s:8:"Daŭrigi";}}s:5:"es_VE";a:8:{s:8:"language";s:5:"es_VE";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:53:56";s:12:"english_name";s:19:"Spanish (Venezuela)";s:11:"native_name";s:21:"Español de Venezuela";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/es_VE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_ES";a:8:{s:8:"language";s:5:"es_ES";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-15 12:53:17";s:12:"english_name";s:15:"Spanish (Spain)";s:11:"native_name";s:8:"Español";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/es_ES.zip";s:3:"iso";a:1:{i:1;s:2:"es";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_PE";a:8:{s:8:"language";s:5:"es_PE";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-09 09:36:22";s:12:"english_name";s:14:"Spanish (Peru)";s:11:"native_name";s:17:"Español de Perú";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/es_PE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CL";a:8:{s:8:"language";s:5:"es_CL";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-28 20:09:49";s:12:"english_name";s:15:"Spanish (Chile)";s:11:"native_name";s:17:"Español de Chile";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/es_CL.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_AR";a:8:{s:8:"language";s:5:"es_AR";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:41:31";s:12:"english_name";s:19:"Spanish (Argentina)";s:11:"native_name";s:21:"Español de Argentina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/es_AR.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CO";a:8:{s:8:"language";s:5:"es_CO";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:54:37";s:12:"english_name";s:18:"Spanish (Colombia)";s:11:"native_name";s:20:"Español de Colombia";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/es_CO.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_GT";a:8:{s:8:"language";s:5:"es_GT";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:54:37";s:12:"english_name";s:19:"Spanish (Guatemala)";s:11:"native_name";s:21:"Español de Guatemala";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/es_GT.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_MX";a:8:{s:8:"language";s:5:"es_MX";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:42:28";s:12:"english_name";s:16:"Spanish (Mexico)";s:11:"native_name";s:19:"Español de México";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/es_MX.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"et";a:8:{s:8:"language";s:2:"et";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-27 16:37:11";s:12:"english_name";s:8:"Estonian";s:11:"native_name";s:5:"Eesti";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/et.zip";s:3:"iso";a:2:{i:1;s:2:"et";i:2;s:3:"est";}s:7:"strings";a:1:{s:8:"continue";s:6:"Jätka";}}s:2:"eu";a:8:{s:8:"language";s:2:"eu";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:54:33";s:12:"english_name";s:6:"Basque";s:11:"native_name";s:7:"Euskara";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/eu.zip";s:3:"iso";a:2:{i:1;s:2:"eu";i:2;s:3:"eus";}s:7:"strings";a:1:{s:8:"continue";s:8:"Jarraitu";}}s:5:"fa_IR";a:8:{s:8:"language";s:5:"fa_IR";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-02-02 15:21:03";s:12:"english_name";s:7:"Persian";s:11:"native_name";s:10:"فارسی";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/fa_IR.zip";s:3:"iso";a:2:{i:1;s:2:"fa";i:2;s:3:"fas";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:2:"fi";a:8:{s:8:"language";s:2:"fi";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:42:25";s:12:"english_name";s:7:"Finnish";s:11:"native_name";s:5:"Suomi";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/fi.zip";s:3:"iso";a:2:{i:1;s:2:"fi";i:2;s:3:"fin";}s:7:"strings";a:1:{s:8:"continue";s:5:"Jatka";}}s:5:"fr_FR";a:8:{s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-18 16:05:09";s:12:"english_name";s:15:"French (France)";s:11:"native_name";s:9:"Français";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/fr_FR.zip";s:3:"iso";a:1:{i:1;s:2:"fr";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:5:"fr_BE";a:8:{s:8:"language";s:5:"fr_BE";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:40:32";s:12:"english_name";s:16:"French (Belgium)";s:11:"native_name";s:21:"Français de Belgique";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/fr_BE.zip";s:3:"iso";a:2:{i:1;s:2:"fr";i:2;s:3:"fra";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:5:"fr_CA";a:8:{s:8:"language";s:5:"fr_CA";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-02-03 21:08:25";s:12:"english_name";s:15:"French (Canada)";s:11:"native_name";s:19:"Français du Canada";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/fr_CA.zip";s:3:"iso";a:2:{i:1;s:2:"fr";i:2;s:3:"fra";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:2:"gd";a:8:{s:8:"language";s:2:"gd";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-08-23 17:41:37";s:12:"english_name";s:15:"Scottish Gaelic";s:11:"native_name";s:9:"Gàidhlig";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip";s:3:"iso";a:3:{i:1;s:2:"gd";i:2;s:3:"gla";i:3;s:3:"gla";}s:7:"strings";a:1:{s:8:"continue";s:15:"Lean air adhart";}}s:5:"gl_ES";a:8:{s:8:"language";s:5:"gl_ES";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:40:27";s:12:"english_name";s:8:"Galician";s:11:"native_name";s:6:"Galego";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/gl_ES.zip";s:3:"iso";a:2:{i:1;s:2:"gl";i:2;s:3:"glg";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"gu";a:8:{s:8:"language";s:2:"gu";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-02-07 18:47:03";s:12:"english_name";s:8:"Gujarati";s:11:"native_name";s:21:"ગુજરાતી";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/gu.zip";s:3:"iso";a:2:{i:1;s:2:"gu";i:2;s:3:"guj";}s:7:"strings";a:1:{s:8:"continue";s:31:"ચાલુ રાખવું";}}s:3:"haz";a:8:{s:8:"language";s:3:"haz";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-12-05 00:59:09";s:12:"english_name";s:8:"Hazaragi";s:11:"native_name";s:15:"هزاره گی";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip";s:3:"iso";a:1:{i:3;s:3:"haz";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:5:"he_IL";a:8:{s:8:"language";s:5:"he_IL";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-29 21:21:10";s:12:"english_name";s:6:"Hebrew";s:11:"native_name";s:16:"עִבְרִית";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/he_IL.zip";s:3:"iso";a:1:{i:1;s:2:"he";}s:7:"strings";a:1:{s:8:"continue";s:8:"המשך";}}s:5:"hi_IN";a:8:{s:8:"language";s:5:"hi_IN";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-03 12:18:25";s:12:"english_name";s:5:"Hindi";s:11:"native_name";s:18:"हिन्दी";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/hi_IN.zip";s:3:"iso";a:2:{i:1;s:2:"hi";i:2;s:3:"hin";}s:7:"strings";a:1:{s:8:"continue";s:12:"जारी";}}s:2:"hr";a:8:{s:8:"language";s:2:"hr";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-28 13:34:22";s:12:"english_name";s:8:"Croatian";s:11:"native_name";s:8:"Hrvatski";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/hr.zip";s:3:"iso";a:2:{i:1;s:2:"hr";i:2;s:3:"hrv";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:5:"hu_HU";a:8:{s:8:"language";s:5:"hu_HU";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:48:39";s:12:"english_name";s:9:"Hungarian";s:11:"native_name";s:6:"Magyar";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/hu_HU.zip";s:3:"iso";a:2:{i:1;s:2:"hu";i:2;s:3:"hun";}s:7:"strings";a:1:{s:8:"continue";s:10:"Folytatás";}}s:2:"hy";a:8:{s:8:"language";s:2:"hy";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-12-03 16:21:10";s:12:"english_name";s:8:"Armenian";s:11:"native_name";s:14:"Հայերեն";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip";s:3:"iso";a:2:{i:1;s:2:"hy";i:2;s:3:"hye";}s:7:"strings";a:1:{s:8:"continue";s:20:"Շարունակել";}}s:5:"id_ID";a:8:{s:8:"language";s:5:"id_ID";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-04-03 02:27:45";s:12:"english_name";s:10:"Indonesian";s:11:"native_name";s:16:"Bahasa Indonesia";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/id_ID.zip";s:3:"iso";a:2:{i:1;s:2:"id";i:2;s:3:"ind";}s:7:"strings";a:1:{s:8:"continue";s:9:"Lanjutkan";}}s:5:"is_IS";a:8:{s:8:"language";s:5:"is_IS";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-02-16 13:36:46";s:12:"english_name";s:9:"Icelandic";s:11:"native_name";s:9:"Íslenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/is_IS.zip";s:3:"iso";a:2:{i:1;s:2:"is";i:2;s:3:"isl";}s:7:"strings";a:1:{s:8:"continue";s:6:"Áfram";}}s:5:"it_IT";a:8:{s:8:"language";s:5:"it_IT";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-04-03 15:49:35";s:12:"english_name";s:7:"Italian";s:11:"native_name";s:8:"Italiano";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/it_IT.zip";s:3:"iso";a:2:{i:1;s:2:"it";i:2;s:3:"ita";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"ja";a:8:{s:8:"language";s:2:"ja";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-29 14:23:06";s:12:"english_name";s:8:"Japanese";s:11:"native_name";s:9:"日本語";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/ja.zip";s:3:"iso";a:1:{i:1;s:2:"ja";}s:7:"strings";a:1:{s:8:"continue";s:9:"続ける";}}s:5:"ka_GE";a:8:{s:8:"language";s:5:"ka_GE";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-04-03 08:43:01";s:12:"english_name";s:8:"Georgian";s:11:"native_name";s:21:"ქართული";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/ka_GE.zip";s:3:"iso";a:2:{i:1;s:2:"ka";i:2;s:3:"kat";}s:7:"strings";a:1:{s:8:"continue";s:30:"გაგრძელება";}}s:3:"kab";a:8:{s:8:"language";s:3:"kab";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:39:13";s:12:"english_name";s:6:"Kabyle";s:11:"native_name";s:9:"Taqbaylit";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/kab.zip";s:3:"iso";a:2:{i:2;s:3:"kab";i:3;s:3:"kab";}s:7:"strings";a:1:{s:8:"continue";s:6:"Kemmel";}}s:2:"km";a:8:{s:8:"language";s:2:"km";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-12-07 02:07:59";s:12:"english_name";s:5:"Khmer";s:11:"native_name";s:27:"ភាសាខ្មែរ";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/km.zip";s:3:"iso";a:2:{i:1;s:2:"km";i:2;s:3:"khm";}s:7:"strings";a:1:{s:8:"continue";s:12:"បន្ត";}}s:5:"ko_KR";a:8:{s:8:"language";s:5:"ko_KR";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-04-04 05:43:51";s:12:"english_name";s:6:"Korean";s:11:"native_name";s:9:"한국어";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/ko_KR.zip";s:3:"iso";a:2:{i:1;s:2:"ko";i:2;s:3:"kor";}s:7:"strings";a:1:{s:8:"continue";s:6:"계속";}}s:3:"ckb";a:8:{s:8:"language";s:3:"ckb";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:48:25";s:12:"english_name";s:16:"Kurdish (Sorani)";s:11:"native_name";s:13:"كوردی‎";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/ckb.zip";s:3:"iso";a:2:{i:1;s:2:"ku";i:3;s:3:"ckb";}s:7:"strings";a:1:{s:8:"continue";s:30:"به‌رده‌وام به‌";}}s:2:"lo";a:8:{s:8:"language";s:2:"lo";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-12 09:59:23";s:12:"english_name";s:3:"Lao";s:11:"native_name";s:21:"ພາສາລາວ";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip";s:3:"iso";a:2:{i:1;s:2:"lo";i:2;s:3:"lao";}s:7:"strings";a:1:{s:8:"continue";s:18:"ຕໍ່​ໄປ";}}s:5:"lt_LT";a:8:{s:8:"language";s:5:"lt_LT";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-30 09:46:13";s:12:"english_name";s:10:"Lithuanian";s:11:"native_name";s:15:"Lietuvių kalba";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/lt_LT.zip";s:3:"iso";a:2:{i:1;s:2:"lt";i:2;s:3:"lit";}s:7:"strings";a:1:{s:8:"continue";s:6:"Tęsti";}}s:2:"lv";a:8:{s:8:"language";s:2:"lv";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-17 20:40:40";s:12:"english_name";s:7:"Latvian";s:11:"native_name";s:16:"Latviešu valoda";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/lv.zip";s:3:"iso";a:2:{i:1;s:2:"lv";i:2;s:3:"lav";}s:7:"strings";a:1:{s:8:"continue";s:9:"Turpināt";}}s:5:"mk_MK";a:8:{s:8:"language";s:5:"mk_MK";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:54:41";s:12:"english_name";s:10:"Macedonian";s:11:"native_name";s:31:"Македонски јазик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/mk_MK.zip";s:3:"iso";a:2:{i:1;s:2:"mk";i:2;s:3:"mkd";}s:7:"strings";a:1:{s:8:"continue";s:16:"Продолжи";}}s:5:"ml_IN";a:8:{s:8:"language";s:5:"ml_IN";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-27 03:43:32";s:12:"english_name";s:9:"Malayalam";s:11:"native_name";s:18:"മലയാളം";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip";s:3:"iso";a:2:{i:1;s:2:"ml";i:2;s:3:"mal";}s:7:"strings";a:1:{s:8:"continue";s:18:"തുടരുക";}}s:2:"mn";a:8:{s:8:"language";s:2:"mn";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-12 07:29:35";s:12:"english_name";s:9:"Mongolian";s:11:"native_name";s:12:"Монгол";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip";s:3:"iso";a:2:{i:1;s:2:"mn";i:2;s:3:"mon";}s:7:"strings";a:1:{s:8:"continue";s:24:"Үргэлжлүүлэх";}}s:2:"mr";a:8:{s:8:"language";s:2:"mr";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-24 06:52:11";s:12:"english_name";s:7:"Marathi";s:11:"native_name";s:15:"मराठी";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/mr.zip";s:3:"iso";a:2:{i:1;s:2:"mr";i:2;s:3:"mar";}s:7:"strings";a:1:{s:8:"continue";s:25:"सुरु ठेवा";}}s:5:"ms_MY";a:8:{s:8:"language";s:5:"ms_MY";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-05 09:45:10";s:12:"english_name";s:5:"Malay";s:11:"native_name";s:13:"Bahasa Melayu";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/ms_MY.zip";s:3:"iso";a:2:{i:1;s:2:"ms";i:2;s:3:"msa";}s:7:"strings";a:1:{s:8:"continue";s:8:"Teruskan";}}s:5:"my_MM";a:8:{s:8:"language";s:5:"my_MM";s:7:"version";s:6:"4.1.16";s:7:"updated";s:19:"2015-03-26 15:57:42";s:12:"english_name";s:17:"Myanmar (Burmese)";s:11:"native_name";s:15:"ဗမာစာ";s:7:"package";s:65:"https://downloads.wordpress.org/translation/core/4.1.16/my_MM.zip";s:3:"iso";a:2:{i:1;s:2:"my";i:2;s:3:"mya";}s:7:"strings";a:1:{s:8:"continue";s:54:"ဆက်လက်လုပ်ဆောင်ပါ။";}}s:5:"nb_NO";a:8:{s:8:"language";s:5:"nb_NO";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:42:31";s:12:"english_name";s:19:"Norwegian (Bokmål)";s:11:"native_name";s:13:"Norsk bokmål";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/nb_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nb";i:2;s:3:"nob";}s:7:"strings";a:1:{s:8:"continue";s:8:"Fortsett";}}s:5:"ne_NP";a:8:{s:8:"language";s:5:"ne_NP";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:48:31";s:12:"english_name";s:6:"Nepali";s:11:"native_name";s:18:"नेपाली";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ne_NP.zip";s:3:"iso";a:2:{i:1;s:2:"ne";i:2;s:3:"nep";}s:7:"strings";a:1:{s:8:"continue";s:43:"जारी राख्नुहोस्";}}s:12:"nl_NL_formal";a:8:{s:8:"language";s:12:"nl_NL_formal";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-02-16 13:24:21";s:12:"english_name";s:14:"Dutch (Formal)";s:11:"native_name";s:20:"Nederlands (Formeel)";s:7:"package";s:71:"https://downloads.wordpress.org/translation/core/4.7.3/nl_NL_formal.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nl_NL";a:8:{s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-04-03 14:07:21";s:12:"english_name";s:5:"Dutch";s:11:"native_name";s:10:"Nederlands";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/nl_NL.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nl_BE";a:8:{s:8:"language";s:5:"nl_BE";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-21 11:39:51";s:12:"english_name";s:15:"Dutch (Belgium)";s:11:"native_name";s:20:"Nederlands (België)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/nl_BE.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nn_NO";a:8:{s:8:"language";s:5:"nn_NO";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:40:57";s:12:"english_name";s:19:"Norwegian (Nynorsk)";s:11:"native_name";s:13:"Norsk nynorsk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/nn_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nn";i:2;s:3:"nno";}s:7:"strings";a:1:{s:8:"continue";s:9:"Hald fram";}}s:3:"oci";a:8:{s:8:"language";s:3:"oci";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-02 13:47:38";s:12:"english_name";s:7:"Occitan";s:11:"native_name";s:7:"Occitan";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/oci.zip";s:3:"iso";a:2:{i:1;s:2:"oc";i:2;s:3:"oci";}s:7:"strings";a:1:{s:8:"continue";s:9:"Contunhar";}}s:5:"pa_IN";a:8:{s:8:"language";s:5:"pa_IN";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-16 05:19:43";s:12:"english_name";s:7:"Punjabi";s:11:"native_name";s:18:"ਪੰਜਾਬੀ";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip";s:3:"iso";a:2:{i:1;s:2:"pa";i:2;s:3:"pan";}s:7:"strings";a:1:{s:8:"continue";s:25:"ਜਾਰੀ ਰੱਖੋ";}}s:5:"pl_PL";a:8:{s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-29 09:34:25";s:12:"english_name";s:6:"Polish";s:11:"native_name";s:6:"Polski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/pl_PL.zip";s:3:"iso";a:2:{i:1;s:2:"pl";i:2;s:3:"pol";}s:7:"strings";a:1:{s:8:"continue";s:9:"Kontynuuj";}}s:2:"ps";a:8:{s:8:"language";s:2:"ps";s:7:"version";s:6:"4.1.16";s:7:"updated";s:19:"2015-03-29 22:19:48";s:12:"english_name";s:6:"Pashto";s:11:"native_name";s:8:"پښتو";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.1.16/ps.zip";s:3:"iso";a:2:{i:1;s:2:"ps";i:2;s:3:"pus";}s:7:"strings";a:1:{s:8:"continue";s:19:"دوام ورکړه";}}s:5:"pt_BR";a:8:{s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-16 03:50:08";s:12:"english_name";s:19:"Portuguese (Brazil)";s:11:"native_name";s:20:"Português do Brasil";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/pt_BR.zip";s:3:"iso";a:2:{i:1;s:2:"pt";i:2;s:3:"por";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"pt_PT";a:8:{s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-24 10:09:34";s:12:"english_name";s:21:"Portuguese (Portugal)";s:11:"native_name";s:10:"Português";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/pt_PT.zip";s:3:"iso";a:1:{i:1;s:2:"pt";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:3:"rhg";a:8:{s:8:"language";s:3:"rhg";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-03-16 13:03:18";s:12:"english_name";s:8:"Rohingya";s:11:"native_name";s:8:"Ruáinga";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip";s:3:"iso";a:1:{i:3;s:3:"rhg";}s:7:"strings";a:1:{s:8:"continue";s:0:"";}}s:5:"ro_RO";a:8:{s:8:"language";s:5:"ro_RO";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-04-03 17:26:22";s:12:"english_name";s:8:"Romanian";s:11:"native_name";s:8:"Română";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/ro_RO.zip";s:3:"iso";a:2:{i:1;s:2:"ro";i:2;s:3:"ron";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuă";}}s:5:"ru_RU";a:8:{s:8:"language";s:5:"ru_RU";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-13 19:43:03";s:12:"english_name";s:7:"Russian";s:11:"native_name";s:14:"Русский";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/ru_RU.zip";s:3:"iso";a:2:{i:1;s:2:"ru";i:2;s:3:"rus";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продолжить";}}s:3:"sah";a:8:{s:8:"language";s:3:"sah";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-21 02:06:41";s:12:"english_name";s:5:"Sakha";s:11:"native_name";s:14:"Сахалыы";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip";s:3:"iso";a:2:{i:2;s:3:"sah";i:3;s:3:"sah";}s:7:"strings";a:1:{s:8:"continue";s:12:"Салҕаа";}}s:5:"si_LK";a:8:{s:8:"language";s:5:"si_LK";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-12 06:00:52";s:12:"english_name";s:7:"Sinhala";s:11:"native_name";s:15:"සිංහල";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip";s:3:"iso";a:2:{i:1;s:2:"si";i:2;s:3:"sin";}s:7:"strings";a:1:{s:8:"continue";s:44:"දිගටම කරගෙන යන්න";}}s:5:"sk_SK";a:8:{s:8:"language";s:5:"sk_SK";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-24 12:22:25";s:12:"english_name";s:6:"Slovak";s:11:"native_name";s:11:"Slovenčina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/sk_SK.zip";s:3:"iso";a:2:{i:1;s:2:"sk";i:2;s:3:"slk";}s:7:"strings";a:1:{s:8:"continue";s:12:"Pokračovať";}}s:5:"sl_SI";a:8:{s:8:"language";s:5:"sl_SI";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-02-08 17:57:45";s:12:"english_name";s:9:"Slovenian";s:11:"native_name";s:13:"Slovenščina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/sl_SI.zip";s:3:"iso";a:2:{i:1;s:2:"sl";i:2;s:3:"slv";}s:7:"strings";a:1:{s:8:"continue";s:8:"Nadaljuj";}}s:2:"sq";a:8:{s:8:"language";s:2:"sq";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-29 18:17:50";s:12:"english_name";s:8:"Albanian";s:11:"native_name";s:5:"Shqip";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/sq.zip";s:3:"iso";a:2:{i:1;s:2:"sq";i:2;s:3:"sqi";}s:7:"strings";a:1:{s:8:"continue";s:6:"Vazhdo";}}s:5:"sr_RS";a:8:{s:8:"language";s:5:"sr_RS";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:41:03";s:12:"english_name";s:7:"Serbian";s:11:"native_name";s:23:"Српски језик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/sr_RS.zip";s:3:"iso";a:2:{i:1;s:2:"sr";i:2;s:3:"srp";}s:7:"strings";a:1:{s:8:"continue";s:14:"Настави";}}s:5:"sv_SE";a:8:{s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-04-03 00:34:10";s:12:"english_name";s:7:"Swedish";s:11:"native_name";s:7:"Svenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/sv_SE.zip";s:3:"iso";a:2:{i:1;s:2:"sv";i:2;s:3:"swe";}s:7:"strings";a:1:{s:8:"continue";s:9:"Fortsätt";}}s:3:"szl";a:8:{s:8:"language";s:3:"szl";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-24 19:58:14";s:12:"english_name";s:8:"Silesian";s:11:"native_name";s:17:"Ślōnskŏ gŏdka";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip";s:3:"iso";a:1:{i:3;s:3:"szl";}s:7:"strings";a:1:{s:8:"continue";s:13:"Kōntynuować";}}s:5:"ta_IN";a:8:{s:8:"language";s:5:"ta_IN";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-27 03:22:47";s:12:"english_name";s:5:"Tamil";s:11:"native_name";s:15:"தமிழ்";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip";s:3:"iso";a:2:{i:1;s:2:"ta";i:2;s:3:"tam";}s:7:"strings";a:1:{s:8:"continue";s:24:"தொடரவும்";}}s:2:"te";a:8:{s:8:"language";s:2:"te";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:47:39";s:12:"english_name";s:6:"Telugu";s:11:"native_name";s:18:"తెలుగు";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/te.zip";s:3:"iso";a:2:{i:1;s:2:"te";i:2;s:3:"tel";}s:7:"strings";a:1:{s:8:"continue";s:30:"కొనసాగించు";}}s:2:"th";a:8:{s:8:"language";s:2:"th";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:48:43";s:12:"english_name";s:4:"Thai";s:11:"native_name";s:9:"ไทย";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/th.zip";s:3:"iso";a:2:{i:1;s:2:"th";i:2;s:3:"tha";}s:7:"strings";a:1:{s:8:"continue";s:15:"ต่อไป";}}s:2:"tl";a:8:{s:8:"language";s:2:"tl";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-12-30 02:38:08";s:12:"english_name";s:7:"Tagalog";s:11:"native_name";s:7:"Tagalog";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/tl.zip";s:3:"iso";a:2:{i:1;s:2:"tl";i:2;s:3:"tgl";}s:7:"strings";a:1:{s:8:"continue";s:10:"Magpatuloy";}}s:5:"tr_TR";a:8:{s:8:"language";s:5:"tr_TR";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-02-17 11:46:52";s:12:"english_name";s:7:"Turkish";s:11:"native_name";s:8:"Türkçe";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/tr_TR.zip";s:3:"iso";a:2:{i:1;s:2:"tr";i:2;s:3:"tur";}s:7:"strings";a:1:{s:8:"continue";s:5:"Devam";}}s:5:"tt_RU";a:8:{s:8:"language";s:5:"tt_RU";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-20 20:20:50";s:12:"english_name";s:5:"Tatar";s:11:"native_name";s:19:"Татар теле";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip";s:3:"iso";a:2:{i:1;s:2:"tt";i:2;s:3:"tat";}s:7:"strings";a:1:{s:8:"continue";s:17:"дәвам итү";}}s:3:"tah";a:8:{s:8:"language";s:3:"tah";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-03-06 18:39:39";s:12:"english_name";s:8:"Tahitian";s:11:"native_name";s:10:"Reo Tahiti";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip";s:3:"iso";a:3:{i:1;s:2:"ty";i:2;s:3:"tah";i:3;s:3:"tah";}s:7:"strings";a:1:{s:8:"continue";s:0:"";}}s:5:"ug_CN";a:8:{s:8:"language";s:5:"ug_CN";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-12-05 09:23:39";s:12:"english_name";s:6:"Uighur";s:11:"native_name";s:9:"Uyƣurqə";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ug_CN.zip";s:3:"iso";a:2:{i:1;s:2:"ug";i:2;s:3:"uig";}s:7:"strings";a:1:{s:8:"continue";s:26:"داۋاملاشتۇرۇش";}}s:2:"uk";a:8:{s:8:"language";s:2:"uk";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-28 21:21:58";s:12:"english_name";s:9:"Ukrainian";s:11:"native_name";s:20:"Українська";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/uk.zip";s:3:"iso";a:2:{i:1;s:2:"uk";i:2;s:3:"ukr";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продовжити";}}s:2:"ur";a:8:{s:8:"language";s:2:"ur";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-27 07:08:07";s:12:"english_name";s:4:"Urdu";s:11:"native_name";s:8:"اردو";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/ur.zip";s:3:"iso";a:2:{i:1;s:2:"ur";i:2;s:3:"urd";}s:7:"strings";a:1:{s:8:"continue";s:19:"جاری رکھیں";}}s:5:"uz_UZ";a:8:{s:8:"language";s:5:"uz_UZ";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-04-04 05:03:16";s:12:"english_name";s:5:"Uzbek";s:11:"native_name";s:11:"O‘zbekcha";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/uz_UZ.zip";s:3:"iso";a:2:{i:1;s:2:"uz";i:2;s:3:"uzb";}s:7:"strings";a:1:{s:8:"continue";s:11:"Davom etish";}}s:2:"vi";a:8:{s:8:"language";s:2:"vi";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-29 06:46:22";s:12:"english_name";s:10:"Vietnamese";s:11:"native_name";s:14:"Tiếng Việt";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/vi.zip";s:3:"iso";a:2:{i:1;s:2:"vi";i:2;s:3:"vie";}s:7:"strings";a:1:{s:8:"continue";s:12:"Tiếp tục";}}s:5:"zh_CN";a:8:{s:8:"language";s:5:"zh_CN";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-01-26 15:54:45";s:12:"english_name";s:15:"Chinese (China)";s:11:"native_name";s:12:"简体中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/zh_CN.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"继续";}}s:5:"zh_HK";a:8:{s:8:"language";s:5:"zh_HK";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-28 12:03:30";s:12:"english_name";s:19:"Chinese (Hong Kong)";s:11:"native_name";s:16:"香港中文版	";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/zh_HK.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}s:5:"zh_TW";a:8:{s:8:"language";s:5:"zh_TW";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-03-20 03:14:21";s:12:"english_name";s:16:"Chinese (Taiwan)";s:11:"native_name";s:12:"繁體中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.3/zh_TW.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}}', 'no'),
(1112, 'my_option_name', 'a:1:{s:7:"hotline";i:19008198;}', 'yes'),
(1125, 'company_info', 'a:1:{s:7:"hotline";s:23:"1988000990099,234234234";}', 'yes'),
(1126, '_transient_timeout_external_ip_address_127.0.0.1', '1492050281', 'no'),
(1127, '_transient_external_ip_address_127.0.0.1', '2405:4800:5a97:28f:c99e:ce17:5760:501c', 'no'),
(1136, '_site_transient_timeout_browser_96595946f790e951c5e30a51b06afa7d', '1492050398', 'no'),
(1137, '_site_transient_browser_96595946f790e951c5e30a51b06afa7d', 'a:9:{s:8:"platform";s:9:"Macintosh";s:4:"name";s:6:"Chrome";s:7:"version";s:13:"57.0.2987.133";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'no'),
(1138, '_transient_timeout_wc_report_sales_by_date', '1491531998', 'no'),
(1139, '_transient_wc_report_sales_by_date', 'a:7:{s:32:"7c6ebe603fa000cc9e573986c362298c";a:1:{i:0;O:8:"stdClass":2:{s:5:"count";s:1:"2";s:9:"post_date";s:19:"2017-04-02 09:25:51";}}s:32:"0ae8b6779895cea8153a0211c78968a7";a:0:{}s:32:"3ad8f5a4c81593ca3a0b4a07ebaa7ab8";a:1:{i:0;O:8:"stdClass":2:{s:16:"order_item_count";s:1:"5";s:9:"post_date";s:19:"2017-04-02 00:23:29";}}s:32:"e00b6e2a9467edbaee348be63f59242a";N;s:32:"28e315110fe4f994e7eb8988f329e916";a:1:{i:0;O:8:"stdClass":5:{s:11:"total_sales";s:7:"6012000";s:14:"total_shipping";s:1:"0";s:9:"total_tax";s:1:"0";s:18:"total_shipping_tax";s:1:"0";s:9:"post_date";s:19:"2017-04-02 00:23:29";}}s:32:"dd29141c7729e83c033dad5ee2718bac";a:0:{}s:32:"026376612591870607311d4cd4f9c7d0";a:0:{}}', 'no'),
(1140, '_transient_timeout_wc_admin_report', '1491531998', 'no'),
(1141, '_transient_wc_admin_report', 'a:2:{s:32:"24683333dc3856266cfab68524a467d9";a:1:{i:0;O:8:"stdClass":2:{s:15:"sparkline_value";s:7:"6012000";s:9:"post_date";s:19:"2017-04-02 00:23:29";}}s:32:"aff0786fa48e3c6fae6fb97062df7012";a:1:{i:0;O:8:"stdClass":3:{s:10:"product_id";s:2:"20";s:15:"sparkline_value";s:1:"3";s:9:"post_date";s:19:"2017-04-02 00:23:29";}}}', 'no'),
(1155, '_transient_timeout_jetpack_https_test', '1491532012', 'no'),
(1156, '_transient_jetpack_https_test', '1', 'no'),
(1169, 'wpforms_preview_page', '82', 'yes'),
(1170, 'wpforms_version', '1.3.6.1', 'yes'),
(1171, 'wpforms_activated', 'a:1:{s:4:"lite";i:1491446749;}', 'yes'),
(1174, 'widget_wpforms-widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(1175, 'wpforms_review', 'a:2:{s:4:"time";i:1491446751;s:9:"dismissed";b:0;}', 'yes'),
(1262, 'wpcf7', 'a:2:{s:7:"version";s:3:"4.7";s:13:"bulk_validate";a:4:{s:9:"timestamp";i:1491472869;s:7:"version";s:3:"4.7";s:11:"count_valid";i:1;s:13:"count_invalid";i:0;}}', 'yes'),
(1270, 'flamingo_inbound_channel_children', 'a:1:{i:28;a:1:{i:0;i:29;}}', 'yes'),
(1277, '_transient_is_multi_author', '0', 'yes'),
(1279, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1491485541', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1280, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:"stdClass":100:{s:6:"widget";a:3:{s:4:"name";s:6:"widget";s:4:"slug";s:6:"widget";s:5:"count";i:4324;}s:4:"post";a:3:{s:4:"name";s:4:"post";s:4:"slug";s:4:"post";s:5:"count";i:2464;}s:5:"admin";a:3:{s:4:"name";s:5:"admin";s:4:"slug";s:5:"admin";s:5:"count";i:2351;}s:11:"woocommerce";a:3:{s:4:"name";s:11:"woocommerce";s:4:"slug";s:11:"woocommerce";s:5:"count";i:1971;}s:5:"posts";a:3:{s:4:"name";s:5:"posts";s:4:"slug";s:5:"posts";s:5:"count";i:1814;}s:8:"comments";a:3:{s:4:"name";s:8:"comments";s:4:"slug";s:8:"comments";s:5:"count";i:1568;}s:9:"shortcode";a:3:{s:4:"name";s:9:"shortcode";s:4:"slug";s:9:"shortcode";s:5:"count";i:1532;}s:7:"twitter";a:3:{s:4:"name";s:7:"twitter";s:4:"slug";s:7:"twitter";s:5:"count";i:1420;}s:6:"google";a:3:{s:4:"name";s:6:"google";s:4:"slug";s:6:"google";s:5:"count";i:1322;}s:6:"images";a:3:{s:4:"name";s:6:"images";s:4:"slug";s:6:"images";s:5:"count";i:1314;}s:8:"facebook";a:3:{s:4:"name";s:8:"facebook";s:4:"slug";s:8:"facebook";s:5:"count";i:1299;}s:7:"sidebar";a:3:{s:4:"name";s:7:"sidebar";s:4:"slug";s:7:"sidebar";s:5:"count";i:1267;}s:5:"image";a:3:{s:4:"name";s:5:"image";s:4:"slug";s:5:"image";s:5:"count";i:1249;}s:3:"seo";a:3:{s:4:"name";s:3:"seo";s:4:"slug";s:3:"seo";s:5:"count";i:1077;}s:4:"page";a:3:{s:4:"name";s:4:"page";s:4:"slug";s:4:"page";s:5:"count";i:1036;}s:7:"gallery";a:3:{s:4:"name";s:7:"gallery";s:4:"slug";s:7:"gallery";s:5:"count";i:1019;}s:6:"social";a:3:{s:4:"name";s:6:"social";s:4:"slug";s:6:"social";s:5:"count";i:978;}s:5:"email";a:3:{s:4:"name";s:5:"email";s:4:"slug";s:5:"email";s:5:"count";i:900;}s:5:"links";a:3:{s:4:"name";s:5:"links";s:4:"slug";s:5:"links";s:5:"count";i:809;}s:9:"ecommerce";a:3:{s:4:"name";s:9:"ecommerce";s:4:"slug";s:9:"ecommerce";s:5:"count";i:774;}s:5:"login";a:3:{s:4:"name";s:5:"login";s:4:"slug";s:5:"login";s:5:"count";i:773;}s:7:"widgets";a:3:{s:4:"name";s:7:"widgets";s:4:"slug";s:7:"widgets";s:5:"count";i:757;}s:5:"video";a:3:{s:4:"name";s:5:"video";s:4:"slug";s:5:"video";s:5:"count";i:745;}s:3:"rss";a:3:{s:4:"name";s:3:"rss";s:4:"slug";s:3:"rss";s:5:"count";i:661;}s:10:"buddypress";a:3:{s:4:"name";s:10:"buddypress";s:4:"slug";s:10:"buddypress";s:5:"count";i:650;}s:5:"pages";a:3:{s:4:"name";s:5:"pages";s:4:"slug";s:5:"pages";s:5:"count";i:641;}s:7:"content";a:3:{s:4:"name";s:7:"content";s:4:"slug";s:7:"content";s:5:"count";i:638;}s:4:"spam";a:3:{s:4:"name";s:4:"spam";s:4:"slug";s:4:"spam";s:5:"count";i:632;}s:6:"jquery";a:3:{s:4:"name";s:6:"jquery";s:4:"slug";s:6:"jquery";s:5:"count";i:631;}s:8:"security";a:3:{s:4:"name";s:8:"security";s:4:"slug";s:8:"security";s:5:"count";i:604;}s:4:"ajax";a:3:{s:4:"name";s:4:"ajax";s:4:"slug";s:4:"ajax";s:5:"count";i:590;}s:6:"slider";a:3:{s:4:"name";s:6:"slider";s:4:"slug";s:6:"slider";s:5:"count";i:584;}s:5:"media";a:3:{s:4:"name";s:5:"media";s:4:"slug";s:5:"media";s:5:"count";i:582;}s:8:"category";a:3:{s:4:"name";s:8:"category";s:4:"slug";s:8:"category";s:5:"count";i:565;}s:4:"feed";a:3:{s:4:"name";s:4:"feed";s:4:"slug";s:4:"feed";s:5:"count";i:563;}s:9:"analytics";a:3:{s:4:"name";s:9:"analytics";s:4:"slug";s:9:"analytics";s:5:"count";i:561;}s:6:"search";a:3:{s:4:"name";s:6:"search";s:4:"slug";s:6:"search";s:5:"count";i:557;}s:10:"e-commerce";a:3:{s:4:"name";s:10:"e-commerce";s:4:"slug";s:10:"e-commerce";s:5:"count";i:549;}s:4:"menu";a:3:{s:4:"name";s:4:"menu";s:4:"slug";s:4:"menu";s:5:"count";i:535;}s:5:"embed";a:3:{s:4:"name";s:5:"embed";s:4:"slug";s:5:"embed";s:5:"count";i:525;}s:10:"javascript";a:3:{s:4:"name";s:10:"javascript";s:4:"slug";s:10:"javascript";s:5:"count";i:519;}s:4:"link";a:3:{s:4:"name";s:4:"link";s:4:"slug";s:4:"link";s:5:"count";i:516;}s:4:"form";a:3:{s:4:"name";s:4:"form";s:4:"slug";s:4:"form";s:5:"count";i:497;}s:3:"css";a:3:{s:4:"name";s:3:"css";s:4:"slug";s:3:"css";s:5:"count";i:494;}s:5:"share";a:3:{s:4:"name";s:5:"share";s:4:"slug";s:5:"share";s:5:"count";i:487;}s:7:"comment";a:3:{s:4:"name";s:7:"comment";s:4:"slug";s:7:"comment";s:5:"count";i:486;}s:7:"youtube";a:3:{s:4:"name";s:7:"youtube";s:4:"slug";s:7:"youtube";s:5:"count";i:479;}s:5:"theme";a:3:{s:4:"name";s:5:"theme";s:4:"slug";s:5:"theme";s:5:"count";i:472;}s:6:"custom";a:3:{s:4:"name";s:6:"custom";s:4:"slug";s:6:"custom";s:5:"count";i:460;}s:10:"categories";a:3:{s:4:"name";s:10:"categories";s:4:"slug";s:10:"categories";s:5:"count";i:457;}s:10:"responsive";a:3:{s:4:"name";s:10:"responsive";s:4:"slug";s:10:"responsive";s:5:"count";i:451;}s:9:"dashboard";a:3:{s:4:"name";s:9:"dashboard";s:4:"slug";s:9:"dashboard";s:5:"count";i:451;}s:3:"ads";a:3:{s:4:"name";s:3:"ads";s:4:"slug";s:3:"ads";s:5:"count";i:441;}s:6:"button";a:3:{s:4:"name";s:6:"button";s:4:"slug";s:6:"button";s:5:"count";i:433;}s:4:"tags";a:3:{s:4:"name";s:4:"tags";s:4:"slug";s:4:"tags";s:5:"count";i:428;}s:9:"affiliate";a:3:{s:4:"name";s:9:"affiliate";s:4:"slug";s:9:"affiliate";s:5:"count";i:427;}s:6:"editor";a:3:{s:4:"name";s:6:"editor";s:4:"slug";s:6:"editor";s:5:"count";i:418;}s:5:"photo";a:3:{s:4:"name";s:5:"photo";s:4:"slug";s:5:"photo";s:5:"count";i:415;}s:4:"user";a:3:{s:4:"name";s:4:"user";s:4:"slug";s:4:"user";s:5:"count";i:404;}s:9:"slideshow";a:3:{s:4:"name";s:9:"slideshow";s:4:"slug";s:9:"slideshow";s:5:"count";i:399;}s:5:"stats";a:3:{s:4:"name";s:5:"stats";s:4:"slug";s:5:"stats";s:5:"count";i:397;}s:6:"mobile";a:3:{s:4:"name";s:6:"mobile";s:4:"slug";s:6:"mobile";s:5:"count";i:395;}s:12:"contact-form";a:3:{s:4:"name";s:12:"contact form";s:4:"slug";s:12:"contact-form";s:5:"count";i:395;}s:7:"contact";a:3:{s:4:"name";s:7:"contact";s:4:"slug";s:7:"contact";s:5:"count";i:389;}s:5:"users";a:3:{s:4:"name";s:5:"users";s:4:"slug";s:5:"users";s:5:"count";i:389;}s:6:"photos";a:3:{s:4:"name";s:6:"photos";s:4:"slug";s:6:"photos";s:5:"count";i:388;}s:10:"statistics";a:3:{s:4:"name";s:10:"statistics";s:4:"slug";s:10:"statistics";s:5:"count";i:372;}s:3:"api";a:3:{s:4:"name";s:3:"api";s:4:"slug";s:3:"api";s:5:"count";i:366;}s:10:"navigation";a:3:{s:4:"name";s:10:"navigation";s:4:"slug";s:10:"navigation";s:5:"count";i:358;}s:6:"events";a:3:{s:4:"name";s:6:"events";s:4:"slug";s:6:"events";s:5:"count";i:354;}s:4:"news";a:3:{s:4:"name";s:4:"news";s:4:"slug";s:4:"news";s:5:"count";i:343;}s:8:"calendar";a:3:{s:4:"name";s:8:"calendar";s:4:"slug";s:8:"calendar";s:5:"count";i:329;}s:12:"social-media";a:3:{s:4:"name";s:12:"social media";s:4:"slug";s:12:"social-media";s:5:"count";i:328;}s:7:"plugins";a:3:{s:4:"name";s:7:"plugins";s:4:"slug";s:7:"plugins";s:5:"count";i:327;}s:9:"multisite";a:3:{s:4:"name";s:9:"multisite";s:4:"slug";s:9:"multisite";s:5:"count";i:324;}s:10:"shortcodes";a:3:{s:4:"name";s:10:"shortcodes";s:4:"slug";s:10:"shortcodes";s:5:"count";i:319;}s:4:"meta";a:3:{s:4:"name";s:4:"meta";s:4:"slug";s:4:"meta";s:5:"count";i:318;}s:4:"code";a:3:{s:4:"name";s:4:"code";s:4:"slug";s:4:"code";s:5:"count";i:317;}s:4:"list";a:3:{s:4:"name";s:4:"list";s:4:"slug";s:4:"list";s:5:"count";i:316;}s:3:"url";a:3:{s:4:"name";s:3:"url";s:4:"slug";s:3:"url";s:5:"count";i:313;}s:10:"newsletter";a:3:{s:4:"name";s:10:"newsletter";s:4:"slug";s:10:"newsletter";s:5:"count";i:304;}s:7:"payment";a:3:{s:4:"name";s:7:"payment";s:4:"slug";s:7:"payment";s:5:"count";i:303;}s:3:"tag";a:3:{s:4:"name";s:3:"tag";s:4:"slug";s:3:"tag";s:5:"count";i:289;}s:6:"simple";a:3:{s:4:"name";s:6:"simple";s:4:"slug";s:6:"simple";s:5:"count";i:289;}s:16:"custom-post-type";a:3:{s:4:"name";s:16:"custom post type";s:4:"slug";s:16:"custom-post-type";s:5:"count";i:282;}s:5:"popup";a:3:{s:4:"name";s:5:"popup";s:4:"slug";s:5:"popup";s:5:"count";i:281;}s:11:"advertising";a:3:{s:4:"name";s:11:"advertising";s:4:"slug";s:11:"advertising";s:5:"count";i:280;}s:9:"marketing";a:3:{s:4:"name";s:9:"marketing";s:4:"slug";s:9:"marketing";s:5:"count";i:280;}s:8:"redirect";a:3:{s:4:"name";s:8:"redirect";s:4:"slug";s:8:"redirect";s:5:"count";i:278;}s:4:"chat";a:3:{s:4:"name";s:4:"chat";s:4:"slug";s:4:"chat";s:5:"count";i:277;}s:6:"author";a:3:{s:4:"name";s:6:"author";s:4:"slug";s:6:"author";s:5:"count";i:275;}s:7:"adsense";a:3:{s:4:"name";s:7:"adsense";s:4:"slug";s:7:"adsense";s:5:"count";i:269;}s:4:"html";a:3:{s:4:"name";s:4:"html";s:4:"slug";s:4:"html";s:5:"count";i:267;}s:14:"administration";a:3:{s:4:"name";s:14:"administration";s:4:"slug";s:14:"administration";s:5:"count";i:264;}s:8:"lightbox";a:3:{s:4:"name";s:8:"lightbox";s:4:"slug";s:8:"lightbox";s:5:"count";i:263;}s:5:"forms";a:3:{s:4:"name";s:5:"forms";s:4:"slug";s:5:"forms";s:5:"count";i:261;}s:7:"captcha";a:3:{s:4:"name";s:7:"captcha";s:4:"slug";s:7:"captcha";s:5:"count";i:261;}s:12:"notification";a:3:{s:4:"name";s:12:"notification";s:4:"slug";s:12:"notification";s:5:"count";i:260;}s:7:"tinymce";a:3:{s:4:"name";s:7:"tinyMCE";s:4:"slug";s:7:"tinymce";s:5:"count";i:260;}s:5:"flash";a:3:{s:4:"name";s:5:"flash";s:4:"slug";s:5:"flash";s:5:"count";i:260;}}', 'no'),
(1287, 'ReduxFrameworkPlugin', 'a:1:{s:4:"demo";b:0;}', 'yes'),
(1288, 'r_notice_data', '{"type":"redux-message","title":"<strong>The Ultimate Redux Extensions Bundle!<\\/strong><br\\/>\\r\\n\\r\\n","message":"Attention Redux Developers!  Due to popular demand, we are extending the availability of the Ultimate Redux Extension Bundle, which includes <strong>all<\\/strong> of our extensions at over 40% off!  Grab you\'re copy today at <a href=\\"https:\\/\\/reduxframework.com\\/extension\\/ultimate-bundle\\">https:\\/\\/reduxframework.com\\/extension\\/ultimate-bundle<\\/a>","color":"rgba(0,162,227,1)"}', 'yes'),
(1289, 'redux_blast', '1491474849', 'yes'),
(1290, 'redux_version_upgraded_from', '3.6.4', 'yes'),
(1291, '_transient_timeout__redux_activation_redirect', '1491527099', 'no'),
(1292, '_transient__redux_activation_redirect', '1', 'no'),
(1293, 'redux_demo', 'a:104:{s:8:"last_tab";s:1:"9";s:12:"opt-checkbox";s:1:"1";s:15:"opt-multi-check";a:3:{i:1;s:1:"1";i:2;s:0:"";i:3;s:0:"";}s:17:"opt-checkbox-data";a:1:{i:18;s:0:"";}s:20:"opt-checkbox-sidebar";a:7:{s:9:"sidebar-1";s:0:"";s:8:"header-1";s:0:"";s:8:"footer-1";s:0:"";s:8:"footer-2";s:0:"";s:8:"footer-3";s:0:"";s:8:"footer-4";s:0:"";s:12:"footer-bar-1";s:0:"";}s:9:"opt-radio";s:1:"2";s:12:"opt-sortable";a:3:{s:8:"Text One";s:6:"Item 1";s:8:"Text Two";s:6:"Item 2";s:10:"Text Three";s:6:"Item 3";}s:18:"opt-check-sortable";a:3:{s:3:"cb1";s:0:"";s:3:"cb2";s:1:"1";s:3:"cb3";s:0:"";}s:12:"text-example";s:12:"Default Text";s:17:"text-example-hint";s:12:"Default Text";s:16:"text-placeholder";s:0:"";s:13:"opt-multitext";a:1:{i:0;s:0:"";}s:8:"password";a:2:{s:8:"username";s:11:"thientanngo";s:8:"password";s:20:"Thientan171131052103";}s:12:"opt-textarea";s:12:"Default Text";s:10:"opt-editor";s:27:"Powered by Redux Framework.";s:15:"opt-editor-tiny";s:34:"<p>Powered by Redux Framework.</p>";s:15:"opt-editor-full";s:0:"";s:18:"opt-ace-editor-css";s:31:"#header{\r\n   margin: 0 auto;\r\n}";s:17:"opt-ace-editor-js";s:41:"jQuery(document).ready(function(){\r\n\r\n});";s:18:"opt-ace-editor-php";s:29:"<?php\r\n    echo "PHP String";";s:15:"opt-color-title";s:7:"#000000";s:16:"opt-color-footer";s:7:"#dd9933";s:16:"opt-color-header";a:2:{s:4:"from";s:7:"#1e73be";s:2:"to";s:7:"#00897e";}s:14:"opt-color-rgba";a:3:{s:5:"color";s:7:"#7e33dd";s:5:"alpha";s:2:".8";s:4:"rgba";s:20:"rgba(126,51,221,0.8)";}s:14:"opt-link-color";a:3:{s:7:"regular";s:4:"#aaa";s:5:"hover";s:4:"#bbb";s:6:"active";s:4:"#ccc";}s:17:"opt-palette-color";s:3:"red";s:14:"opt-background";a:7:{s:16:"background-color";s:0:"";s:17:"background-repeat";s:0:"";s:15:"background-size";s:0:"";s:21:"background-attachment";s:0:"";s:19:"background-position";s:0:"";s:16:"background-image";s:0:"";s:5:"media";a:4:{s:2:"id";s:0:"";s:6:"height";s:0:"";s:5:"width";s:0:"";s:9:"thumbnail";s:0:"";}}s:17:"opt-header-border";a:6:{s:10:"border-top";s:3:"3px";s:12:"border-right";s:3:"3px";s:13:"border-bottom";s:3:"3px";s:11:"border-left";s:3:"3px";s:12:"border-style";s:5:"solid";s:12:"border-color";s:7:"#1e73be";}s:26:"opt-header-border-expanded";a:6:{s:10:"border-top";s:3:"3px";s:12:"border-right";s:3:"3px";s:13:"border-bottom";s:3:"3px";s:11:"border-left";s:3:"3px";s:12:"border-style";s:5:"solid";s:12:"border-color";s:7:"#1e73be";}s:14:"opt-dimensions";a:3:{s:5:"width";s:5:"200px";s:6:"height";s:5:"100px";s:5:"units";s:2:"px";}s:20:"opt-dimensions-width";a:2:{s:5:"width";s:5:"200px";s:5:"units";s:2:"px";}s:11:"opt-spacing";a:4:{s:10:"margin-top";s:1:"1";s:12:"margin-right";s:1:"2";s:13:"margin-bottom";s:1:"3";s:11:"margin-left";s:1:"4";}s:20:"opt-spacing-expanded";a:5:{s:10:"margin-top";s:3:"1px";s:12:"margin-right";s:3:"2px";s:13:"margin-bottom";s:3:"3px";s:11:"margin-left";s:3:"4px";s:5:"units";s:2:"px";}s:11:"opt-gallery";s:0:"";s:9:"opt-media";a:5:{s:3:"url";s:52:"http://s.wordpress.org/style/images/codeispoetry.png";s:2:"id";s:0:"";s:6:"height";s:0:"";s:5:"width";s:0:"";s:9:"thumbnail";s:0:"";}s:12:"media-no-url";a:5:{s:3:"url";s:0:"";s:2:"id";s:0:"";s:6:"height";s:0:"";s:5:"width";s:0:"";s:9:"thumbnail";s:0:"";}s:16:"media-no-preview";a:5:{s:3:"url";s:0:"";s:2:"id";s:0:"";s:6:"height";s:0:"";s:5:"width";s:0:"";s:9:"thumbnail";s:0:"";}s:17:"opt-random-upload";a:5:{s:3:"url";s:0:"";s:2:"id";s:0:"";s:6:"height";s:0:"";s:5:"width";s:0:"";s:9:"thumbnail";s:0:"";}s:10:"opt-slides";a:1:{i:0;a:9:{s:5:"title";s:0:"";s:11:"description";s:0:"";s:3:"url";s:0:"";s:4:"sort";s:1:"0";s:13:"attachment_id";s:0:"";s:5:"thumb";s:0:"";s:5:"image";s:0:"";s:6:"height";s:0:"";s:5:"width";s:0:"";}}s:12:"section-test";s:0:"";s:18:"section-test-media";a:5:{s:3:"url";s:0:"";s:2:"id";s:0:"";s:6:"height";s:0:"";s:5:"width";s:0:"";s:9:"thumbnail";s:0:"";}s:14:"opt-button-set";s:1:"2";s:20:"opt-button-set-multi";a:2:{i:0;s:1:"2";i:1;s:1:"3";}s:9:"switch-on";s:1:"1";s:10:"switch-off";s:0:"";s:13:"switch-parent";s:1:"0";s:13:"switch-child1";s:0:"";s:13:"switch-child2";s:0:"";s:10:"opt-select";s:1:"2";s:21:"opt-select-stylesheet";s:11:"default.css";s:19:"opt-select-optgroup";s:1:"2";s:16:"opt-multi-select";a:2:{i:0;s:1:"2";i:1;s:1:"3";}s:16:"opt-select-image";s:0:"";s:21:"opt-select-categories";s:0:"";s:16:"opt-select-pages";s:0:"";s:16:"opt-select-menus";s:0:"";s:20:"opt-select-post-type";s:0:"";s:16:"opt-select-posts";s:0:"";s:16:"opt-select-roles";s:0:"";s:18:"opt-select-elusive";s:0:"";s:16:"opt-select-users";s:0:"";s:23:"opt-image-select-layout";s:1:"2";s:16:"opt-image-select";s:1:"2";s:16:"opt-select_image";s:0:"";s:16:"opt-slider-label";s:3:"250";s:15:"opt-slider-text";s:2:"75";s:16:"opt-slider-float";s:3:"0.5";s:11:"opt-spinner";s:2:"40";s:19:"opt-typography-body";a:10:{s:11:"font-family";s:26:"Arial,Helvetica,sans-serif";s:12:"font-options";s:0:"";s:6:"google";s:1:"1";s:11:"font-weight";s:6:"Normal";s:10:"font-style";s:0:"";s:7:"subsets";s:0:"";s:10:"text-align";s:0:"";s:9:"font-size";s:4:"30px";s:11:"line-height";s:0:"";s:5:"color";s:7:"#dd9933";}s:14:"opt-typography";a:11:{s:11:"font-family";s:4:"Abel";s:12:"font-options";s:0:"";s:6:"google";s:1:"1";s:11:"font-backup";s:0:"";s:11:"font-weight";s:0:"";s:10:"font-style";s:3:"700";s:7:"subsets";s:0:"";s:10:"text-align";s:0:"";s:9:"font-size";s:4:"33px";s:11:"line-height";s:4:"40px";s:5:"color";s:4:"#333";}s:14:"opt-datepicker";s:0:"";s:19:"opt-homepage-layout";a:3:{s:7:"enabled";a:5:{s:7:"placebo";s:7:"placebo";s:10:"highlights";s:10:"Highlights";s:6:"slider";s:6:"Slider";s:10:"staticpage";s:11:"Static Page";s:8:"services";s:8:"Services";}s:8:"disabled";a:1:{s:7:"placebo";s:7:"placebo";}s:6:"backup";a:1:{s:7:"placebo";s:7:"placebo";}}s:21:"opt-homepage-layout-2";a:2:{s:8:"disabled";a:3:{s:7:"placebo";s:7:"placebo";s:10:"highlights";s:10:"Highlights";s:6:"slider";s:6:"Slider";}s:7:"enabled";a:3:{s:7:"placebo";s:7:"placebo";s:10:"staticpage";s:11:"Static Page";s:8:"services";s:8:"Services";}}s:14:"opt-text-email";s:13:"test@test.com";s:18:"opt-text-post-type";a:4:{s:10:"attachment";s:19:"Đa phương tiện";s:4:"page";s:5:"Trang";s:4:"post";s:11:"Bài viết";s:7:"product";s:12:"Sản phẩm";}s:14:"opt-multi-text";a:1:{i:0;b:0;}s:12:"opt-text-url";s:25:"http://reduxframework.com";s:16:"opt-text-numeric";s:1:"0";s:22:"opt-text-comma-numeric";s:1:"0";s:25:"opt-text-no-special-chars";s:1:"0";s:20:"opt-text-str_replace";s:53:"Thisthisisaspaceisthisisaspacethethisisaspacedefault.";s:21:"opt-text-preg_replace";s:10:"no numbers";s:24:"opt-text-custom_validate";s:1:"0";s:20:"opt-textarea-no-html";s:27:"No HTML is allowed in here.";s:17:"opt-textarea-html";s:24:"HTML is allowed in here.";s:22:"opt-textarea-some-html";s:29:"Some HTML is allowed in here.";s:15:"opt-textarea-js";s:0:"";s:18:"opt-required-basic";s:0:"";s:23:"opt-required-basic-text";s:0:"";s:19:"opt-required-nested";s:0:"";s:29:"opt-required-nested-buttonset";s:11:"button-text";s:24:"opt-required-nested-text";s:0:"";s:28:"opt-required-nested-textarea";s:0:"";s:26:"opt-required-nested-editor";s:0:"";s:23:"opt-required-nested-ace";s:0:"";s:19:"opt-required-select";s:10:"no-sidebar";s:32:"opt-required-select-left-sidebar";s:0:"";s:33:"opt-required-select-right-sidebar";s:0:"";s:9:"wpml-text";s:0:"";s:15:"wpml-multicheck";a:3:{i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";}s:19:"opt-customizer-only";s:1:"2";s:12:"opt-patterns";i:0;s:11:"opt-presets";i:0;s:17:"opt-slider-select";a:2:{i:1;i:100;i:2;i:300;}}', 'yes'),
(1294, 'redux_demo-transients', 'a:2:{s:14:"changed_values";a:1:{s:8:"last_tab";s:1:"1";}s:9:"last_save";i:1491475220;}', 'yes'),
(1295, '_transient_timeout_redux-extensions-fetch', '1491561312', 'no'),
(1296, '_transient_redux-extensions-fetch', 'a:15:{s:11:"google-maps";a:7:{s:2:"id";i:4714;s:5:"title";s:11:"Google Maps";s:7:"created";s:19:"2016-02-10 03:02:51";s:7:"updated";s:19:"2016-02-11 06:45:34";s:7:"excerpt";s:0:"";s:3:"url";s:49:"https://reduxframework.com/extension/google-maps/";s:5:"thumb";s:75:"http://reduxframework.com/wp-content/uploads/edd/2016/02/google_map_ext.png";}s:10:"ad-remover";a:7:{s:2:"id";i:4526;s:5:"title";s:10:"Ad Remover";s:7:"created";s:19:"2016-01-12 04:02:58";s:7:"updated";s:19:"2016-01-12 04:04:22";s:7:"excerpt";s:0:"";s:3:"url";s:48:"https://reduxframework.com/extension/ad-remover/";s:5:"thumb";s:74:"http://reduxframework.com/wp-content/uploads/edd/2016/01/ext_ad_remove.png";}s:9:"accordion";a:7:{s:2:"id";i:2598;s:5:"title";s:9:"Accordion";s:7:"created";s:19:"2015-03-26 21:43:11";s:7:"updated";s:19:"2015-05-12 20:13:43";s:7:"excerpt";s:129:"The Redux Accordion extension is an improved way to group like items in your options panel, inside a collapsible accordion field.";s:3:"url";s:47:"https://reduxframework.com/extension/accordion/";s:5:"thumb";s:75:"http://reduxframework.com/wp-content/uploads/edd/2015/03/accordion_logo.png";}s:8:"repeater";a:7:{s:2:"id";i:2147;s:5:"title";s:8:"Repeater";s:7:"created";s:19:"2015-01-14 00:29:40";s:7:"updated";s:19:"2015-06-13 03:54:41";s:7:"excerpt";s:140:"Give your users the power of infinite flexibility with dynamic, repeatable fields. Use any Redux field and customize your user\'s experience.";s:3:"url";s:46:"https://reduxframework.com/extension/repeater/";s:5:"thumb";s:69:"http://reduxframework.com/wp-content/uploads/edd/2015/01/repeater.png";}s:15:"social-profiles";a:7:{s:2:"id";i:2049;s:5:"title";s:15:"Social Profiles";s:7:"created";s:19:"2014-12-16 05:40:43";s:7:"updated";s:19:"2016-12-15 03:58:51";s:7:"excerpt";s:153:"Give your users the ability to add social profiles (icons with links to their social pages) from one central field in your Redux Framework options panel.";s:3:"url";s:53:"https://reduxframework.com/extension/social-profiles/";s:5:"thumb";s:71:"http://reduxframework.com/wp-content/uploads/edd/2014/12/social_ext.png";}s:9:"js-button";a:7:{s:2:"id";i:1965;s:5:"title";s:9:"JS Button";s:7:"created";s:19:"2014-11-27 03:57:50";s:7:"updated";s:19:"2015-03-26 02:45:38";s:7:"excerpt";s:131:"Give your users the ability to run JavaScript functions manually from within the Redux Framework options panel via button clicks.  ";s:3:"url";s:47:"https://reduxframework.com/extension/js-button/";s:5:"thumb";s:70:"http://reduxframework.com/wp-content/uploads/edd/2014/11/js_button.png";}s:11:"multi-media";a:7:{s:2:"id";i:1963;s:5:"title";s:11:"Multi Media";s:7:"created";s:19:"2014-11-27 03:58:57";s:7:"updated";s:19:"2015-06-13 04:02:12";s:7:"excerpt";s:100:"Give your users the ability to upload and/or select multiple files from the Wordpress Media library.";s:3:"url";s:49:"https://reduxframework.com/extension/multi-media/";s:5:"thumb";s:72:"http://reduxframework.com/wp-content/uploads/edd/2014/11/multi-media.png";}s:10:"css-layout";a:7:{s:2:"id";i:1684;s:5:"title";s:10:"CSS Layout";s:7:"created";s:19:"2014-08-17 02:52:29";s:7:"updated";s:19:"2017-01-11 01:15:05";s:7:"excerpt";s:183:"The Redux CSS Layout extension is great and innovative new way to set the margin, border, border radius, and padding elements for any CSS selector, all from one easy to use interface.";s:3:"url";s:48:"https://reduxframework.com/extension/css-layout/";s:5:"thumb";s:71:"http://reduxframework.com/wp-content/uploads/edd/2014/08/css-layout.png";}s:13:"color-schemes";a:7:{s:2:"id";i:988;s:5:"title";s:13:"Color Schemes";s:7:"created";s:19:"2014-05-05 20:01:51";s:7:"updated";s:19:"2015-06-13 04:03:02";s:7:"excerpt";s:156:"Give your users the ability to change the color layout of their themes with virtually no effort. Allow your users to save, and re-use any number of presets.";s:3:"url";s:51:"https://reduxframework.com/extension/color-schemes/";s:5:"thumb";s:74:"http://reduxframework.com/wp-content/uploads/edd/2014/07/color_schemes.png";}s:11:"live-search";a:7:{s:2:"id";i:526;s:5:"title";s:11:"Live Search";s:7:"created";s:19:"2014-02-21 05:13:10";s:7:"updated";s:19:"2015-03-26 02:46:04";s:7:"excerpt";s:203:"Does your panel have so many options it\'s difficult for your users to locate things? Live Search will create a search box that will dynamically filter all your available options. No configuration needed!";s:3:"url";s:49:"https://reduxframework.com/extension/live-search/";s:5:"thumb";s:72:"http://reduxframework.com/wp-content/uploads/edd/2014/07/live-search.png";}s:9:"date-time";a:7:{s:2:"id";i:458;s:5:"title";s:18:"Date / Time Picker";s:7:"created";s:19:"2014-02-14 02:08:36";s:7:"updated";s:19:"2016-09-20 01:12:26";s:7:"excerpt";s:192:"The Redux Date-Time Picker (DTP) extension is a new way to select dates and times for use in text fields.  The DTP contains several modes based on how you\'d like to display time and date data.";s:3:"url";s:47:"https://reduxframework.com/extension/date-time/";s:5:"thumb";s:69:"http://reduxframework.com/wp-content/uploads/edd/2014/05/datetime.png";}s:9:"metaboxes";a:7:{s:2:"id";i:191;s:5:"title";s:9:"Metaboxes";s:7:"created";s:19:"2014-01-28 00:49:31";s:7:"updated";s:19:"2016-12-15 03:52:20";s:7:"excerpt";s:124:"Integrate your options panel and meta items into one. It even overrides your global variable, so you only have to code once.";s:3:"url";s:47:"https://reduxframework.com/extension/metaboxes/";s:5:"thumb";s:74:"http://reduxframework.com/wp-content/uploads/edd/2014/07/ext-metaboxes.png";}s:12:"widget-areas";a:7:{s:2:"id";i:208;s:5:"title";s:12:"Widget Areas";s:7:"created";s:19:"2014-01-28 05:49:30";s:7:"updated";s:19:"2016-04-25 13:24:37";s:7:"excerpt";s:253:"Why limit your users to a limited number of widget areas? With this extension your users will be able to create and delete as many widget areas as they desire. You need only allow supply them with a way to switch the widget area for any given page/post.";s:3:"url";s:50:"https://reduxframework.com/extension/widget-areas/";s:5:"thumb";s:77:"http://reduxframework.com/wp-content/uploads/edd/2014/07/ext-widget-areas.png";}s:10:"shortcodes";a:7:{s:2:"id";i:209;s:5:"title";s:10:"Shortcodes";s:7:"created";s:19:"2014-01-28 05:50:24";s:7:"updated";s:19:"2015-03-26 02:52:01";s:7:"excerpt";s:166:"Give your users some very simple, but powerful shortcodes. This extension makes a number of the WordPress core and functions and blog settings accessible to the user.";s:3:"url";s:48:"https://reduxframework.com/extension/shortcodes/";s:5:"thumb";s:75:"http://reduxframework.com/wp-content/uploads/edd/2014/07/ext-shortcodes.png";}s:11:"icon-select";a:7:{s:2:"id";i:328;s:5:"title";s:11:"Icon Select";s:7:"created";s:19:"2014-02-01 16:39:13";s:7:"updated";s:19:"2016-09-17 20:06:27";s:7:"excerpt";s:222:"Let your users select an icon of their choice from your choosing. You can incorporate any font family. You need only denote the prefix and supply the URL or path to the font CSS file. It will even enqueue the font for you.";s:3:"url";s:49:"https://reduxframework.com/extension/icon-select/";s:5:"thumb";s:72:"http://reduxframework.com/wp-content/uploads/edd/2014/07/icon_select.png";}}', 'no'),
(1297, '_transient_timeout_select2-css_style_cdn_is_up', '1491561320', 'no'),
(1298, '_transient_select2-css_style_cdn_is_up', '1', 'no'),
(1299, '_transient_timeout_select2-js_script_cdn_is_up', '1491561320', 'no'),
(1300, '_transient_select2-js_script_cdn_is_up', '1', 'no'),
(1301, '_transient_timeout_ace-editor-js_script_cdn_is_up', '1491561321', 'no'),
(1302, '_transient_ace-editor-js_script_cdn_is_up', '1', 'no'),
(1304, 'redux_company_info-transients', 'a:2:{s:14:"changed_values";a:1:{s:18:"facebook_page_name";s:0:"";}s:9:"last_save";i:1491497819;}', 'yes'),
(1309, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:62:"https://downloads.wordpress.org/release/vi/wordpress-4.7.3.zip";s:6:"locale";s:2:"vi";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:62:"https://downloads.wordpress.org/release/vi/wordpress-4.7.3.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.7.3";s:7:"version";s:5:"4.7.3";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1491524592;s:15:"version_checked";s:5:"4.7.3";s:12:"translations";a:1:{i:0;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:2:"vi";s:7:"version";s:5:"4.7.3";s:7:"updated";s:19:"2017-04-05 09:36:00";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.3/vi.zip";s:10:"autoupdate";b:1;}}}', 'no'),
(1310, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1491524594;s:7:"checked";a:2:{s:16:"storefront-child";s:5:"2.1.8";s:10:"storefront";s:5:"2.1.8";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(1313, 'redux_company_info', 'a:13:{s:8:"last_tab";s:1:"0";s:7:"hotline";s:13:"190123,123123";s:11:"company_mst";s:10:"0309537103";s:23:"company_dang_ky_lan_dau";s:27:"Ngày 05 tháng 1 năm 2010";s:29:"company_dang_ky_lan_dau_lan_1";s:120:"Ngày 24 tháng 11 năm 2014. Do sở kế hoạch và đầu tư TP Hồ Chí Minh, phòng Đăng Ký Kinh Doanh cấp.";s:16:"company_dai_dien";s:31:"Nguyễn Thị Ngọc Phượng";s:15:"company_dia_chi";s:57:"245 Nhật Tảo – Phường 8 – Quận 10 – Tp HCM";s:18:"company_dien_thoai";s:31:"(08) 6271 3730 – 0902 333 345";s:11:"company_fax";s:14:"(08) 6290 9152";s:13:"company_email";s:18:"hoada175@gmail.com";s:15:"company_website";s:16:"hoadamedical.com";s:17:"facebook_page_url";s:41:"https://www.facebook.com/dungcuytethammy/";s:18:"facebook_page_name";s:44:"Dụng Cụ Y Tế & Thẩm Mỹ Tâm Đức";}', 'yes'),
(1333, 'jetpack_sync_settings_disable', '0', 'yes'),
(1336, 'jpsq_sync-1491497992.140488-445898-1', 'a:6:{i:0;s:14:"deleted_plugin";i:1;a:2:{i:0;s:19:"jetpack/jetpack.php";i:1;b:1;}i:2;i:1;i:3;d:1491497992.14045810699462890625;i:4;b:0;i:5;a:9:{s:12:"display_name";s:10:"yte@tamduc";s:10:"user_email";s:21:"kqthang1505@gmail.com";s:2:"ip";s:9:"127.0.0.1";s:7:"is_cron";b:0;s:11:"is_wp_admin";b:1;s:7:"is_rest";b:0;s:9:"is_xmlrpc";b:0;s:10:"is_wp_rest";b:0;s:7:"is_ajax";b:1;}}', 'no'),
(1339, '_transient_timeout_wc_upgrade_notice_3.0.0', '1491610360', 'no'),
(1340, '_transient_wc_upgrade_notice_3.0.0', '<div class="wc_plugin_upgrade_notice">3.0 is a major update. It is important that you make backups and ensure themes and extensions are 3.0 compatible before upgrading.</div> ', 'no'),
(1341, '_site_transient_timeout_theme_roots', '1491526336', 'no'),
(1342, '_site_transient_theme_roots', 'a:2:{s:16:"storefront-child";s:7:"/themes";s:10:"storefront";s:7:"/themes";}', 'no'),
(1344, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1491524596;s:7:"checked";a:6:{s:19:"akismet/akismet.php";s:3:"3.3";s:36:"contact-form-7/wp-contact-form-7.php";s:3:"4.7";s:33:"custom-widgets/custom-widgets.php";s:0:"";s:21:"flamingo/flamingo.php";s:3:"1.5";s:35:"redux-framework/redux-framework.php";s:5:"3.6.4";s:27:"woocommerce/woocommerce.php";s:5:"3.0.1";}s:8:"response";a:0:{}s:12:"translations";a:0:{}s:9:"no_update";a:5:{s:19:"akismet/akismet.php";O:8:"stdClass":6:{s:2:"id";s:2:"15";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:3:"3.3";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:54:"https://downloads.wordpress.org/plugin/akismet.3.3.zip";}s:36:"contact-form-7/wp-contact-form-7.php";O:8:"stdClass":6:{s:2:"id";s:3:"790";s:4:"slug";s:14:"contact-form-7";s:6:"plugin";s:36:"contact-form-7/wp-contact-form-7.php";s:11:"new_version";s:3:"4.7";s:3:"url";s:45:"https://wordpress.org/plugins/contact-form-7/";s:7:"package";s:61:"https://downloads.wordpress.org/plugin/contact-form-7.4.7.zip";}s:21:"flamingo/flamingo.php";O:8:"stdClass":6:{s:2:"id";s:5:"30380";s:4:"slug";s:8:"flamingo";s:6:"plugin";s:21:"flamingo/flamingo.php";s:11:"new_version";s:3:"1.5";s:3:"url";s:39:"https://wordpress.org/plugins/flamingo/";s:7:"package";s:55:"https://downloads.wordpress.org/plugin/flamingo.1.5.zip";}s:35:"redux-framework/redux-framework.php";O:8:"stdClass":6:{s:2:"id";s:5:"45018";s:4:"slug";s:15:"redux-framework";s:6:"plugin";s:35:"redux-framework/redux-framework.php";s:11:"new_version";s:5:"3.6.4";s:3:"url";s:46:"https://wordpress.org/plugins/redux-framework/";s:7:"package";s:64:"https://downloads.wordpress.org/plugin/redux-framework.3.6.4.zip";}s:27:"woocommerce/woocommerce.php";O:8:"stdClass":7:{s:2:"id";s:5:"25331";s:4:"slug";s:11:"woocommerce";s:6:"plugin";s:27:"woocommerce/woocommerce.php";s:11:"new_version";s:5:"3.0.1";s:3:"url";s:42:"https://wordpress.org/plugins/woocommerce/";s:7:"package";s:60:"https://downloads.wordpress.org/plugin/woocommerce.3.0.1.zip";s:14:"upgrade_notice";s:130:"3.0 is a major update. It is important that you make backups and ensure themes and extensions are 3.0 compatible before upgrading.";}}}', 'no'),
(1345, 'woocommerce_db_version', '3.0.1', 'yes'),
(1346, 'woocommerce_version', '3.0.1', 'yes'),
(1348, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'template-homepage-2.php'),
(2, 8, '_edit_last', '1'),
(3, 8, '_edit_lock', '1489820849:1'),
(4, 8, '_product_attributes', 'a:2:{s:10:"kich-thuoc";a:6:{s:4:"name";s:14:"Kích thước";s:5:"value";s:14:"7 | 8 | 9 | 10";s:8:"position";s:1:"0";s:10:"is_visible";i:1;s:12:"is_variation";i:1;s:11:"is_taxonomy";i:0;}s:9:"chat-lieu";a:6:{s:4:"name";s:13:"Chất liệu";s:5:"value";s:13:"Nhôm | Sắt";s:8:"position";s:1:"1";s:10:"is_visible";i:1;s:12:"is_variation";i:1;s:11:"is_taxonomy";i:0;}}'),
(5, 8, '_visibility', 'visible'),
(6, 8, '_stock_status', 'instock'),
(7, 8, 'total_sales', '2'),
(8, 8, '_downloadable', 'no'),
(9, 8, '_virtual', 'no'),
(10, 8, '_purchase_note', ''),
(11, 8, '_featured', 'no'),
(12, 8, '_weight', ''),
(13, 8, '_length', ''),
(14, 8, '_width', ''),
(15, 8, '_height', ''),
(16, 8, '_sku', 'KJHAKJSHDIUBK'),
(17, 8, '_regular_price', ''),
(18, 8, '_sale_price', ''),
(19, 8, '_sale_price_dates_from', ''),
(20, 8, '_sale_price_dates_to', ''),
(22, 8, '_sold_individually', ''),
(23, 8, '_manage_stock', 'no'),
(24, 8, '_backorders', 'no'),
(25, 8, '_stock', ''),
(26, 8, '_upsell_ids', 'a:0:{}'),
(27, 8, '_crosssell_ids', 'a:0:{}'),
(28, 8, '_product_version', '2.6.14'),
(29, 8, '_product_image_gallery', ''),
(32, 8, '_wc_rating_count', 'a:0:{}'),
(33, 8, '_wc_average_rating', '0'),
(34, 8, '_wc_review_count', '0'),
(35, 10, 'attribute_chat-lieu', 'Sắt'),
(36, 10, 'attribute_kich-thuoc', '10'),
(37, 10, '_stock_status', 'instock'),
(38, 11, 'attribute_chat-lieu', 'Nhôm'),
(39, 11, 'attribute_kich-thuoc', '10'),
(40, 11, '_stock_status', 'instock'),
(41, 12, 'attribute_chat-lieu', 'Sắt'),
(42, 12, 'attribute_kich-thuoc', '9'),
(43, 12, '_stock_status', 'instock'),
(44, 13, 'attribute_chat-lieu', 'Nhôm'),
(45, 13, 'attribute_kich-thuoc', '9'),
(46, 13, '_stock_status', 'instock'),
(47, 14, 'attribute_chat-lieu', 'Sắt'),
(48, 14, 'attribute_kich-thuoc', '8'),
(49, 14, '_stock_status', 'instock'),
(50, 15, 'attribute_chat-lieu', 'Nhôm'),
(51, 15, 'attribute_kich-thuoc', '8'),
(52, 15, '_stock_status', 'instock'),
(53, 16, 'attribute_chat-lieu', 'Sắt'),
(54, 16, 'attribute_kich-thuoc', '7'),
(55, 16, '_stock_status', 'instock'),
(56, 17, 'attribute_chat-lieu', 'Nhôm'),
(57, 17, 'attribute_kich-thuoc', '7'),
(58, 17, '_stock_status', 'instock'),
(59, 8, '_min_variation_price', '6000'),
(60, 8, '_max_variation_price', '9000'),
(61, 8, '_min_price_variation_id', '17'),
(62, 8, '_max_price_variation_id', '16'),
(63, 8, '_min_variation_regular_price', '6000'),
(64, 8, '_max_variation_regular_price', '9000'),
(65, 8, '_min_regular_price_variation_id', '17'),
(66, 8, '_max_regular_price_variation_id', '16'),
(67, 8, '_min_variation_sale_price', NULL),
(68, 8, '_max_variation_sale_price', NULL),
(69, 8, '_min_sale_price_variation_id', NULL),
(70, 8, '_max_sale_price_variation_id', NULL),
(73, 17, '_sku', ''),
(74, 17, '_thumbnail_id', '0'),
(75, 17, '_virtual', 'no'),
(76, 17, '_downloadable', 'no'),
(77, 17, '_weight', ''),
(78, 17, '_length', ''),
(79, 17, '_width', ''),
(80, 17, '_height', ''),
(81, 17, '_manage_stock', 'no'),
(82, 17, '_regular_price', '6000'),
(83, 17, '_sale_price', ''),
(84, 17, '_sale_price_dates_from', ''),
(85, 17, '_sale_price_dates_to', ''),
(86, 17, '_price', '6000'),
(87, 17, '_download_limit', ''),
(88, 17, '_download_expiry', ''),
(89, 17, '_downloadable_files', ''),
(90, 17, '_variation_description', ''),
(91, 16, '_sku', ''),
(92, 16, '_thumbnail_id', '0'),
(93, 16, '_virtual', 'no'),
(94, 16, '_downloadable', 'no'),
(95, 16, '_weight', ''),
(96, 16, '_length', ''),
(97, 16, '_width', ''),
(98, 16, '_height', ''),
(99, 16, '_manage_stock', 'no'),
(100, 16, '_regular_price', '9000'),
(101, 16, '_sale_price', ''),
(102, 16, '_sale_price_dates_from', ''),
(103, 16, '_sale_price_dates_to', ''),
(104, 16, '_price', '9000'),
(105, 16, '_download_limit', ''),
(106, 16, '_download_expiry', ''),
(107, 16, '_downloadable_files', ''),
(108, 16, '_variation_description', ''),
(111, 8, '_default_attributes', 'a:0:{}'),
(114, 8, '_price', '6000'),
(115, 8, '_price', '9000'),
(116, 18, '_wp_trash_meta_status', 'publish'),
(117, 18, '_wp_trash_meta_time', '1489821285'),
(118, 19, '_wp_trash_meta_status', 'publish'),
(119, 19, '_wp_trash_meta_time', '1489821388'),
(120, 20, '_edit_last', '1'),
(121, 20, '_edit_lock', '1489822954:1'),
(122, 20, '_visibility', 'visible'),
(123, 20, '_stock_status', 'instock'),
(124, 20, 'total_sales', '3'),
(125, 20, '_downloadable', 'no'),
(126, 20, '_virtual', 'no'),
(127, 20, '_purchase_note', ''),
(128, 20, '_featured', 'yes'),
(129, 20, '_weight', ''),
(130, 20, '_length', ''),
(131, 20, '_width', ''),
(132, 20, '_height', ''),
(133, 20, '_sku', 'lihfoiae osihflhi'),
(134, 20, '_product_attributes', 'a:0:{}'),
(135, 20, '_regular_price', '2000000'),
(136, 20, '_sale_price', ''),
(137, 20, '_sale_price_dates_from', ''),
(138, 20, '_sale_price_dates_to', ''),
(139, 20, '_price', '2000000'),
(140, 20, '_sold_individually', ''),
(141, 20, '_manage_stock', 'no'),
(142, 20, '_backorders', 'no'),
(143, 20, '_stock', ''),
(144, 20, '_upsell_ids', 'a:0:{}'),
(145, 20, '_crosssell_ids', 'a:0:{}'),
(146, 20, '_product_version', '2.6.14'),
(147, 20, '_product_image_gallery', ''),
(151, 2, '_edit_lock', '1490548785:1'),
(152, 2, '_edit_last', '1'),
(153, 23, '_menu_item_type', 'post_type'),
(154, 23, '_menu_item_menu_item_parent', '0'),
(155, 23, '_menu_item_object_id', '2'),
(156, 23, '_menu_item_object', 'page'),
(157, 23, '_menu_item_target', ''),
(158, 23, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(159, 23, '_menu_item_xfn', ''),
(160, 23, '_menu_item_url', ''),
(179, 27, '_edit_last', '1'),
(180, 27, '_wp_page_template', 'default'),
(181, 27, '_edit_lock', '1491312889:1'),
(182, 30, '_wp_trash_meta_status', 'publish'),
(183, 30, '_wp_trash_meta_time', '1489942846'),
(184, 31, '_wp_trash_meta_status', 'publish'),
(185, 31, '_wp_trash_meta_time', '1489942888'),
(186, 32, '_wp_trash_meta_status', 'publish'),
(187, 32, '_wp_trash_meta_time', '1489943328'),
(188, 2, '_g_feedback_shortcode', '[contact-field label="Name" type="name" required="1"/][contact-field label="Email" type="email" required="1"/][contact-field label="Website" type="url"/][contact-field label="Comment" type="textarea" required="1"/]'),
(189, 2, '_g_feedback_shortcode_atts', 'a:6:{s:2:"to";s:21:"kqthang1505@gmail.com";s:7:"subject";s:45:"[Thiết bị y tế Tâm Đức] Trang chủ";s:12:"show_subject";s:2:"no";s:6:"widget";i:0;s:2:"id";i:2;s:18:"submit_button_text";s:35:"YOUR CUSTOM SUBMIT BUTTON TEXT HERE";}'),
(190, 33, '_feedback_extra_fields', 'a:0:{}'),
(191, 33, '_feedback_email', 'a:2:{s:2:"to";a:1:{i:0;s:21:"kqthang1505@gmail.com";}s:7:"message";s:339:"<b>Name:</b> fsdfsdfsdf<br /><br />\n<b>Email:</b> neo.tk12345@gmail.com<br /><br />\n<b>Comment:</b> alskdja ljkas aas daksjdh ahaksjhalsdsd<br /><br />\n\n<hr />\nTime: 20/03/2017 at 00:26<br />\nĐịa chỉ IP: ::1<br />\nContact Form URL: http://localhost:8888/thiet-bi-y-te/<br />\n\nSent by a verified "Thiết bị y tế Tâm Đức" user.";}'),
(192, 33, '_edit_lock', '1489944767:1'),
(193, 34, '_edit_last', '1'),
(194, 34, '_edit_lock', '1491474891:1'),
(195, 34, '_wp_page_template', 'template-contact.php'),
(196, 34, '_g_feedback_shortcode', '[contact-field label=\'Name\' type=\'name\' required=\'1\'/][contact-field label=\'Email\' type=\'email\' required=\'1\'/][contact-field label=\'Phản hồi\' type=\'textarea\' required=\'1\'/]'),
(197, 34, '_g_feedback_shortcode_atts', 'a:6:{s:2:"to";s:21:"kqthang1505@gmail.com";s:7:"subject";s:44:"[Thiết bị y tế Tâm Đức] Liên hệ";s:12:"show_subject";s:2:"no";s:6:"widget";i:0;s:2:"id";i:34;s:18:"submit_button_text";s:12:"Gửi &#187;";}'),
(198, 36, '_menu_item_type', 'post_type'),
(199, 36, '_menu_item_menu_item_parent', '0'),
(200, 36, '_menu_item_object_id', '34'),
(201, 36, '_menu_item_object', 'page'),
(202, 36, '_menu_item_target', ''),
(203, 36, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(204, 36, '_menu_item_xfn', ''),
(205, 36, '_menu_item_url', ''),
(220, 40, '_wp_trash_meta_status', 'publish'),
(221, 40, '_wp_trash_meta_time', '1490542329'),
(222, 43, '_wp_trash_meta_status', 'publish'),
(223, 43, '_wp_trash_meta_time', '1490542513'),
(224, 44, '_wp_trash_meta_status', 'publish'),
(225, 44, '_wp_trash_meta_time', '1490543598'),
(229, 46, '_wp_trash_meta_status', 'publish'),
(230, 46, '_wp_trash_meta_time', '1490804804'),
(231, 47, '_wp_trash_meta_status', 'publish'),
(232, 47, '_wp_trash_meta_time', '1490890773'),
(237, 50, '_order_key', 'wc_order_58dfe21150f1e'),
(238, 50, '_order_currency', 'VND'),
(239, 50, '_prices_include_tax', 'no'),
(240, 50, '_customer_ip_address', '127.0.0.1'),
(241, 50, '_customer_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(242, 50, '_customer_user', '1'),
(243, 50, '_created_via', 'checkout'),
(244, 50, '_cart_hash', '1398bd82e994634a4103f7db165d66dd'),
(245, 50, '_order_version', '2.6.14'),
(246, 50, '_billing_first_name', 'sdjflksjflks'),
(247, 50, '_billing_last_name', 'sdlkfjsdlfkjds'),
(248, 50, '_billing_company', ''),
(249, 50, '_billing_email', 'kqthang1505@gmail.com'),
(250, 50, '_billing_phone', '938289473783927982'),
(251, 50, '_billing_country', 'VN'),
(252, 50, '_billing_address_1', 'lldfsjlsfldjlsjdf'),
(253, 50, '_billing_address_2', ''),
(254, 50, '_billing_city', 'sdflkdsklsdflkdfskl'),
(255, 50, '_billing_state', ''),
(256, 50, '_billing_postcode', ''),
(257, 50, '_shipping_first_name', 'sdjflksjflks'),
(258, 50, '_shipping_last_name', 'sdlkfjsdlfkjds'),
(259, 50, '_shipping_company', ''),
(260, 50, '_shipping_country', 'VN'),
(261, 50, '_shipping_address_1', 'lldfsjlsfldjlsjdf'),
(262, 50, '_shipping_address_2', ''),
(263, 50, '_shipping_city', 'sdflkdsklsdflkdfskl'),
(264, 50, '_shipping_state', ''),
(265, 50, '_shipping_postcode', ''),
(266, 50, '_payment_method', 'cod'),
(267, 50, '_payment_method_title', 'Trả tiền mặt khi nhận hàng'),
(268, 50, '_order_shipping', ''),
(269, 50, '_cart_discount', '0'),
(270, 50, '_cart_discount_tax', '0'),
(271, 50, '_order_tax', '0'),
(272, 50, '_order_shipping_tax', '0'),
(273, 50, '_order_total', '4012000.00'),
(274, 50, '_download_permissions_granted', '1'),
(275, 50, '_recorded_sales', 'yes'),
(276, 50, '_order_stock_reduced', '1'),
(277, 50, '_edit_lock', '1491067401:1'),
(278, 52, '_order_key', 'wc_order_58e0612f295a4'),
(279, 52, '_order_currency', 'VND'),
(280, 52, '_prices_include_tax', 'no'),
(281, 52, '_customer_ip_address', '127.0.0.1'),
(282, 52, '_customer_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(283, 52, '_customer_user', '1'),
(284, 52, '_created_via', 'checkout'),
(285, 52, '_cart_hash', '1c061cc17977f8f4de3892a667287562'),
(286, 52, '_order_version', '2.6.14'),
(287, 52, '_billing_first_name', 'sdjflksjflks'),
(288, 52, '_billing_last_name', 'sdlkfjsdlfkjds'),
(289, 52, '_billing_company', ''),
(290, 52, '_billing_email', 'kqthang1505@gmail.com'),
(291, 52, '_billing_phone', '938289473783927982'),
(292, 52, '_billing_country', 'VN'),
(293, 52, '_billing_address_1', 'lldfsjlsfldjlsjdf'),
(294, 52, '_billing_address_2', ''),
(295, 52, '_billing_city', 'sdflkdsklsdflkdfskl'),
(296, 52, '_billing_state', ''),
(297, 52, '_billing_postcode', ''),
(298, 52, '_shipping_first_name', 'sdjflksjflks'),
(299, 52, '_shipping_last_name', 'sdlkfjsdlfkjds'),
(300, 52, '_shipping_company', ''),
(301, 52, '_shipping_country', 'VN'),
(302, 52, '_shipping_address_1', 'lldfsjlsfldjlsjdf'),
(303, 52, '_shipping_address_2', ''),
(304, 52, '_shipping_city', 'sdflkdsklsdflkdfskl'),
(305, 52, '_shipping_state', ''),
(306, 52, '_shipping_postcode', ''),
(307, 52, '_payment_method', 'cod'),
(308, 52, '_payment_method_title', 'Trả tiền mặt khi nhận hàng'),
(309, 52, '_order_shipping', ''),
(310, 52, '_cart_discount', '0'),
(311, 52, '_cart_discount_tax', '0'),
(312, 52, '_order_tax', '0'),
(313, 52, '_order_shipping_tax', '0'),
(314, 52, '_order_total', '2000000.00'),
(315, 52, '_download_permissions_granted', '1'),
(316, 52, '_recorded_sales', 'yes'),
(317, 52, '_order_stock_reduced', '1'),
(318, 52, '_completed_date', '2017-04-02 09:26:21'),
(319, 20, '_wc_rating_count', 'a:1:{i:4;s:1:"1";}'),
(320, 20, '_wc_review_count', '1'),
(321, 20, '_wc_average_rating', '4.00'),
(322, 53, '_edit_last', '1'),
(323, 53, '_edit_lock', '1491130929:1'),
(324, 53, '_wp_page_template', 'default'),
(325, 53, '_wp_trash_meta_status', 'publish'),
(326, 53, '_wp_trash_meta_time', '1491131076'),
(327, 53, '_wp_desired_post_slug', 'blog'),
(328, 55, '_edit_last', '1'),
(329, 55, '_edit_lock', '1491446857:1'),
(332, 7, '_edit_lock', '1491131675:1'),
(333, 7, '_edit_last', '1'),
(334, 7, '_wp_page_template', 'template-fullwidth.php'),
(335, 58, '_menu_item_type', 'post_type'),
(336, 58, '_menu_item_menu_item_parent', '0'),
(337, 58, '_menu_item_object_id', '7'),
(338, 58, '_menu_item_object', 'page'),
(339, 58, '_menu_item_target', ''),
(340, 58, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(341, 58, '_menu_item_xfn', ''),
(342, 58, '_menu_item_url', ''),
(344, 4, '_edit_lock', '1491131813:1'),
(345, 4, '_edit_last', '1'),
(346, 4, '_wp_page_template', 'default'),
(347, 5, '_edit_lock', '1491131828:1'),
(348, 5, '_edit_last', '1'),
(349, 5, '_wp_page_template', 'default'),
(350, 61, '_menu_item_type', 'post_type'),
(351, 61, '_menu_item_menu_item_parent', '0'),
(352, 61, '_menu_item_object_id', '4'),
(353, 61, '_menu_item_object', 'page'),
(354, 61, '_menu_item_target', ''),
(355, 61, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(356, 61, '_menu_item_xfn', ''),
(357, 61, '_menu_item_url', ''),
(359, 1, '_edit_lock', '1491132780:1'),
(360, 1, '_edit_last', '1'),
(362, 64, '_wp_attached_file', '2017/04/logo.png'),
(363, 64, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:500;s:4:"file";s:16:"2017/04/logo.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"logo-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:16:"logo-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:16:"logo-180x180.png";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:16:"logo-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(364, 65, '_wp_attached_file', '2017/04/cropped-logo.png'),
(365, 65, '_wp_attachment_context', 'custom-logo'),
(366, 65, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:110;s:6:"height";i:110;s:4:"file";s:24:"2017/04/cropped-logo.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(367, 66, '_wp_attached_file', '2017/04/cropped-logo-1.png'),
(368, 66, '_wp_attachment_context', 'site-icon'),
(369, 66, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:512;s:6:"height";i:512;s:4:"file";s:26:"2017/04/cropped-logo-1.png";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"cropped-logo-1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"cropped-logo-1-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:26:"cropped-logo-1-180x180.png";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}s:12:"shop_catalog";a:4:{s:4:"file";s:26:"cropped-logo-1-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:13:"site_icon-270";a:4:{s:4:"file";s:26:"cropped-logo-1-270x270.png";s:5:"width";i:270;s:6:"height";i:270;s:9:"mime-type";s:9:"image/png";}s:13:"site_icon-192";a:4:{s:4:"file";s:26:"cropped-logo-1-192x192.png";s:5:"width";i:192;s:6:"height";i:192;s:9:"mime-type";s:9:"image/png";}s:13:"site_icon-180";a:4:{s:4:"file";s:26:"cropped-logo-1-180x180.png";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}s:12:"site_icon-32";a:4:{s:4:"file";s:24:"cropped-logo-1-32x32.png";s:5:"width";i:32;s:6:"height";i:32;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(370, 67, '_wp_trash_meta_status', 'publish'),
(371, 67, '_wp_trash_meta_time', '1491233905'),
(372, 68, '_wp_attached_file', '2017/04/17760340_2271412016416520_956207783_n.jpg'),
(373, 68, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:672;s:4:"file";s:49:"2017/04/17760340_2271412016416520_956207783_n.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:49:"17760340_2271412016416520_956207783_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:49:"17760340_2271412016416520_956207783_n-300x210.jpg";s:5:"width";i:300;s:6:"height";i:210;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:49:"17760340_2271412016416520_956207783_n-768x538.jpg";s:5:"width";i:768;s:6:"height";i:538;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:49:"17760340_2271412016416520_956207783_n-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:49:"17760340_2271412016416520_956207783_n-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:49:"17760340_2271412016416520_956207783_n-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(374, 6, '_edit_lock', '1491312877:1'),
(375, 6, '_edit_last', '1'),
(376, 6, '_wp_page_template', 'default'),
(377, 20, '_wp_trash_meta_status', 'publish'),
(378, 20, '_wp_trash_meta_time', '1491313041'),
(379, 20, '_wp_desired_post_slug', 'ban-san-khoa'),
(380, 20, '_wp_trash_meta_comments_status', 'a:1:{i:5;s:1:"1";}'),
(381, 10, '_wp_trash_meta_status', 'publish'),
(382, 10, '_wp_trash_meta_time', '1491313041'),
(383, 10, '_wp_desired_post_slug', 'product-8-variation'),
(384, 11, '_wp_trash_meta_status', 'publish'),
(385, 11, '_wp_trash_meta_time', '1491313041'),
(386, 11, '_wp_desired_post_slug', 'product-8-variation-2'),
(387, 12, '_wp_trash_meta_status', 'publish'),
(388, 12, '_wp_trash_meta_time', '1491313041'),
(389, 12, '_wp_desired_post_slug', 'product-8-variation-3'),
(390, 13, '_wp_trash_meta_status', 'publish'),
(391, 13, '_wp_trash_meta_time', '1491313041'),
(392, 13, '_wp_desired_post_slug', 'product-8-variation-4'),
(393, 14, '_wp_trash_meta_status', 'publish'),
(394, 14, '_wp_trash_meta_time', '1491313041'),
(395, 14, '_wp_desired_post_slug', 'product-8-variation-5'),
(396, 15, '_wp_trash_meta_status', 'publish'),
(397, 15, '_wp_trash_meta_time', '1491313041'),
(398, 15, '_wp_desired_post_slug', 'product-8-variation-6'),
(399, 16, '_wp_trash_meta_status', 'publish'),
(400, 16, '_wp_trash_meta_time', '1491313041'),
(401, 16, '_wp_desired_post_slug', 'product-8-variation-7'),
(402, 17, '_wp_trash_meta_status', 'publish'),
(403, 17, '_wp_trash_meta_time', '1491313041'),
(404, 17, '_wp_desired_post_slug', 'product-8-variation-8'),
(405, 8, '_wp_trash_meta_status', 'publish'),
(406, 8, '_wp_trash_meta_time', '1491313041'),
(407, 8, '_wp_desired_post_slug', 'dao-mo'),
(408, 70, '_wc_review_count', '0'),
(409, 70, '_wc_rating_count', 'a:0:{}'),
(410, 70, '_wc_average_rating', '0'),
(411, 71, '_wp_attached_file', '2017/04/17504495_1065917686847181_4047669029422804919_o.jpg'),
(412, 71, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1354;s:6:"height";i:848;s:4:"file";s:59:"2017/04/17504495_1065917686847181_4047669029422804919_o.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:59:"17504495_1065917686847181_4047669029422804919_o-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:59:"17504495_1065917686847181_4047669029422804919_o-300x188.jpg";s:5:"width";i:300;s:6:"height";i:188;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:59:"17504495_1065917686847181_4047669029422804919_o-768x481.jpg";s:5:"width";i:768;s:6:"height";i:481;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:60:"17504495_1065917686847181_4047669029422804919_o-1024x641.jpg";s:5:"width";i:1024;s:6:"height";i:641;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:59:"17504495_1065917686847181_4047669029422804919_o-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:59:"17504495_1065917686847181_4047669029422804919_o-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:59:"17504495_1065917686847181_4047669029422804919_o-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(413, 72, '_wp_attached_file', '2017/04/17620235_1065917626847187_7455844497062875170_o.jpg'),
(414, 72, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1342;s:6:"height";i:1213;s:4:"file";s:59:"2017/04/17620235_1065917626847187_7455844497062875170_o.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:59:"17620235_1065917626847187_7455844497062875170_o-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:59:"17620235_1065917626847187_7455844497062875170_o-300x271.jpg";s:5:"width";i:300;s:6:"height";i:271;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:59:"17620235_1065917626847187_7455844497062875170_o-768x694.jpg";s:5:"width";i:768;s:6:"height";i:694;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:60:"17620235_1065917626847187_7455844497062875170_o-1024x926.jpg";s:5:"width";i:1024;s:6:"height";i:926;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:59:"17620235_1065917626847187_7455844497062875170_o-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:59:"17620235_1065917626847187_7455844497062875170_o-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:59:"17620235_1065917626847187_7455844497062875170_o-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(415, 73, '_wp_attached_file', '2017/04/17546684_1065917586847191_9045933183536499323_o.jpg'),
(416, 73, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1350;s:6:"height";i:1129;s:4:"file";s:59:"2017/04/17546684_1065917586847191_9045933183536499323_o.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:59:"17546684_1065917586847191_9045933183536499323_o-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:59:"17546684_1065917586847191_9045933183536499323_o-300x251.jpg";s:5:"width";i:300;s:6:"height";i:251;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:59:"17546684_1065917586847191_9045933183536499323_o-768x642.jpg";s:5:"width";i:768;s:6:"height";i:642;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:60:"17546684_1065917586847191_9045933183536499323_o-1024x856.jpg";s:5:"width";i:1024;s:6:"height";i:856;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:59:"17546684_1065917586847191_9045933183536499323_o-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:59:"17546684_1065917586847191_9045933183536499323_o-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:59:"17546684_1065917586847191_9045933183536499323_o-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(417, 74, '_wp_attached_file', '2017/04/17635411_1065917623513854_2485058034244721328_o.jpg'),
(418, 74, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1354;s:6:"height";i:1533;s:4:"file";s:59:"2017/04/17635411_1065917623513854_2485058034244721328_o.jpg";s:5:"sizes";a:7:{s:9:"thumbnail";a:4:{s:4:"file";s:59:"17635411_1065917623513854_2485058034244721328_o-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:59:"17635411_1065917623513854_2485058034244721328_o-265x300.jpg";s:5:"width";i:265;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:59:"17635411_1065917623513854_2485058034244721328_o-768x870.jpg";s:5:"width";i:768;s:6:"height";i:870;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:60:"17635411_1065917623513854_2485058034244721328_o-904x1024.jpg";s:5:"width";i:904;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:59:"17635411_1065917623513854_2485058034244721328_o-180x180.jpg";s:5:"width";i:180;s:6:"height";i:180;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:59:"17635411_1065917623513854_2485058034244721328_o-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:59:"17635411_1065917623513854_2485058034244721328_o-600x600.jpg";s:5:"width";i:600;s:6:"height";i:600;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(419, 70, '_edit_last', '1'),
(420, 70, '_edit_lock', '1491353177:1'),
(421, 70, '_sku', ''),
(422, 70, '_regular_price', '1400000'),
(423, 70, '_sale_price', ''),
(424, 70, '_sale_price_dates_from', ''),
(425, 70, '_sale_price_dates_to', ''),
(426, 70, 'total_sales', '0'),
(427, 70, '_tax_status', 'taxable'),
(428, 70, '_tax_class', ''),
(429, 70, '_manage_stock', 'no'),
(430, 70, '_backorders', 'no'),
(431, 70, '_sold_individually', 'no'),
(432, 70, '_weight', ''),
(433, 70, '_length', ''),
(434, 70, '_width', ''),
(435, 70, '_height', ''),
(436, 70, '_upsell_ids', 'a:0:{}'),
(437, 70, '_crosssell_ids', 'a:0:{}'),
(438, 70, '_purchase_note', ''),
(439, 70, '_default_attributes', 'a:0:{}'),
(440, 70, '_virtual', 'no'),
(441, 70, '_downloadable', 'no'),
(442, 70, '_product_image_gallery', ''),
(443, 70, '_download_limit', '-1'),
(444, 70, '_download_expiry', '-1'),
(445, 70, '_stock', NULL),
(446, 70, '_stock_status', 'instock'),
(447, 70, '_product_attributes', 'a:3:{s:22:"massage-toi-cac-vi-tri";a:6:{s:4:"name";s:28:"Massage tới các vị trí";s:5:"value";s:43:"cổ, gáy, lưng, thắt lưng, chân, ...";s:8:"position";i:0;s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:0;}s:18:"cong-suat-tieu-thu";a:6:{s:4:"name";s:24:"Công suất tiêu thụ";s:5:"value";s:3:"22W";s:8:"position";i:1;s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:0;}s:10:"nguon-dien";a:6:{s:4:"name";s:15:"Nguồn điện";s:5:"value";s:11:"220V - 50Hz";s:8:"position";i:2;s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:0;}}'),
(448, 70, '_product_version', '3.0.0'),
(449, 70, '_price', '1400000'),
(450, 70, '_thumbnail_id', '74'),
(451, 86, '_form', '<label> Tên (bắt buộc)\n    [text* Name] </label>\n\n<label> Email (bắt buộc)\n    [email* Email] </label>\n\n<label> Nội dung\n    [textarea Message] </label>\n\n[submit "Gửi"]'),
(452, 86, '_mail', 'a:9:{s:6:"active";b:1;s:7:"subject";s:43:"Thiết bị y tế Tâm Đức "[Subject]"";s:6:"sender";s:28:"[Name] <wordpress@thang.dev>";s:9:"recipient";s:21:"kqthang1505@gmail.com";s:4:"body";s:174:"Người gửi: [Name] <[Email]>\nTiêu đề: [Subject]\n\nNội dung:\n[Message]\n\n-- \nEmail được gởi từ Thiết bị y tế Tâm Đức (http://thang.dev/thiet-bi-y-te)";s:18:"additional_headers";s:17:"Reply-To: [Email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(453, 86, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:48:"Thiết bị y tế Tâm Đức "[your-subject]"";s:6:"sender";s:53:"Thiết bị y tế Tâm Đức <wordpress@thang.dev>";s:9:"recipient";s:12:"[your-email]";s:4:"body";s:142:"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Thiết bị y tế Tâm Đức (http://thang.dev/thiet-bi-y-te)";s:18:"additional_headers";s:31:"Reply-To: kqthang1505@gmail.com";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(454, 86, '_messages', 'a:23:{s:12:"mail_sent_ok";s:35:"Cảm ơn phản hồi của bạn.";s:12:"mail_sent_ng";s:71:"There was an error trying to send your message. Please try again later.";s:16:"validation_error";s:61:"One or more fields have an error. Please check and try again.";s:4:"spam";s:71:"There was an error trying to send your message. Please try again later.";s:12:"accept_terms";s:69:"You must accept the terms and conditions before sending your message.";s:16:"invalid_required";s:22:"The field is required.";s:16:"invalid_too_long";s:22:"The field is too long.";s:17:"invalid_too_short";s:23:"The field is too short.";s:12:"invalid_date";s:29:"The date format is incorrect.";s:14:"date_too_early";s:44:"The date is before the earliest one allowed.";s:13:"date_too_late";s:41:"The date is after the latest one allowed.";s:13:"upload_failed";s:46:"There was an unknown error uploading the file.";s:24:"upload_file_type_invalid";s:49:"You are not allowed to upload files of this type.";s:21:"upload_file_too_large";s:20:"The file is too big.";s:23:"upload_failed_php_error";s:38:"There was an error uploading the file.";s:14:"invalid_number";s:29:"The number format is invalid.";s:16:"number_too_small";s:47:"The number is smaller than the minimum allowed.";s:16:"number_too_large";s:46:"The number is larger than the maximum allowed.";s:23:"quiz_answer_not_correct";s:36:"The answer to the quiz is incorrect.";s:17:"captcha_not_match";s:31:"Your entered code is incorrect.";s:13:"invalid_email";s:38:"The e-mail address entered is invalid.";s:11:"invalid_url";s:19:"The URL is invalid.";s:11:"invalid_tel";s:32:"The telephone number is invalid.";}'),
(455, 86, '_additional_settings', ''),
(456, 86, '_locale', 'vi'),
(457, 89, '_email', 'kqthang1505@gmail.com'),
(458, 89, '_name', 'yte@tamduc'),
(459, 89, '_props', 'a:2:{s:10:"first_name";s:0:"";s:9:"last_name";s:0:"";}'),
(460, 89, '_last_contacted', '2017-04-06 10:43:00'),
(461, 90, '_email', 'wapuu@wordpress.example'),
(462, 90, '_name', 'A WordPress Commenter'),
(463, 90, '_props', 'a:0:{}'),
(464, 90, '_last_contacted', '2017-04-06 10:42:30'),
(465, 91, '_subject', 'sdsdfdsfdsf'),
(466, 91, '_from', 'sdfdsfdsfdsf <kqthang1505@gmail.com>'),
(467, 91, '_from_name', 'sdfdsfdsfdsf'),
(468, 91, '_from_email', 'kqthang1505@gmail.com'),
(469, 91, '_field_your-name', 'sdfdsfdsfdsf'),
(470, 91, '_field_your-email', 'kqthang1505@gmail.com'),
(471, 91, '_field_your-subject', 'sdsdfdsfdsf'),
(472, 91, '_field_your-message', 'dfssfddsdfsdfsdfs'),
(473, 91, '_fields', 'a:4:{s:9:"your-name";N;s:10:"your-email";N;s:12:"your-subject";N;s:12:"your-message";N;}'),
(474, 91, '_meta', 'a:11:{s:9:"remote_ip";s:9:"127.0.0.1";s:10:"user_agent";s:121:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36";s:3:"url";s:39:"http://thang.dev/thiet-bi-y-te/lien-he/";s:4:"date";s:10:"06/04/2017";s:4:"time";s:5:"10:43";s:7:"post_id";s:2:"34";s:9:"post_name";s:7:"lien-he";s:10:"post_title";s:10:"Liên hệ";s:8:"post_url";s:39:"http://thang.dev/thiet-bi-y-te/lien-he/";s:11:"post_author";s:10:"yte@tamduc";s:17:"post_author_email";s:21:"kqthang1505@gmail.com";}'),
(475, 91, '_akismet', NULL),
(478, 92, '_subject', '[your-subject]'),
(479, 92, '_from', '[your-name] <[your-email]>'),
(480, 92, '_from_name', '[your-name]'),
(481, 92, '_from_email', '[your-email]'),
(482, 92, '_field_name', 'kllkfjsldkfjsd'),
(483, 92, '_field_email', 'kqthang1505@gmail.com'),
(484, 92, '_field_message', 'asfhsdafh ajsfkasjdhf asjdhf sakjdlasjdfs\naskjdfhaskjdfh \nsdflsahdfkjsdsadf\n'),
(485, 92, '_fields', 'a:3:{s:4:"Name";N;s:5:"Email";N;s:7:"Message";N;}'),
(486, 92, '_meta', 'a:11:{s:9:"remote_ip";s:9:"127.0.0.1";s:10:"user_agent";s:121:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36";s:3:"url";s:39:"http://thang.dev/thiet-bi-y-te/lien-he/";s:4:"date";s:10:"06/04/2017";s:4:"time";s:5:"13:17";s:7:"post_id";s:2:"34";s:9:"post_name";s:7:"lien-he";s:10:"post_title";s:10:"Liên hệ";s:8:"post_url";s:39:"http://thang.dev/thiet-bi-y-te/lien-he/";s:11:"post_author";s:10:"yte@tamduc";s:17:"post_author_email";s:21:"kqthang1505@gmail.com";}'),
(487, 92, '_akismet', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2017-03-16 16:01:27', '2017-03-16 16:01:27', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'closed', 'open', '', 'hello-world', '', '', '2017-04-02 18:24:20', '2017-04-02 11:24:20', '', 0, 'http://localhost:8888/thiet-bi-y-te/?p=1', 0, 'post', '', 1),
(2, 1, '2017-03-16 16:01:27', '2017-03-16 16:01:27', '', 'Trang chủ', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2017-03-19 22:11:28', '2017-03-19 15:11:28', '', 0, 'http://localhost:8888/thiet-bi-y-te/?page_id=2', 0, 'page', '', 0),
(4, 1, '2017-03-16 16:37:31', '2017-03-16 16:37:31', '', 'Sản phẩm', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2017-04-02 18:19:11', '2017-04-02 11:19:11', '', 0, 'http://localhost:8888/thiet-bi-y-te/shop/', 0, 'page', '', 0),
(5, 1, '2017-03-16 16:37:31', '2017-03-16 16:37:31', '[woocommerce_cart]', 'Giỏ hàng', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2017-04-02 18:19:30', '2017-04-02 11:19:30', '', 0, 'http://localhost:8888/thiet-bi-y-te/cart/', 0, 'page', '', 0),
(6, 1, '2017-03-16 16:37:31', '2017-03-16 16:37:31', '[woocommerce_checkout]', 'Thanh toán', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2017-04-04 20:36:58', '2017-04-04 13:36:58', '', 0, 'http://localhost:8888/thiet-bi-y-te/checkout/', 0, 'page', '', 0),
(7, 1, '2017-03-16 16:37:31', '2017-03-16 16:37:31', '', 'Tin tức', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2017-04-02 18:16:53', '2017-04-02 11:16:53', '', 0, 'http://localhost:8888/thiet-bi-y-te/my-account/', 0, 'page', '', 0),
(8, 1, '2017-03-16 16:40:59', '2017-03-16 16:40:59', 'Dao mổ số 7', 'Dao mổ', '', 'trash', 'open', 'closed', '', 'dao-mo__trashed', '', '', '2017-04-04 20:37:21', '2017-04-04 13:37:21', '', 0, 'http://localhost:8888/thiet-bi-y-te/?post_type=product&#038;p=8', 0, 'product', '', 0),
(10, 1, '2017-03-16 16:57:41', '2017-03-16 16:57:41', '', 'Dao mổ &ndash; Kích thước: 10, Chất liệu: Sắt', '', 'trash', 'closed', 'closed', '', 'product-8-variation__trashed', '', '', '2017-04-04 20:37:21', '2017-04-04 13:37:21', '', 8, 'http://localhost:8888/thiet-bi-y-te/product/dao-mo/', 0, 'product_variation', '', 0),
(11, 1, '2017-03-16 16:57:41', '2017-03-16 16:57:41', '', 'Dao mổ &ndash; Kích thước: 10, Chất liệu: Nhôm', '', 'trash', 'closed', 'closed', '', 'product-8-variation-2__trashed', '', '', '2017-04-04 20:37:21', '2017-04-04 13:37:21', '', 8, 'http://localhost:8888/thiet-bi-y-te/product/dao-mo/', 0, 'product_variation', '', 0),
(12, 1, '2017-03-16 16:57:41', '2017-03-16 16:57:41', '', 'Dao mổ &ndash; Kích thước: 9, Chất liệu: Sắt', '', 'trash', 'closed', 'closed', '', 'product-8-variation-3__trashed', '', '', '2017-04-04 20:37:21', '2017-04-04 13:37:21', '', 8, 'http://localhost:8888/thiet-bi-y-te/product/dao-mo/', 0, 'product_variation', '', 0),
(13, 1, '2017-03-16 16:57:41', '2017-03-16 16:57:41', '', 'Dao mổ &ndash; Kích thước: 9, Chất liệu: Nhôm', '', 'trash', 'closed', 'closed', '', 'product-8-variation-4__trashed', '', '', '2017-04-04 20:37:21', '2017-04-04 13:37:21', '', 8, 'http://localhost:8888/thiet-bi-y-te/product/dao-mo/', 0, 'product_variation', '', 0),
(14, 1, '2017-03-16 16:57:41', '2017-03-16 16:57:41', '', 'Dao mổ &ndash; Kích thước: 8, Chất liệu: Sắt', '', 'trash', 'closed', 'closed', '', 'product-8-variation-5__trashed', '', '', '2017-04-04 20:37:21', '2017-04-04 13:37:21', '', 8, 'http://localhost:8888/thiet-bi-y-te/product/dao-mo/', 0, 'product_variation', '', 0),
(15, 1, '2017-03-16 16:57:41', '2017-03-16 16:57:41', '', 'Dao mổ &ndash; Kích thước: 8, Chất liệu: Nhôm', '', 'trash', 'closed', 'closed', '', 'product-8-variation-6__trashed', '', '', '2017-04-04 20:37:21', '2017-04-04 13:37:21', '', 8, 'http://localhost:8888/thiet-bi-y-te/product/dao-mo/', 0, 'product_variation', '', 0),
(16, 1, '2017-03-16 16:57:41', '2017-03-16 16:57:41', '', 'Dao mổ &ndash; Kích thước: 7, Chất liệu: Sắt', '', 'trash', 'closed', 'closed', '', 'product-8-variation-7__trashed', '', '', '2017-04-04 20:37:21', '2017-04-04 13:37:21', '', 8, 'http://localhost:8888/thiet-bi-y-te/product/dao-mo/', 0, 'product_variation', '', 0),
(17, 1, '2017-03-16 16:57:41', '2017-03-16 16:57:41', '', 'Dao mổ &ndash; Kích thước: 7, Chất liệu: Nhôm', '', 'trash', 'closed', 'closed', '', 'product-8-variation-8__trashed', '', '', '2017-04-04 20:37:21', '2017-04-04 13:37:21', '', 8, 'http://localhost:8888/thiet-bi-y-te/product/dao-mo/', 0, 'product_variation', '', 0),
(18, 1, '2017-03-18 07:14:45', '2017-03-18 07:14:45', '{\n    "blogdescription": {\n        "value": "Healthy & Beauty Care",\n        "type": "option",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '07e1a5c4-fc41-4e3b-a8a4-1d3b176cd545', '', '', '2017-03-18 07:14:45', '2017-03-18 07:14:45', '', 0, 'http://localhost:8888/thiet-bi-y-te/?p=18', 0, 'customize_changeset', '', 0),
(19, 1, '2017-03-18 07:16:28', '2017-03-18 07:16:28', '{\n    "show_on_front": {\n        "value": "page",\n        "type": "option",\n        "user_id": 1\n    },\n    "page_on_front": {\n        "value": "2",\n        "type": "option",\n        "user_id": 1\n    },\n    "storefront-child::storefront_layout": {\n        "value": "left",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'c5688728-0731-492b-8d91-c7f5000fd2ba', '', '', '2017-03-18 07:16:28', '2017-03-18 07:16:28', '', 0, 'http://localhost:8888/thiet-bi-y-te/?p=19', 0, 'customize_changeset', '', 0),
(20, 1, '2017-03-18 07:20:11', '2017-03-18 07:20:11', 'Bàn sản khoa', 'Bàn sản khoa', '', 'trash', 'open', 'closed', '', 'ban-san-khoa__trashed', '', '', '2017-04-04 20:37:21', '2017-04-04 13:37:21', '', 0, 'http://localhost:8888/thiet-bi-y-te/?post_type=product&#038;p=20', 0, 'product', '', 1),
(21, 1, '2017-03-18 14:31:52', '2017-03-18 07:31:52', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\r\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</blockquote>\r\n...or something like this:\r\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\r\nAs a new WordPress user, you should go to <a href="http://localhost:8888/thiet-bi-y-te/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-03-18 14:31:52', '2017-03-18 07:31:52', '', 2, 'http://localhost:8888/thiet-bi-y-te/2017/03/18/2-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2017-03-18 16:11:28', '2017-03-18 09:11:28', '', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-03-18 16:11:28', '2017-03-18 09:11:28', '', 2, 'http://localhost:8888/thiet-bi-y-te/2017/03/18/2-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2017-03-18 16:22:02', '2017-03-18 09:22:02', ' ', '', '', 'publish', 'closed', 'closed', '', '23', '', '', '2017-04-02 18:21:16', '2017-04-02 11:21:16', '', 0, 'http://localhost:8888/thiet-bi-y-te/?p=23', 1, 'nav_menu_item', '', 0),
(26, 1, '2017-03-19 22:11:28', '2017-03-19 15:11:28', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-03-19 22:11:28', '2017-03-19 15:11:28', '', 2, 'http://localhost:8888/thiet-bi-y-te/2017/03/19/2-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2017-03-19 22:12:06', '2017-03-19 15:12:06', '', 'Chính sách bảo mật thông tin', '', 'publish', 'closed', 'closed', '', 'chinh-sach-bao-mat-thong-tin', '', '', '2017-03-19 22:12:06', '2017-03-19 15:12:06', '', 0, 'http://localhost:8888/thiet-bi-y-te/?page_id=27', 0, 'page', '', 0),
(28, 1, '2017-03-19 22:12:06', '2017-03-19 15:12:06', '', 'Chính sách bảo mật thông tin', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2017-03-19 22:12:06', '2017-03-19 15:12:06', '', 27, 'http://localhost:8888/thiet-bi-y-te/2017/03/19/27-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2017-03-20 00:00:46', '2017-03-19 17:00:46', '{\n    "storefront-child::shcs_contact_address": {\n        "value": "125 DC11, ph\\u01b0\\u1eddng S\\u01a1n K\\u1ef3, qu\\u1eadn T\\u00e2n Ph\\u00fa",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "storefront-child::shcs_contact_phone_number": {\n        "value": "0962054200",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "storefront-child::shcs_contact_email_address": {\n        "value": "kqthang1505@gmail.com",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '9a679d16-da5e-4f51-b174-34fa11b1b5df', '', '', '2017-03-20 00:00:46', '2017-03-19 17:00:46', '', 0, 'http://localhost:8888/thiet-bi-y-te/?p=30', 0, 'customize_changeset', '', 0),
(31, 1, '2017-03-20 00:01:28', '2017-03-19 17:01:28', '{\n    "storefront-child::shcs_contact_form": {\n        "value": true,\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'a0b53380-df73-4b13-8324-19d0d986ed39', '', '', '2017-03-20 00:01:28', '2017-03-19 17:01:28', '', 0, 'http://localhost:8888/thiet-bi-y-te/?p=31', 0, 'customize_changeset', '', 0),
(32, 1, '2017-03-20 00:08:48', '2017-03-19 17:08:48', '{\n    "storefront::shcs_contact_address": {\n        "value": "125 DC11, ph\\u01b0\\u1eddng S\\u01a1n K\\u1ef3, qu\\u1eadn T\\u00e2n Ph\\u00fa",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "storefront::shcs_contact_phone_number": {\n        "value": "0962054200",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "storefront::shcs_contact_email_address": {\n        "value": "kqthang1505@gmail.com",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '1685ecab-95c4-4ca3-a9c1-f1f33096ade4', '', '', '2017-03-20 00:08:48', '2017-03-19 17:08:48', '', 0, 'http://localhost:8888/thiet-bi-y-te/2017/03/20/1685ecab-95c4-4ca3-a9c1-f1f33096ade4/', 0, 'customize_changeset', '', 0),
(33, 0, '2017-03-20 00:26:49', '2017-03-19 17:26:49', 'alskdja ljkas aas daksjdh ahaksjhalsdsd\n<!--more-->\nAUTHOR: fsdfsdfsdf\nAUTHOR EMAIL: neo.tk12345@gmail.com\nAUTHOR URL: \nSUBJECT: [Thiết bị y tế Tâm Đức] Trang chủ\nIP: ::1\nArray\n(\n    [1_Name] =&gt; fsdfsdfsdf\n    [2_Email] =&gt; neo.tk12345@gmail.com\n    [3_Comment] =&gt; alskdja ljkas aas daksjdh ahaksjhalsdsd\n    [entry_title] =&gt; Trang chủ\n    [entry_permalink] =&gt; http://localhost:8888/thiet-bi-y-te/\n    [feedback_id] =&gt; c503c8713a6384848e52d4e7a5a26a77\n)\n', 'fsdfsdfsdf - 2017-03-20 00:26:49', '', 'publish', 'closed', 'closed', '', 'c503c8713a6384848e52d4e7a5a26a77', '', '', '2017-03-20 00:26:49', '2017-03-19 17:26:49', '', 2, 'http://localhost:8888/thiet-bi-y-te/?post_type=feedback&p=33', 0, 'feedback', '', 0),
(34, 1, '2017-03-20 00:35:53', '2017-03-19 17:35:53', '[contact-form-7 id="86" title="Contact form 1"]\r\n\r\n<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15677.422864818953!2d106.6939804!3d10.78404405!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbdaa1764f3e3e4ca!2zQuG7h25oIHZpw6puIG3huq90IFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaA!5e0!3m2!1svi!2s!4v1491446273192" width="100%" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe>', 'Liên hệ', '', 'publish', 'closed', 'closed', '', 'lien-he', '', '', '2017-04-06 10:02:21', '2017-04-06 03:02:21', '', 0, 'http://localhost:8888/thiet-bi-y-te/?page_id=34', 0, 'page', '', 0),
(35, 1, '2017-03-20 00:35:53', '2017-03-19 17:35:53', '[contact-form][contact-field label=\'Name\' type=\'name\' required=\'1\'/][contact-field label=\'Email\' type=\'email\' required=\'1\'/][contact-field label=\'Phản hồi\' type=\'textarea\' required=\'1\'/][/contact-form]', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2017-03-20 00:35:53', '2017-03-19 17:35:53', '', 34, 'http://localhost:8888/thiet-bi-y-te/2017/03/20/34-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2017-03-20 00:37:19', '2017-03-19 17:37:19', ' ', '', '', 'publish', 'closed', 'closed', '', '36', '', '', '2017-04-02 18:21:16', '2017-04-02 11:21:16', '', 0, 'http://localhost:8888/thiet-bi-y-te/?p=36', 4, 'nav_menu_item', '', 0),
(40, 1, '2017-03-26 22:32:09', '2017-03-26 15:32:09', '{\n    "storefront-child::header_image": {\n        "value": "http://localhost:8888/thiet-bi-y-te/wp-content/uploads/2017/03/cropped-IMG_4841.jpg",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "storefront-child::header_image_data": {\n        "value": {\n            "url": "http://localhost:8888/thiet-bi-y-te/wp-content/uploads/2017/03/cropped-IMG_4841.jpg",\n            "thumbnail_url": "http://localhost:8888/thiet-bi-y-te/wp-content/uploads/2017/03/cropped-IMG_4841.jpg",\n            "timestamp": 1490542223327,\n            "attachment_id": 42,\n            "width": 1949,\n            "height": 500\n        },\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "storefront-child::storefront_header_text_color": {\n        "value": "#0a0909",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "storefront-child::storefront_header_link_color": {\n        "value": "#0a0504",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "storefront-child::background_color": {\n        "value": "#f8f8f8",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "storefront-child::storefront_button_background_color": {\n        "value": "#dd4f4f",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'b12fc3e1-4f8c-49d7-b426-08bfb00bbdae', '', '', '2017-03-26 22:32:09', '2017-03-26 15:32:09', '', 0, 'http://localhost:8888/thiet-bi-y-te/?p=40', 0, 'customize_changeset', '', 0),
(43, 1, '2017-03-26 22:35:13', '2017-03-26 15:35:13', '{\n    "storefront-child::header_image": {\n        "value": "http://localhost:8888/thiet-bi-y-te/wp-content/uploads/2017/03/cropped-IMG_6629.jpg",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "storefront-child::header_image_data": {\n        "value": {\n            "attachment_id": 39,\n            "url": "http://localhost:8888/thiet-bi-y-te/wp-content/uploads/2017/03/cropped-IMG_6629.jpg",\n            "thumbnail_url": "http://localhost:8888/thiet-bi-y-te/wp-content/uploads/2017/03/cropped-IMG_6629.jpg",\n            "alt_text": "",\n            "width": 1949,\n            "height": 500,\n            "timestamp": [\n                "1490542112"\n            ]\n        },\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '606100a3-bcf3-45c6-ab85-c9493ebc2dc4', '', '', '2017-03-26 22:35:13', '2017-03-26 15:35:13', '', 0, 'http://localhost:8888/thiet-bi-y-te/606100a3-bcf3-45c6-ab85-c9493ebc2dc4/', 0, 'customize_changeset', '', 0),
(44, 1, '2017-03-26 22:53:18', '2017-03-26 15:53:18', '{\n    "storefront-child::storefront_header_text_color": {\n        "value": "#212121",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "storefront-child::storefront_header_link_color": {\n        "value": "#ffffff",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '9f696432-4cdb-4889-8571-7beb4b2143e7', '', '', '2017-03-26 22:53:18', '2017-03-26 15:53:18', '', 0, 'http://localhost:8888/thiet-bi-y-te/9f696432-4cdb-4889-8571-7beb4b2143e7/', 0, 'customize_changeset', '', 0),
(46, 1, '2017-03-29 23:26:44', '2017-03-29 16:26:44', '{\n    "storefront-child::header_image": {\n        "value": "http://tbyt.thang.dev/wp-content/uploads/2017/03/cropped-IMG_6629.jpg",\n        "type": "theme_mod",\n        "user_id": 1\n    },\n    "storefront-child::header_image_data": {\n        "value": {\n            "attachment_id": 39,\n            "url": "http://tbyt.thang.dev/wp-content/uploads/2017/03/cropped-IMG_6629.jpg",\n            "thumbnail_url": "http://tbyt.thang.dev/wp-content/uploads/2017/03/cropped-IMG_6629.jpg",\n            "alt_text": "",\n            "width": 1949,\n            "height": 500,\n            "timestamp": [\n                "1490542513"\n            ]\n        },\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'dc68adfa-66af-477c-a4b5-a82210715d5b', '', '', '2017-03-29 23:26:44', '2017-03-29 16:26:44', '', 0, 'http://tbyt.thang.dev/dc68adfa-66af-477c-a4b5-a82210715d5b/', 0, 'customize_changeset', '', 0),
(47, 1, '2017-03-30 23:19:33', '2017-03-30 16:19:33', '{\n    "storefront-child::storefront_accent_color": {\n        "value": "#c0392b",\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '70fed898-7f0e-45f2-b124-8471fe7aa41e', '', '', '2017-03-30 23:19:33', '2017-03-30 16:19:33', '', 0, 'http://tbyt.thang.dev/?p=47', 0, 'customize_changeset', '', 0),
(50, 1, '2017-04-02 00:23:29', '2017-04-01 17:23:29', '', 'Order &ndash; Tháng Tư 2, 2017 @ 12:23 Sáng', 'klklsdfsfldfsjldflsjdkfjsldkfjldks', 'wc-processing', 'open', 'closed', 'order_58dfe2114852b', 'don-hang-apr-01-2017-0523-pm', '', '', '2017-04-02 00:23:29', '2017-04-01 17:23:29', '', 0, 'http://tbyt.thang.dev/?post_type=shop_order&#038;p=50', 0, 'shop_order', '', 1),
(51, 1, '2017-04-02 00:23:44', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'open', 'open', '', '', '', '', '2017-04-02 00:23:44', '0000-00-00 00:00:00', '', 0, 'http://tbyt.thang.dev/?p=51', 0, 'post', '', 0),
(52, 1, '2017-04-02 09:25:51', '2017-04-02 02:25:51', '', 'Order &ndash; Tháng Tư 2, 2017 @ 09:25 Sáng', '', 'wc-completed', 'open', 'closed', 'order_58e0612f1d5e1', 'don-hang-apr-02-2017-0225-am', '', '', '2017-04-02 09:26:21', '2017-04-02 02:26:21', '', 0, 'http://tbyt.thang.dev/?post_type=shop_order&#038;p=52', 0, 'shop_order', '', 2),
(53, 1, '2017-04-02 17:57:34', '2017-04-02 10:57:34', '', 'Blog', '', 'trash', 'closed', 'closed', '', 'blog__trashed', '', '', '2017-04-02 18:04:36', '2017-04-02 11:04:36', '', 0, 'http://tbyt.thang.dev/?page_id=53', 0, 'page', '', 0),
(54, 1, '2017-04-02 17:57:34', '2017-04-02 10:57:34', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '53-revision-v1', '', '', '2017-04-02 17:57:34', '2017-04-02 10:57:34', '', 53, 'http://tbyt.thang.dev/53-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2017-04-02 18:15:16', '2017-04-02 11:15:16', 'askasdjadshasd', 'kasdjkshdhkasd', '', 'publish', 'open', 'open', '', 'kasdjkshdhkasd', '', '', '2017-04-02 18:15:16', '2017-04-02 11:15:16', '', 0, 'http://tbyt.thang.dev/?p=55', 0, 'post', '', 0),
(56, 1, '2017-04-02 18:15:16', '2017-04-02 11:15:16', 'askasdjadshasd', 'kasdjkshdhkasd', '', 'inherit', 'closed', 'closed', '', '55-revision-v1', '', '', '2017-04-02 18:15:16', '2017-04-02 11:15:16', '', 55, 'http://tbyt.thang.dev/55-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2017-04-02 18:16:53', '2017-04-02 11:16:53', '', 'Tin tức', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2017-04-02 18:16:53', '2017-04-02 11:16:53', '', 7, 'http://tbyt.thang.dev/7-revision-v1/', 0, 'revision', '', 0),
(58, 1, '2017-04-02 18:17:52', '2017-04-02 11:17:52', ' ', '', '', 'publish', 'closed', 'closed', '', '58', '', '', '2017-04-02 18:21:16', '2017-04-02 11:21:16', '', 0, 'http://tbyt.thang.dev/?p=58', 3, 'nav_menu_item', '', 0),
(59, 1, '2017-04-02 18:19:11', '2017-04-02 11:19:11', '', 'Sản phẩm', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-04-02 18:19:11', '2017-04-02 11:19:11', '', 4, 'http://tbyt.thang.dev/4-revision-v1/', 0, 'revision', '', 0),
(60, 1, '2017-04-02 18:19:30', '2017-04-02 11:19:30', '[woocommerce_cart]', 'Giỏ hàng', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2017-04-02 18:19:30', '2017-04-02 11:19:30', '', 5, 'http://tbyt.thang.dev/5-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2017-04-02 18:21:16', '2017-04-02 11:21:16', ' ', '', '', 'publish', 'closed', 'closed', '', '61', '', '', '2017-04-02 18:21:16', '2017-04-02 11:21:16', '', 0, 'http://tbyt.thang.dev/?p=61', 2, 'nav_menu_item', '', 0),
(62, 1, '2017-04-02 18:24:20', '2017-04-02 11:24:20', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2017-04-02 18:24:20', '2017-04-02 11:24:20', '', 1, 'http://tbyt.thang.dev/1-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2017-04-03 22:37:37', '2017-04-03 15:37:37', '', 'logo', '', 'inherit', 'closed', 'closed', '', 'logo-2', '', '', '2017-04-03 22:37:37', '2017-04-03 15:37:37', '', 0, 'http://thang.dev/thiet-bi-y-te/wp-content/uploads/2017/04/logo.png', 0, 'attachment', 'image/png', 0),
(65, 1, '2017-04-03 22:37:59', '2017-04-03 15:37:59', 'http://thang.dev/thiet-bi-y-te/wp-content/uploads/2017/04/cropped-logo.png', 'cropped-logo.png', '', 'inherit', 'closed', 'closed', '', 'cropped-logo-png', '', '', '2017-04-03 22:37:59', '2017-04-03 15:37:59', '', 0, 'http://thang.dev/thiet-bi-y-te/wp-content/uploads/2017/04/cropped-logo.png', 0, 'attachment', 'image/png', 0),
(66, 1, '2017-04-03 22:38:15', '2017-04-03 15:38:15', 'http://thang.dev/thiet-bi-y-te/wp-content/uploads/2017/04/cropped-logo-1.png', 'cropped-logo-1.png', '', 'inherit', 'closed', 'closed', '', 'cropped-logo-1-png', '', '', '2017-04-03 22:38:15', '2017-04-03 15:38:15', '', 0, 'http://thang.dev/thiet-bi-y-te/wp-content/uploads/2017/04/cropped-logo-1.png', 0, 'attachment', 'image/png', 0),
(67, 1, '2017-04-03 22:38:25', '2017-04-03 15:38:25', '{\n    "site_icon": {\n        "value": 66,\n        "type": "option",\n        "user_id": 1\n    },\n    "storefront-child::custom_logo": {\n        "value": 65,\n        "type": "theme_mod",\n        "user_id": 1\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '58454537-4833-4169-9dd4-d71e21358c76', '', '', '2017-04-03 22:38:25', '2017-04-03 15:38:25', '', 0, 'http://thang.dev/thiet-bi-y-te/58454537-4833-4169-9dd4-d71e21358c76/', 0, 'customize_changeset', '', 0),
(68, 1, '2017-04-04 20:35:14', '2017-04-04 13:35:14', '', '17760340_2271412016416520_956207783_n', '', 'inherit', 'closed', 'closed', '', '17760340_2271412016416520_956207783_n', '', '', '2017-04-04 20:35:14', '2017-04-04 13:35:14', '', 0, 'http://thang.dev/thiet-bi-y-te/wp-content/uploads/2017/04/17760340_2271412016416520_956207783_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(69, 1, '2017-04-04 20:36:58', '2017-04-04 13:36:58', '[woocommerce_checkout]', 'Thanh toán', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2017-04-04 20:36:58', '2017-04-04 13:36:58', '', 6, 'http://thang.dev/thiet-bi-y-te/6-revision-v1/', 0, 'revision', '', 0),
(70, 1, '2017-04-04 20:55:29', '2017-04-04 13:55:29', 'Massage cơ và các huyệt đạo với 6 con lăn. Tích hợp chức năng nhiệt nhẹ nhàng bằng tia hồng ngoại.', 'Massage lưng vai gáy - Mas 632', 'Massage cơ và các huyệt đạo với 6 con lăn. Tích hợp chức năng nhiệt nhẹ nhàng bằng tia hồng ngoại.', 'publish', 'closed', 'closed', '', 'massage-lung-vai-gay-mas-632', '', '', '2017-04-04 20:56:01', '2017-04-04 13:56:01', '', 0, 'http://thang.dev/thiet-bi-y-te/?post_type=product&#038;p=70', 0, 'product', '', 0),
(71, 1, '2017-04-04 20:38:13', '2017-04-04 13:38:13', '', '17504495_1065917686847181_4047669029422804919_o', '', 'inherit', 'closed', 'closed', '', '17504495_1065917686847181_4047669029422804919_o', '', '', '2017-04-04 20:38:13', '2017-04-04 13:38:13', '', 70, 'http://thang.dev/thiet-bi-y-te/wp-content/uploads/2017/04/17504495_1065917686847181_4047669029422804919_o.jpg', 0, 'attachment', 'image/jpeg', 0),
(72, 1, '2017-04-04 20:38:15', '2017-04-04 13:38:15', '', '17620235_1065917626847187_7455844497062875170_o', '', 'inherit', 'closed', 'closed', '', '17620235_1065917626847187_7455844497062875170_o', '', '', '2017-04-04 20:38:15', '2017-04-04 13:38:15', '', 70, 'http://thang.dev/thiet-bi-y-te/wp-content/uploads/2017/04/17620235_1065917626847187_7455844497062875170_o.jpg', 0, 'attachment', 'image/jpeg', 0),
(73, 1, '2017-04-04 20:38:16', '2017-04-04 13:38:16', '', '17546684_1065917586847191_9045933183536499323_o', '', 'inherit', 'closed', 'closed', '', '17546684_1065917586847191_9045933183536499323_o', '', '', '2017-04-04 20:38:16', '2017-04-04 13:38:16', '', 70, 'http://thang.dev/thiet-bi-y-te/wp-content/uploads/2017/04/17546684_1065917586847191_9045933183536499323_o.jpg', 0, 'attachment', 'image/jpeg', 0),
(74, 1, '2017-04-04 20:38:18', '2017-04-04 13:38:18', '', '17635411_1065917623513854_2485058034244721328_o', '', 'inherit', 'closed', 'closed', '', '17635411_1065917623513854_2485058034244721328_o', '', '', '2017-04-04 20:38:18', '2017-04-04 13:38:18', '', 70, 'http://thang.dev/thiet-bi-y-te/wp-content/uploads/2017/04/17635411_1065917623513854_2485058034244721328_o.jpg', 0, 'attachment', 'image/jpeg', 0),
(75, 1, '2017-04-06 09:28:48', '2017-04-06 02:28:48', '', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2017-04-06 09:28:48', '2017-04-06 02:28:48', '', 34, 'http://thang.dev/thiet-bi-y-te/34-revision-v1/', 0, 'revision', '', 0),
(76, 1, '2017-04-06 09:32:30', '2017-04-06 02:32:30', 'dfssdfsdfsdf', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2017-04-06 09:32:30', '2017-04-06 02:32:30', '', 34, 'http://thang.dev/thiet-bi-y-te/34-revision-v1/', 0, 'revision', '', 0),
(77, 1, '2017-04-06 09:38:12', '2017-04-06 02:38:12', '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15677.422864818953!2d106.6939804!3d10.78404405!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbdaa1764f3e3e4ca!2zQuG7h25oIHZpw6puIG3huq90IFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaA!5e0!3m2!1svi!2s!4v1491446273192" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '34-autosave-v1', '', '', '2017-04-06 09:38:12', '2017-04-06 02:38:12', '', 34, 'http://thang.dev/thiet-bi-y-te/34-autosave-v1/', 0, 'revision', '', 0),
(78, 1, '2017-04-06 09:38:23', '2017-04-06 02:38:23', '<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15677.422864818953!2d106.6939804!3d10.78404405!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbdaa1764f3e3e4ca!2zQuG7h25oIHZpw6puIG3huq90IFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaA!5e0!3m2!1svi!2s!4v1491446273192" width="100%" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2017-04-06 09:38:23', '2017-04-06 02:38:23', '', 34, 'http://thang.dev/thiet-bi-y-te/34-revision-v1/', 0, 'revision', '', 0),
(79, 1, '2017-04-06 09:39:07', '2017-04-06 02:39:07', '<form action="" method="post" class="contact-form">\r\n				<p>\r\n					<label for="g34-name" class="grunion-field-label name">Name<span>(yêu cầu)</span></label>\r\n					<input type="text" name="g34-name" id="g34-name" value="" class="name" required aria-required="true">\r\n				</p>\r\n				  \r\n				<p>\r\n					<label for="g34-email" class="grunion-field-label email">Email<span>(yêu cầu)</span></label>\r\n					<input type="email" name="g34-email" id="g34-email" value="" class="email" required aria-required="true">\r\n				</p>\r\n				  \r\n				<p>\r\n				<label for="contact-form-comment-g34-phnhi" class="grunion-field-label textarea">Phản hồi<span>(yêu cầu)</	span></label>\r\n				<textarea name="g34-phnhi" id="contact-form-comment-g34-phnhi" rows="5" class="textarea" required 	aria-required=" true"></textarea>\r\n				</p>\r\n				<p class="contact-submit">\r\n					<input type="submit" value="Gửi »" class="pushbutton-wide" value="submit">\r\n				</p>\r\n			</form>\r\n\r\n<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15677.422864818953!2d106.6939804!3d10.78404405!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbdaa1764f3e3e4ca!2zQuG7h25oIHZpw6puIG3huq90IFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaA!5e0!3m2!1svi!2s!4v1491446273192" width="100%" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2017-04-06 09:39:07', '2017-04-06 02:39:07', '', 34, 'http://thang.dev/thiet-bi-y-te/34-revision-v1/', 0, 'revision', '', 0),
(80, 1, '2017-04-06 09:40:40', '2017-04-06 02:40:40', '<form class="contact-form" action="" method="post">Liên hệ\r\n\r\n<label class="grunion-field-label name" for="g34-name">Name(yêu cầu)</label>\r\n<input id="g34-name" class="name" name="g34-name" required="" type="text" value="" />\r\n\r\n<label class="grunion-field-label email" for="g34-email">Email(yêu cầu)</label>\r\n<input id="g34-email" class="email" name="g34-email" required="" type="email" value="" />\r\n\r\n<label class="grunion-field-label textarea" for="contact-form-comment-g34-phnhi">Phản hồi(yêu cầu)</label>\r\n<textarea id="contact-form-comment-g34-phnhi" class="textarea" name="g34-phnhi" required="" rows="5"></textarea>\r\n<p class="contact-submit"><input class="pushbutton-wide" type="submit" value="Gửi »" /></p>\r\n\r\n</form><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15677.422864818953!2d106.6939804!3d10.78404405!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbdaa1764f3e3e4ca!2zQuG7h25oIHZpw6puIG3huq90IFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaA!5e0!3m2!1svi!2s!4v1491446273192" width="100%" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2017-04-06 09:40:40', '2017-04-06 02:40:40', '', 34, 'http://thang.dev/thiet-bi-y-te/34-revision-v1/', 0, 'revision', '', 0),
(81, 1, '2017-04-06 09:41:07', '2017-04-06 02:41:07', '<form class="contact-form" action="" method="post">\r\n<h1>Liên hệ</h1>\r\n<label class="grunion-field-label name" for="g34-name">Name(yêu cầu)</label>\r\n<input id="g34-name" class="name" name="g34-name" required="" type="text" value="" />\r\n\r\n<label class="grunion-field-label email" for="g34-email">Email(yêu cầu)</label>\r\n<input id="g34-email" class="email" name="g34-email" required="" type="email" value="" />\r\n\r\n<label class="grunion-field-label textarea" for="contact-form-comment-g34-phnhi">Phản hồi(yêu cầu)</label>\r\n<textarea id="contact-form-comment-g34-phnhi" class="textarea" name="g34-phnhi" required="" rows="5"></textarea>\r\n<p class="contact-submit"><input class="pushbutton-wide" type="submit" value="Gửi »" /></p>\r\n\r\n</form><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15677.422864818953!2d106.6939804!3d10.78404405!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbdaa1764f3e3e4ca!2zQuG7h25oIHZpw6puIG3huq90IFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaA!5e0!3m2!1svi!2s!4v1491446273192" width="100%" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2017-04-06 09:41:07', '2017-04-06 02:41:07', '', 34, 'http://thang.dev/thiet-bi-y-te/34-revision-v1/', 0, 'revision', '', 0),
(82, 1, '2017-04-06 09:45:49', '2017-04-06 02:45:49', '<p>This is the WPForms preview page. All your form previews will be handled on this page.</p><p>The page is set to private, so it is not publically accessible. Please do not delete this page :) .</p>', 'WPForms Preview', '', 'private', 'closed', 'closed', '', 'wpforms-preview', '', '', '2017-04-06 09:45:49', '2017-04-06 02:45:49', '', 0, 'http://thang.dev/thiet-bi-y-te/wpforms-preview/', 0, 'page', '', 0),
(83, 1, '2017-04-06 09:46:40', '2017-04-06 02:46:40', '{"id":"83","field_id":3,"fields":[{"id":"0","type":"name","label":"Name","format":"first-last","description":"","required":"1","size":"medium","simple_placeholder":"","simple_default":"","first_placeholder":"","first_default":"","middle_placeholder":"","middle_default":"","last_placeholder":"","last_default":"","css":""},{"id":"1","type":"email","label":"E-mail","description":"","required":"1","size":"medium","placeholder":"","confirmation_placeholder":"","default_value":"","css":""},{"id":"2","type":"textarea","label":"Comment or Message","description":"","required":"1","size":"medium","placeholder":"","css":""}],"settings":{"form_title":"Contact Form","form_desc":"","form_class":"","submit_text":"Submit","submit_text_processing":"Sending...","submit_class":"","honeypot":"1","notification_enable":"1","notifications":{"1":{"email":"{admin_email}","subject":"New Entry: Contact Form","sender_name":"{field_id=\\"0\\"}","sender_address":"{field_id=\\"1\\"}","replyto":"","message":"{all_fields}"}},"confirmation_type":"message","confirmation_message":"Thanks for contacting us! We will be in touch with you shortly.","confirmation_message_scroll":"1","confirmation_page":"27","confirmation_redirect":""},"meta":{"template":"contact"}}', 'Contact Form', '', 'publish', 'closed', 'closed', '', 'contact-form', '', '', '2017-04-06 09:46:49', '2017-04-06 02:46:49', '', 0, 'http://thang.dev/thiet-bi-y-te/?post_type=wpforms&#038;p=83', 0, 'wpforms', '', 0),
(84, 1, '2017-04-06 09:47:20', '2017-04-06 02:47:20', '<form class="contact-form" action="" method="post">[wpforms id="83" title="true"]\r\n\r\n&nbsp;\r\n\r\n</form><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15677.422864818953!2d106.6939804!3d10.78404405!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbdaa1764f3e3e4ca!2zQuG7h25oIHZpw6puIG3huq90IFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaA!5e0!3m2!1svi!2s!4v1491446273192" width="100%" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2017-04-06 09:47:20', '2017-04-06 02:47:20', '', 34, 'http://thang.dev/thiet-bi-y-te/34-revision-v1/', 0, 'revision', '', 0),
(85, 1, '2017-04-06 09:47:47', '2017-04-06 02:47:47', '[wpforms id="83" title="true"]\r\n\r\n<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15677.422864818953!2d106.6939804!3d10.78404405!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbdaa1764f3e3e4ca!2zQuG7h25oIHZpw6puIG3huq90IFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaA!5e0!3m2!1svi!2s!4v1491446273192" width="100%" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2017-04-06 09:47:47', '2017-04-06 02:47:47', '', 34, 'http://thang.dev/thiet-bi-y-te/34-revision-v1/', 0, 'revision', '', 0),
(86, 1, '2017-04-06 10:01:09', '2017-04-06 03:01:09', '<label> Tên (bắt buộc)\r\n    [text* Name] </label>\r\n\r\n<label> Email (bắt buộc)\r\n    [email* Email] </label>\r\n\r\n<label> Nội dung\r\n    [textarea Message] </label>\r\n\r\n[submit "Gửi"]\n1\nThiết bị y tế Tâm Đức "[Subject]"\n[Name] <wordpress@thang.dev>\nkqthang1505@gmail.com\nNgười gửi: [Name] <[Email]>\r\nTiêu đề: [Subject]\r\n\r\nNội dung:\r\n[Message]\r\n\r\n-- \r\nEmail được gởi từ Thiết bị y tế Tâm Đức (http://thang.dev/thiet-bi-y-te)\nReply-To: [Email]\n\n\n\n\nThiết bị y tế Tâm Đức "[your-subject]"\nThiết bị y tế Tâm Đức <wordpress@thang.dev>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Thiết bị y tế Tâm Đức (http://thang.dev/thiet-bi-y-te)\nReply-To: kqthang1505@gmail.com\n\n\n\nCảm ơn phản hồi của bạn.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Form Liên hệ', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2017-04-06 13:18:30', '2017-04-06 06:18:30', '', 0, 'http://thang.dev/thiet-bi-y-te/?post_type=wpcf7_contact_form&#038;p=86', 0, 'wpcf7_contact_form', '', 0),
(87, 1, '2017-04-06 10:02:03', '2017-04-06 03:02:03', '&nbsp;\r\n\r\n<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15677.422864818953!2d106.6939804!3d10.78404405!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbdaa1764f3e3e4ca!2zQuG7h25oIHZpw6puIG3huq90IFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaA!5e0!3m2!1svi!2s!4v1491446273192" width="100%" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2017-04-06 10:02:03', '2017-04-06 03:02:03', '', 34, 'http://thang.dev/thiet-bi-y-te/34-revision-v1/', 0, 'revision', '', 0),
(88, 1, '2017-04-06 10:02:21', '2017-04-06 03:02:21', '[contact-form-7 id="86" title="Contact form 1"]\r\n\r\n<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15677.422864818953!2d106.6939804!3d10.78404405!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbdaa1764f3e3e4ca!2zQuG7h25oIHZpw6puIG3huq90IFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaA!5e0!3m2!1svi!2s!4v1491446273192" width="100%" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2017-04-06 10:02:21', '2017-04-06 03:02:21', '', 34, 'http://thang.dev/thiet-bi-y-te/34-revision-v1/', 0, 'revision', '', 0),
(89, 1, '2017-04-06 10:43:00', '2017-04-06 03:43:00', 'kqthang1505@gmail.com\nyte@tamduc', 'kqthang1505@gmail.com', '', 'publish', 'closed', 'closed', '', 'kqthang1505-gmail-com', '', '', '2017-04-06 10:43:00', '2017-04-06 03:43:00', '', 0, 'http://thang.dev/thiet-bi-y-te/kqthang1505-gmail-com/', 0, 'flamingo_contact', '', 0),
(90, 1, '2017-04-06 10:42:30', '2017-04-06 03:42:30', 'wapuu@wordpress.example\nA WordPress Commenter', 'wapuu@wordpress.example', '', 'publish', 'closed', 'closed', '', 'wapuu-wordpress-example', '', '', '2017-04-06 10:42:30', '2017-04-06 03:42:30', '', 0, 'http://thang.dev/thiet-bi-y-te/wapuu-wordpress-example/', 0, 'flamingo_contact', '', 0),
(91, 1, '2017-04-06 10:43:00', '2017-04-06 03:43:00', 'sdfdsfdsfdsf\nkqthang1505@gmail.com\nsdsdfdsfdsf\ndfssfddsdfsdfsdfs', 'sdsdfdsfdsf', '', 'publish', 'closed', 'closed', '', 'sdsdfdsfdsf', '', '', '2017-04-06 10:43:00', '2017-04-06 03:43:00', '', 0, 'http://thang.dev/thiet-bi-y-te/?post_type=flamingo_inbound&p=91', 0, 'flamingo_inbound', '', 0),
(92, 1, '2017-04-06 13:17:41', '2017-04-06 06:17:41', 'kllkfjsldkfjsd\nkqthang1505@gmail.com\nasfhsdafh ajsfkasjdhf asjdhf sakjdlasjdfs\naskjdfhaskjdfh \nsdflsahdfkjsdsadf', '[your-subject]', '', 'publish', 'closed', 'closed', '', 'your-subject', '', '', '2017-04-06 13:17:41', '2017-04-06 06:17:41', '', 0, 'http://thang.dev/thiet-bi-y-te/?post_type=flamingo_inbound&p=92', 0, 'flamingo_inbound', '', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_termmeta`
--

INSERT INTO `wp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 6, 'order', '0'),
(2, 6, 'display_type', ''),
(3, 6, 'thumbnail_id', '0'),
(4, 7, 'order', '0'),
(5, 7, 'display_type', ''),
(6, 7, 'thumbnail_id', '0'),
(7, 8, 'order', '0'),
(8, 8, 'display_type', ''),
(9, 8, 'thumbnail_id', '0'),
(10, 9, 'order', '0'),
(11, 9, 'display_type', ''),
(12, 9, 'thumbnail_id', '0'),
(13, 10, 'order', '0'),
(14, 10, 'display_type', 'subcategories'),
(15, 10, 'thumbnail_id', '0'),
(16, 11, 'order', '0'),
(17, 11, 'display_type', 'subcategories'),
(18, 11, 'thumbnail_id', '0'),
(19, 12, 'order', '0'),
(20, 12, 'display_type', 'subcategories'),
(21, 12, 'thumbnail_id', '0'),
(22, 13, 'order', '0'),
(23, 13, 'display_type', 'subcategories'),
(24, 13, 'thumbnail_id', '0'),
(25, 14, 'order', '0'),
(26, 14, 'display_type', 'subcategories'),
(27, 14, 'thumbnail_id', '0'),
(28, 15, 'order', '0'),
(29, 15, 'display_type', 'subcategories'),
(30, 15, 'thumbnail_id', '0'),
(31, 16, 'order', '0'),
(32, 16, 'display_type', 'subcategories'),
(33, 16, 'thumbnail_id', '0'),
(34, 17, 'order', '0'),
(35, 17, 'display_type', 'subcategories'),
(36, 17, 'thumbnail_id', '0'),
(37, 17, 'product_count_product_cat', '0'),
(38, 9, 'product_count_product_cat', '0');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'Nội thất y tế', 'noi-that-y-te', 0),
(7, 'Chăm sóc sức khỏe', 'cham-soc-suc-khoe', 0),
(8, 'Thiết bị y tế', 'thiet-bi-y-te', 0),
(9, 'Dụng cụ y khoa', 'dung-cu-y-khoa', 0),
(10, 'Bàn y tế', 'ban-y-te', 0),
(11, 'Bàn sản khoa', 'ban-san-khoa', 0),
(12, 'Ống nghe, huyết áp cơ', 'ong-nghe-huyet-ap-co', 0),
(13, 'Máy đo huyết áp', 'may-do-huyet-ap', 0),
(14, 'Chuẩn đoán hình ảnh', 'chuan-doan-hinh-anh', 0),
(15, 'Thăm dò chức năng', 'tham-do-chuc-nang', 0),
(16, 'Vật tư X-Quang', 'vat-tu-x-quang', 0),
(17, 'Dụng cụ sản khoa', 'dung-cu-san-khoa', 0),
(18, 'Primary Menu', 'primary-menu', 0),
(19, 'exclude-from-search', 'exclude-from-search', 0),
(20, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(21, 'featured', 'featured', 0),
(22, 'outofstock', 'outofstock', 0),
(23, 'rated-1', 'rated-1', 0),
(24, 'rated-2', 'rated-2', 0),
(25, 'rated-3', 'rated-3', 0),
(26, 'rated-4', 'rated-4', 0),
(27, 'rated-5', 'rated-5', 0),
(28, 'Contact Form 7', 'contact-form-7', 0),
(29, 'Contact form 1', 'contact-form-1', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(8, 4, 0),
(8, 17, 0),
(20, 2, 0),
(20, 17, 0),
(20, 21, 0),
(20, 26, 0),
(23, 18, 0),
(36, 18, 0),
(55, 1, 0),
(58, 18, 0),
(61, 18, 0),
(70, 2, 0),
(91, 29, 0),
(92, 29, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 2),
(2, 2, 'product_type', '', 0, 1),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_cat', '', 0, 0),
(7, 7, 'product_cat', '', 0, 0),
(8, 8, 'product_cat', '', 0, 0),
(9, 9, 'product_cat', '', 0, 0),
(10, 10, 'product_cat', '', 6, 0),
(11, 11, 'product_cat', '', 6, 0),
(12, 12, 'product_cat', '', 7, 0),
(13, 13, 'product_cat', '', 7, 0),
(14, 14, 'product_cat', '', 8, 0),
(15, 15, 'product_cat', '', 8, 0),
(16, 16, 'product_cat', '', 9, 0),
(17, 17, 'product_cat', '', 9, 0),
(18, 18, 'nav_menu', '', 0, 4),
(19, 19, 'product_visibility', '', 0, 0),
(20, 20, 'product_visibility', '', 0, 0),
(21, 21, 'product_visibility', '', 0, 0),
(22, 22, 'product_visibility', '', 0, 0),
(23, 23, 'product_visibility', '', 0, 0),
(24, 24, 'product_visibility', '', 0, 0),
(25, 25, 'product_visibility', '', 0, 0),
(26, 26, 'product_visibility', '', 0, 0),
(27, 27, 'product_visibility', '', 0, 0),
(28, 28, 'flamingo_inbound_channel', '', 0, 0),
(29, 29, 'flamingo_inbound_channel', '', 28, 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'yte@tamduc'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'false'),
(10, 1, 'locale', ''),
(11, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(12, 1, 'wp_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', ''),
(14, 1, 'show_welcome_panel', '0'),
(15, 1, 'session_tokens', 'a:4:{s:64:"b2516e9bb28fa83850e0850fc4571c5a991faa994ee575c4b1bf1bc1283c4876";a:4:{s:10:"expiration";i:1492093351;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:120:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";s:5:"login";i:1490883751;}s:64:"8e1dbab1a5dc58f79f545e7dae276d68f186d9e8f11eea2be52b37adf9b91b09";a:4:{s:10:"expiration";i:1491484654;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:120:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";s:5:"login";i:1491311854;}s:64:"df067fef6532514bd8b2bea27520eca5285d54ef9ccdf5570696e8cd87b237f9";a:4:{s:10:"expiration";i:1491491414;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:120:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";s:5:"login";i:1491318614;}s:64:"e0b5dfd40ec6f79474c184581accfaa85c4aa9d4e01c2bc19d0962f0fdfd3b9c";a:4:{s:10:"expiration";i:1491618397;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:121:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36";s:5:"login";i:1491445597;}}'),
(16, 1, 'wp_dashboard_quick_press_last_post_id', '51'),
(17, 1, 'manageedit-shop_ordercolumnshidden', 'a:1:{i:0;s:15:"billing_address";}'),
(19, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:5:{i:0;s:30:"woocommerce_endpoints_nav_link";i:1;s:21:"add-post-type-product";i:2;s:12:"add-post_tag";i:3;s:15:"add-product_cat";i:4;s:15:"add-product_tag";}'),
(21, 1, 'nav_menu_recently_edited', '18'),
(22, 1, 'wp_user-settings', 'libraryContent=browse&editor=tinymce&hidetb=1'),
(23, 1, 'wp_user-settings-time', '1491446436'),
(24, 1, 'billing_first_name', 'sdjflksjflks'),
(25, 1, 'billing_last_name', 'sdlkfjsdlfkjds'),
(26, 1, 'billing_company', ''),
(27, 1, 'billing_address_1', 'lldfsjlsfldjlsjdf'),
(28, 1, 'billing_address_2', ''),
(29, 1, 'billing_city', 'sdflkdsklsdflkdfskl'),
(30, 1, 'billing_postcode', ''),
(31, 1, 'billing_country', 'VN'),
(32, 1, 'billing_state', ''),
(33, 1, 'billing_phone', '938289473783927982'),
(34, 1, 'billing_email', 'kqthang1505@gmail.com'),
(35, 1, 'shipping_first_name', ''),
(36, 1, 'shipping_last_name', ''),
(37, 1, 'shipping_company', ''),
(38, 1, 'shipping_address_1', ''),
(39, 1, 'shipping_address_2', ''),
(40, 1, 'shipping_city', ''),
(41, 1, 'shipping_postcode', ''),
(42, 1, 'shipping_country', ''),
(43, 1, 'shipping_state', ''),
(44, 1, 'last_update', '1491067409'),
(45, 1, 'closedpostboxes_page', 'a:0:{}'),
(46, 1, 'metaboxhidden_page', 'a:5:{i:0;s:12:"revisionsdiv";i:1;s:16:"commentstatusdiv";i:2;s:11:"commentsdiv";i:3;s:7:"slugdiv";i:4;s:9:"authordiv";}'),
(47, 1, 'meta-box-order_page', 'a:3:{s:4:"side";s:36:"submitdiv,pageparentdiv,postimagediv";s:6:"normal";s:70:"revisionsdiv,postcustom,commentstatusdiv,commentsdiv,slugdiv,authordiv";s:8:"advanced";s:0:"";}'),
(48, 1, 'screen_layout_page', '2'),
(50, 1, 'paying_customer', '1'),
(53, 1, 'jetpack_tracks_anon_id', 'jetpack:kxPG866Vgr4WtyydsFRIKYZ8'),
(54, 1, '_woocommerce_persistent_cart', 'a:1:{s:4:"cart";a:1:{s:32:"7cbbc409ec990f19c78c75bd1e06f215";a:9:{s:10:"product_id";i:70;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:2800000;s:13:"line_subtotal";d:2800000;s:8:"line_tax";d:0;s:17:"line_subtotal_tax";d:0;s:13:"line_tax_data";a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}}}}'),
(55, 1, 'closedpostboxes_post', 'a:0:{}'),
(56, 1, 'metaboxhidden_post', 'a:6:{i:0;s:11:"postexcerpt";i:1;s:13:"trackbacksdiv";i:2;s:10:"postcustom";i:3;s:11:"commentsdiv";i:4;s:7:"slugdiv";i:5;s:9:"authordiv";}'),
(57, 1, 'wpcf7_hide_welcome_panel_on', 'a:1:{i:0;s:3:"4.7";}'),
(58, 1, 'closedpostboxes_flamingo_page_flamingo_inbound', 'a:1:{i:0;s:14:"inboundmetadiv";}'),
(59, 1, 'metaboxhidden_flamingo_page_flamingo_inbound', 'a:0:{}'),
(60, 1, 'wp_r_tru_u_x', 'a:2:{s:2:"id";s:0:"";s:7:"expires";i:86400;}'),
(61, 1, 'ignore_redux_blast_1491474849', '1'),
(62, 1, 'ignore_dev_notice_3.6.4.1', '1');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'yte@tamduc', '$P$BYnqKmudT/DeySonedhAcE95k8mGPz.', 'ytetamduc', 'kqthang1505@gmail.com', '', '2017-03-16 16:01:27', '', 0, 'yte@tamduc');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_api_keys`
--

CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_log`
--

CREATE TABLE `wp_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_order_itemmeta`
--

CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_woocommerce_order_itemmeta`
--

INSERT INTO `wp_woocommerce_order_itemmeta` (`meta_id`, `order_item_id`, `meta_key`, `meta_value`) VALUES
(1, 1, '_qty', '2'),
(2, 1, '_tax_class', ''),
(3, 1, '_product_id', '8'),
(4, 1, '_variation_id', '17'),
(5, 1, '_line_subtotal', '12000'),
(6, 1, '_line_total', '12000'),
(7, 1, '_line_subtotal_tax', '0'),
(8, 1, '_line_tax', '0'),
(9, 1, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(10, 1, 'kich-thuoc', '7'),
(11, 1, 'chat-lieu', 'Nhôm'),
(12, 2, '_qty', '2'),
(13, 2, '_tax_class', ''),
(14, 2, '_product_id', '20'),
(15, 2, '_variation_id', '0'),
(16, 2, '_line_subtotal', '4000000'),
(17, 2, '_line_total', '4000000'),
(18, 2, '_line_subtotal_tax', '0'),
(19, 2, '_line_tax', '0'),
(20, 2, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(21, 3, '_qty', '1'),
(22, 3, '_tax_class', ''),
(23, 3, '_product_id', '20'),
(24, 3, '_variation_id', '0'),
(25, 3, '_line_subtotal', '2000000'),
(26, 3, '_line_total', '2000000'),
(27, 3, '_line_subtotal_tax', '0'),
(28, 3, '_line_tax', '0'),
(29, 3, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_woocommerce_order_items`
--

INSERT INTO `wp_woocommerce_order_items` (`order_item_id`, `order_item_name`, `order_item_type`, `order_id`) VALUES
(1, 'Dao mổ', 'line_item', 50),
(2, 'Bàn sản khoa', 'line_item', 50),
(3, 'Bàn sản khoa', 'line_item', 52);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_payment_tokens`
--

CREATE TABLE `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_sessions`
--

CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Đang đổ dữ liệu cho bảng `wp_woocommerce_sessions`
--

INSERT INTO `wp_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(26, '1', 'a:20:{s:4:"cart";s:313:"a:1:{s:32:"7cbbc409ec990f19c78c75bd1e06f215";a:9:{s:10:"product_id";i:70;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:2;s:10:"line_total";d:2800000;s:13:"line_subtotal";d:2800000;s:8:"line_tax";d:0;s:17:"line_subtotal_tax";d:0;s:13:"line_tax_data";a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}}}";s:15:"applied_coupons";s:6:"a:0:{}";s:23:"coupon_discount_amounts";s:6:"a:0:{}";s:27:"coupon_discount_tax_amounts";s:6:"a:0:{}";s:21:"removed_cart_contents";s:6:"a:0:{}";s:19:"cart_contents_total";d:2800000;s:5:"total";d:2800000;s:8:"subtotal";i:2800000;s:15:"subtotal_ex_tax";i:2800000;s:9:"tax_total";i:0;s:5:"taxes";s:6:"a:0:{}";s:14:"shipping_taxes";s:6:"a:0:{}";s:13:"discount_cart";i:0;s:17:"discount_cart_tax";i:0;s:14:"shipping_total";i:0;s:18:"shipping_tax_total";i:0;s:9:"fee_total";i:0;s:4:"fees";s:6:"a:0:{}";s:8:"customer";s:761:"a:24:{s:8:"postcode";s:0:"";s:4:"city";s:19:"sdflkdsklsdflkdfskl";s:9:"address_1";s:17:"lldfsjlsfldjlsjdf";s:7:"address";s:17:"lldfsjlsfldjlsjdf";s:9:"address_2";s:0:"";s:5:"state";s:0:"";s:7:"country";s:2:"VN";s:17:"shipping_postcode";s:0:"";s:13:"shipping_city";s:0:"";s:18:"shipping_address_1";s:0:"";s:16:"shipping_address";s:0:"";s:18:"shipping_address_2";s:0:"";s:14:"shipping_state";s:0:"";s:16:"shipping_country";s:2:"VN";s:13:"is_vat_exempt";b:0;s:19:"calculated_shipping";b:0;s:10:"first_name";s:12:"sdjflksjflks";s:9:"last_name";s:14:"sdlkfjsdlfkjds";s:7:"company";s:0:"";s:5:"phone";s:18:"938289473783927982";s:5:"email";s:21:"kqthang1505@gmail.com";s:19:"shipping_first_name";s:0:"";s:18:"shipping_last_name";s:0:"";s:16:"shipping_company";s:0:"";}";s:10:"wc_notices";N;}', 1491618421);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_shipping_zones`
--

CREATE TABLE `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_tax_rates`
--

CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Chỉ mục cho bảng `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Chỉ mục cho bảng `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Chỉ mục cho bảng `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Chỉ mục cho bảng `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Chỉ mục cho bảng `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Chỉ mục cho bảng `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Chỉ mục cho bảng `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Chỉ mục cho bảng `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Chỉ mục cho bảng `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(191));

--
-- Chỉ mục cho bảng `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(191),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`);

--
-- Chỉ mục cho bảng `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Chỉ mục cho bảng `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Chỉ mục cho bảng `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_key`),
  ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Chỉ mục cho bảng `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Chỉ mục cho bảng `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type` (`location_type`),
  ADD KEY `location_type_code` (`location_type`,`location_code`(90));

--
-- Chỉ mục cho bảng `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Chỉ mục cho bảng `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(191)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(191)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Chỉ mục cho bảng `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type` (`location_type`),
  ADD KEY `location_type_code` (`location_type`,`location_code`(90));

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT cho bảng `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT cho bảng `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1349;
--
-- AUTO_INCREMENT cho bảng `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=488;
--
-- AUTO_INCREMENT cho bảng `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT cho bảng `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT cho bảng `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT cho bảng `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT cho bảng `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT cho bảng `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
