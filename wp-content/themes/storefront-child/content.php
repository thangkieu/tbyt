<?php
/**
 * Template used to display post content.
 *
 * @package storefront
 */
global $post;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	/**
	 * Functions hooked in to storefront_loop_post action.
	 *
	 * @hooked storefront_post_header          - 10
	 * @hooked storefront_post_meta            - 20
	 * @hooked storefront_post_content         - 30
	 * @hooked storefront_init_structured_data - 40
	 */
	// do_action( 'storefront_loop_post' );
	 ?>
	<div class="entry-content">
	<?php

	/**
	 * Functions hooked in to storefront_post_content_before action.
	 *
	 * @hooked storefront_post_thumbnail - 10
	 */
	if ( has_post_thumbnail() ) {
		storefront_post_thumbnail( array( 200, 200 ) );
	} else { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/front/img/post-placeholder.png" alt="Post placeholder">
	<?php }

	storefront_post_header(); ?>
	
	<p class="post-excerpt">
	<?php echo wp_strip_all_tags( $post->post_content ) ?>
	</p>
	<?php do_action( 'storefront_post_content_after' );

	wp_link_pages( array(
		'before' => '<div class="page-links">' . __( 'Pages:', 'storefront' ),
		'after'  => '</div>',
	) );
	?>
	</div><!-- .entry-content -->
	<?php # storefront_post_meta();
	?>

</article><!-- #post-## -->
