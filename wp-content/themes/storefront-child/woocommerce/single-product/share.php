<?php
/**
 * Single Product Share
 *
 * Sharing plugins can hook into here or you can add your own code directly.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/share.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$product_title 	= get_the_title();
$product_url	= get_permalink();
$product_img	= wp_get_attachment_url( get_post_thumbnail_id() ); ?>

<!-- <div 
	class="fb-share-button" 
	data-href="<?php echo $product_url; ?>" 
	data-layout="button_count" 
	data-size="large"
></div> -->

<div 
	class="fb-like" 
	data-href="<?php echo $product_url; ?>" 
	data-layout="standard" 
	data-action="like" 
	data-size="small" 
	data-show-faces="true" 
	data-share="true"></div>