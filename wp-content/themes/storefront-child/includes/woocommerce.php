<?php 
function custom_woocommerce_product_add_to_cart_text() {
  global $product;
  $text = $product->is_purchasable() && $product->is_in_stock() ? __( 'Add to cart', 'woocommerce' ) : __( 'Liên hệ', 'woocommerce' );
  
  return $text;
}
add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );

function custom_woocommerce_product_add_to_cart_url() {
  global $product;
  if ( $product->is_purchasable() && $product->is_in_stock() ) {
    $url = remove_query_arg( 'added-to-cart', add_query_arg( 'add-to-cart', $product->id ) );
  } else {
    $url = get_permalink( get_page_by_path( 'lien-he' ) ) . '?ProductName=' . $product->get_name();
  }
  
  return $url;
}

add_filter( 'woocommerce_product_add_to_cart_url' , 'custom_woocommerce_product_add_to_cart_url' );

function loop_columns() {
  return 3;
}

add_filter('loop_shop_columns', 'loop_columns', 999);

function recently_args() {
  return array(
    'limit'       => 3,
    'columns'       => 3,
    'title'       => __( 'New In', 'storefront' ),
  );
}

add_filter( 'storefront_recent_products_args', 'recently_args' );