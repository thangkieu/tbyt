<?php

class CompanyInfoPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'Thông tin cửa hàng', 
            'manage_options', 
            'company-info', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'company_info' );
        ?>
        <div class="wrap">
            <h1>Thông tin cửa hàng</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'my_option_group' );
                do_settings_sections( 'company-info' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'my_option_group', // Option group
            'company_info', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Thông tin cửa hàng', // Title
            array( $this, 'print_section_info' ), // Callback
            'company-info' // Page
        );  

        add_settings_field(
            'hotline', // ID
            'Hotline', // Title 
            array( $this, 'hotline_callback' ), // Callback
            'company-info', // Page
            'setting_section_id' // Section           
        );    
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['hotline'] ) )
            $new_input['hotline'] = $input['hotline'];

        if( isset( $input['title'] ) )
            $new_input['title'] = sanitize_text_field( $input['title'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your settings below:';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function hotline_callback()
    {
        printf(
            '<input type="text" id="hotline" name="company_info[hotline]" value="%s" class="regular-text" /><p class="description">Số điện thoại cách nhau bởi dấu phẩy (,)</p>',
            isset( $this->options['hotline'] ) ? esc_attr( $this->options['hotline']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function title_callback()
    {
        printf(
            '<input type="text" id="title" name="company_info[title]" value="%s" />',
            isset( $this->options['title'] ) ? esc_attr( $this->options['title']) : ''
        );
    }
}

if( is_admin() ) {
    $my_settings_page = new CompanyInfoPage();
   }
