<?php
        /**
         * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
            return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "redux_company_info";

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        'menu_title'           => __( 'Thông tin công ty', 'redux-framework-demo' ),
        'page_title'           => __( 'Thông tin công ty', 'redux-framework-demo' )
    );

    Redux::setArgs( $opt_name, $args );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Thông tin Công ty', 'redux-framework-demo' ),
        'id'               => 'company-info',
        'desc'             => __( 'Thông tin chi tiết công ty', 'redux-framework-demo' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-home',
        'fields' => array(
            array(
                'id'       => 'hotline',
                'type'     => 'text',
                'title'    => __( 'Hotline', 'redux-framework-demo' ),
                'desc'     => __( 'Số điện thoại cách nhau bởi dấu phẩy', 'redux-framework-demo' ),
                'default'  => '1'// 1 = on | 0 = off
            ),
            array(
                'id'       => 'company_mst',
                'type'     => 'text',
                'title'    => __( 'Mã số thuế', 'redux-framework-demo' ),
                'default'  => ''// 1 = on | 0 = off
            ),
            array(
                'id'       => 'company_dang_ky_lan_dau',
                'type'     => 'text',
                'title'    => __( 'Đăng ký lần đầu', 'redux-framework-demo' ),
                'default'  => ''// 1 = on | 0 = off
            ),
            array(
                'id'       => 'company_dang_ky_lan_dau_lan_1',
                'type'     => 'text',
                'title'    => __( 'Đăng ký thay đổi lần thứ 1', 'redux-framework-demo' ),
                'default'  => ''// 1 = on | 0 = off
            ),
            array(
                'id'       => 'company_dai_dien',
                'type'     => 'text',
                'title'    => __( 'Đại diện', 'redux-framework-demo' ),
                'default'  => ''// 1 = on | 0 = off
            ),
            array(
                'id'       => 'company_dia_chi',
                'type'     => 'text',
                'title'    => __( 'Địa chỉ', 'redux-framework-demo' ),
                'default'  => ''// 1 = on | 0 = off
            ),
            array(
                'id'       => 'company_dien_thoai',
                'type'     => 'text',
                'title'    => __( 'Điện thoại', 'redux-framework-demo' ),
                'default'  => ''// 1 = on | 0 = off
            ),
            array(
                'id'       => 'company_fax',
                'type'     => 'text',
                'title'    => __( 'Fax', 'redux-framework-demo' ),
                'default'  => ''// 1 = on | 0 = off
            ),
            array(
                'id'       => 'company_email',
                'type'     => 'text',
                'title'    => __( 'Email', 'redux-framework-demo' ),
                'default'  => ''// 1 = on | 0 = off
            ),
            array(
                'id'       => 'company_website',
                'type'     => 'text',
                'title'    => __( 'Website', 'redux-framework-demo' ),
                'default'  => ''// 1 = on | 0 = off
            ),
            array(
                'id'       => 'facebook_page_url',
                'type'     => 'text',
                'title'    => __( 'Link trang Facebook', 'redux-framework-demo' ),
                'default'  => ''// 1 = on | 0 = off
            ),
            array(
                'id'       => 'facebook_page_name',
                'type'     => 'text',
                'title'    => __( 'Tên trang Facebook', 'redux-framework-demo' ),
                'default'  => ''// 1 = on | 0 = off
            ),
            array(
                'id'       => 'company_location',
                'type'     => 'textarea',
                'title'    => __( 'Địa chỉ công ty', 'redux-framework-demo' ),
                'desc'     => __( 'Lấy link chia sẻ địa chỉ từ Google Maps', 'redux-framework-demo' ),
                'default'  => ''// 1 = on | 0 = off
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Hình ảnh', 'redux-framework-demo' ),
        'id'               => 'company-partners',
        'desc'             => __( 'Hình ảnh sử dụng trong website', 'redux-framework-demo' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-home',
        'fields' => array(
            array(
                'id'       => 'company_partners',
                'type'     => 'gallery',
                'title'    => __( 'Nhà phân phối chính thức', 'redux-framework-demo' ),
                'desc'     => __( 'Logo của các thương hiệu', 'redux-framework-demo' ),
                'default'  => 0
            ),
            array(
                'id'       => 'fixed_image_home_banner',
                'type'     => 'media',
                'title'    => __( 'Hình cố định của home slide', 'redux-framework-demo' ),
                'default'  => 0
            ),
            array(
                'id'       => 'home_banner_images',
                'type'     => 'slides',
                'title'    => __( 'Hình của home slides', 'redux-framework-demo' ),
                'default'  => 0
            ),
        )
    ) );

