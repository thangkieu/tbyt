(function($) {
	'use strict';
	$(document).ready(function() {
		if (window.Swiper) {
			new Swiper('.js-swiper-container-partners', {
				autoplay: 2500,
				speed: 500,
				slidesPerView: 6,
				spaceBetween: 30,
				loop: true
			});
		}	
	});
	
}(jQuery));