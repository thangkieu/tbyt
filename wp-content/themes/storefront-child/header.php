<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<meta property="og:url"           content="<?php echo get_permalink(); ?>" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="<?php echo get_the_title(); ?>" />
<meta property="og:description"   content="<?php echo esc_attr( the_excerpt() ); ?>" />
<meta property="og:image"         content="<?php echo get_the_post_thumbnail_url(); ?>" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="page" class="hfeed site">
	<?php
	do_action( 'storefront_before_header' ); 
	# Get Hotline from setting
	
	global $redux_company_info;

	$hotline = $redux_company_info['hotline'] ? $redux_company_info['hotline'] : '';
	$hotlines = explode(',', $hotline);
	$length = count( $hotlines );
	$i = 0;
	?>
	<div class="hotline">
		<h3><span class="fa fa-phone"></span> Hotline</h3>
		
		<?php 
		foreach ($hotlines as $hotline) {
			echo '<p><a href="tel:' . $hotline . '">' . $hotline . '</a></p>';
			if ( ++$i < $length ) {
				echo '<hr>';
			}
		}
		 ?>
	</div>
	<header id="masthead" class="site-header" role="banner">
		<div class="header-inner">
			<div class="col-full">
				<div class="storefront-primary-navigation-wrapper">
				<?php 
				storefront_primary_navigation_wrapper();
				$logoId = get_theme_mod('custom_logo');

				if ( $logoId ) : ?>
				  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo('name'); ?>" class="nav-brand"><img class="site-logo" src="<?php echo esc_url( wp_get_attachment_image_url( $logoId, array(50, 50) ) ); ?>" alt="<?php bloginfo('name'); ?>" /></a>
				<?php endif;
				storefront_primary_navigation();
				storefront_product_search();
				storefront_header_cart();
				storefront_primary_navigation_wrapper_close();
				?>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
	
	<?php 
	# Home Slider
	if ( $redux_company_info['home_banner_images'] ) :
		$images = $redux_company_info['home_banner_images'];
		$fixed_image = $redux_company_info['fixed_image_home_banner'];
	?>
	<div class="top-banner">
		<div class="col-full">
			<img src="<?php echo $fixed_image['url']; ?>" alt="Top Banner">
			<div class="swiper-container js-home-swiper-container swiper-home-banner-container">
				<div class="swiper-wrapper">
				<?php foreach ($images as $image) { ?>
					<a href="<?php echo $image['url']; ?>" class="swiper-slide">
						<div class="img-wrap"><img src="<?php echo $image['image']; ?>" alt="<?php echo $image['title']; ?>" /></div>
					</a>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<script>
		(function() {
			'use strict';
			if (window.Swiper) {
				new Swiper('.js-home-swiper-container', {
					pagination: '.swiper-pagination',
					paginationClickable: true,
					autoplay: 2500,
					speed: 1000,
					effect: 'cube',
			        grabCursor: true,
			        cube: {
			            shadow: true,
			            slideShadows: true,
			            shadowOffset: 10,
			            shadowScale: 0.94
			        }
				});
			}
		}());
	</script>
	<?php endif; ?>
	<?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 */
	do_action( 'storefront_before_content' ); ?>

	<div id="content" class="site-content" tabindex="-1">
		<div class="col-full">

		<?php
		/**
		 * Functions hooked in to storefront_content_top
		 *
		 * @hooked woocommerce_breadcrumb - 10
		 */
		do_action( 'storefront_content_top' );
