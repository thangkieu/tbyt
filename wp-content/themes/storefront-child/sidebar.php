<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package storefront
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
global $redux_company_info;
?>

<div id="secondary" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
  <?php 
  # Get partners
  $id_string = $redux_company_info['company_partners'];
  $partners = explode(',', $id_string);

  if ( $id_string && !empty( $partners ) ) : ?>
  <div class="widget">
    <h3 class="widget-title">Đại lý chính thức</h3>
    <div class="partners-image">
      <?php foreach ($partners as $partner) { 
        $img = wp_get_attachment_image( $partner, 'full' );
        echo '<div class="img">' . $img . '</div>';
      } ?>
    </div>
  </div>
  <?php endif; ?>
</div><!-- #secondary -->
