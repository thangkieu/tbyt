<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Homepage Two columns
 *
 * @package storefront
 */

get_header(); ?>
	<?php do_action( 'storefront_sidebar' ); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?#php echo do_shortcode('[featured_products per_page="6"]'); ?>
			<?php storefront_recent_products( array( 'limit' => 4 )); ?>

			<?php get_template_part('templates/products', 'category'); ?>
			<?php
			/**
			 * Functions hooked in to homepage action
			 *
			 * @hooked storefront_homepage_content      - 10
			 * @hooked storefront_product_categories    - 20
			 * @hooked storefront_recent_products       - 30
			 * @hooked storefront_featured_products     - 40
			 * @hooked storefront_popular_products      - 50
			 * @hooked storefront_on_sale_products      - 60
			 * @hooked storefront_best_selling_products - 70
			 */
			// do_action( 'homepage', 'storefront_homepage_contact_section' ); 
			get_template_part('templates/recently', 'viewed');
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
