<?php
function add_view_more_recent_product() {
	echo '<a href="">View more</a>';
}

add_action('storefront_homepage_after_recent_products_title', 'add_view_more_recent_product');

wp_enqueue_style( "storefront-new-style", get_stylesheet_directory_uri() . '/style.css', array( 'storefront-style' ), wp_get_theme()->get('Version') );

// Remove homepage actions
remove_action( 'homepage', 'storefront_homepage_content',      10 );
remove_action( 'homepage', 'storefront_product_categories',    20 );
remove_action( 'homepage', 'storefront_recent_products',       30 );
remove_action( 'homepage', 'storefront_featured_products',     40 );
remove_action( 'homepage', 'storefront_popular_products',      50 );
remove_action( 'homepage', 'storefront_on_sale_products',      60 );
remove_action( 'homepage', 'storefront_best_selling_products', 70 );

?>