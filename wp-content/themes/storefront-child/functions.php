<?php
include 'includes/recently-post.php';
include 'includes/widget-pages.php';
include 'includes/woocommerce.php';
require_once( 'includes/company-info.php' );

function add_view_more_recent_product() {
	echo '<a href="' . get_permalink( get_page_by_path('shop') ) .'" class="view-more">Xem tất cả</a>';
}

add_action('storefront_homepage_after_recent_products_title', 'add_view_more_recent_product');

// wp_enqueue_style( "storefront-new-style", get_stylesheet_directory_uri() . '/style.css', array( 'storefront-style' ), wp_get_theme()->get('Version') );


function enqueue_media_uploader()
{
    wp_enqueue_media();
}

add_action("admin_enqueue_scripts", "enqueue_media_uploader");

wp_enqueue_script( 
	'home', 
	get_stylesheet_directory_uri() . '/assets/front/js/home.js', 
	array( 'jquery'),
	wp_get_theme()->get( 'Version' ) ,
	true
);

wp_enqueue_script( 
	'swiper_instace', 
	get_stylesheet_directory_uri() . '/assets/front/js/swiper.min.js', 
	wp_get_theme()->get( 'Version' ) 
);

wp_enqueue_style( 
	'swiper_instace_style', 
	get_stylesheet_directory_uri() . '/assets/front/css/swiper.min.css', 
	wp_get_theme()->get( 'Version' ) 
);

add_filter( 'woocommerce_get_breadcrumb', '__return_false' );
add_filter( 'storefront_menu_toggle_text', '' );
add_filter('login_errors',create_function('$a', "return null;"));