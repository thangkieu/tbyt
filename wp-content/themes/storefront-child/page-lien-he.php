<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package storefront
 */

get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

      <form action="" method="post" class="contact-form">
        <p>
          <label for="g34-name" class="grunion-field-label name">Name<span>(yêu cầu)</span></label>
          <input type="text" name="g34-name" id="g34-name" value="" class="name" required="" aria-required="true">
        </p>
        
        <p>
          <label for="g34-email" class="grunion-field-label email">Email<span>(yêu cầu)</span></label>
          <input type="email" name="g34-email" id="g34-email" value="" class="email" required="" aria-required="true">
        </p>
        
        <p>
          <label for="contact-form-comment-g34-phnhi" class="grunion-field-label textarea">Phản hồi<span>(yêu cầu)</span></<label for=" "></label>>
          <textarea name="g34-phnhi" id="contact-form-comment-g34-phnhi" rows="20" class="textarea" required="" aria-required=" true"></textarea>
        </p>
        <p class="contact-submit">
          <input type="submit" value="Gửi »" class="pushbutton-wide">
          <input type="hidden" id="_wpnonce" name="_wpnonce" value="ca75ee6a46"><input type="hidden" name="_wp_http_referer"  value="/tbyt/lien-he/">
          <input type="hidden" name="contact-form-id" value="34">
          <input type="hidden" name="action" value="grunion-contact-form">
        </p>
      </form>

      <?php the_content(); ?>
    </main><!-- #main -->
  </div><!-- #primary -->

<?php
do_action( 'storefront_sidebar' );
get_footer();
