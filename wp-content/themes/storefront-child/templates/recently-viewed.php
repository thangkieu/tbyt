<section class="storefront-product-section storefront-product-recently-viewed" aria-label="Product Recently Viewed">
	<div class="woocommerce columns-<?php $columns ?>">
		<h2 class="section-title"><?php echo __( 'Recently Viewed Products', 'woocommerce' ); ?></h2>
		<a href="<?php $term_link; ?>"><?php _e( 'View more', 'storefront' ); ?></a>
		<?php echo the_widget( 'WC_Widget_Recently_Viewed' ); ?>
	</div>
</section>