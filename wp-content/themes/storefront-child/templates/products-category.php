<?php 
$columns                     = 4;
$args = array(
	'parent' => 0
);

$categories = get_terms( 'product_cat', $args );

foreach ($categories as $category) {
	if($category->category_parent == 0) :
	   	$category_slug = $category->slug;
	   	$term_link = get_term_link( $category );
		
		$args = array( 
			'post_type' => 'product', 
			'posts_per_page' => 4, 
			'product_cat' => $category_slug, 
			'orderby' => 'date' 
		);

		$products = new WP_Query( $args );
		
		// ob_start();
		echo '<section class="storefront-product-section storefront-product-categories" aria-label="Product Categories">';
		echo '<div class="woocommerce columns-' . $columns . '">';
		echo '<h2 class="section-title">' . $category->name . '</h2>';
		echo '<a href="' . $term_link . '">' . __( 'View more', 'storefront' ) . '</a>';

		echo woocommerce_product_loop_start();
		
		if ( $products->have_posts() ) :
			while ( $products->have_posts() ) : $products->the_post();

				echo wc_get_template_part( 'content', 'product' );

			endwhile; // end of the loop.
		endif;

		echo woocommerce_product_loop_end();
		echo '</div>';
		echo '</section>';

		woocommerce_reset_loop();
		// echo '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean() . '</div>';
		
	endif;
}

wp_reset_postdata();