<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */
global $redux_company_info;
?>

		</div><!-- .col-full -->
	</div><!-- #content -->
	<?#php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full">
			
			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			// do_action( 'storefront_footer' ); ?>
			<div class="site-info">
				<ul class="info-list">
					<?php 
						$info = array(
							array(
								'label' => 'MST',
								'value' => $redux_company_info['company_mst']
							),
							array(
								'label' => 'Đăng ký lần đầu',
								'value' => $redux_company_info['company_dang_ky_lan_dau']
							),
							array(
								'label' => 'Đăng ký thay đổi lần thứ 1',
								'value' => $redux_company_info['company_dang_ky_lan_dau_lan_1']
							),
							array(
								'label' => 'Đại diện',
								'value' => $redux_company_info['company_dai_dien']
							),
							array(
								'label' => 'Địa chỉ',
								'value' => $redux_company_info['company_dia_chi']
							),
							array(
								'label' => 'Điện thoại',
								'value' => $redux_company_info['company_dien_thoai']
							),
							array(
								'label' => 'Fax',
								'value' => $redux_company_info['company_fax']
							)
						);
						foreach ($info as $item) {
							if ($item['value']) { ?>
								<li><strong><?php echo $item['label'] ?>:</strong> <?php echo $item['value']; ?></li>
							<?php }
						}
					?>
					<?php 
					if ( $redux_company_info['company_email'] ) { ?>
					 	<li><strong>Email:</strong> <a href="mailto:<?php echo $redux_company_info['company_email']; ?>"><?php echo $redux_company_info['company_email']; ?></a></li>
					<?php }
					if ( $redux_company_info['company_website'] ) { ?>
						<li><strong>Website:</strong> <a href="<?php echo $redux_company_info['company_website']; ?>"><?php echo $redux_company_info['company_website']; ?></a></li>
					<?php } ?>
				</ul>
			</div>
			<div class="facebook-page">
				<div class="fb-page" data-href="<?php echo $redux_company_info['facebook_page_url'] ?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?php echo $redux_company_info['facebook_page_url'] ?>" class="fb-xfbml-parse-ignore"><a href="<?php echo $redux_company_info['facebook_page_url'] ?>"><?php echo $redux_company_info['facebook_page_name'] ?></a></blockquote></div>
			</div>

		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
