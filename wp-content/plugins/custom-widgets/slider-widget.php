<?php
	// Creating the widget 
	class Slider_Widget extends WP_Widget {

		function __construct() {
			

			parent::__construct(
				// Base ID of your widget
				'slider_widget', 

				// Widget name will appear in UI
				__( 'Slider', 'storefront' ), 

				// Widget description
				array( 'description' => __( 'Widget show slider images', 'storefront' ), ) 
			);
		}

		// Creating widget front-end
		// This is where the action happens
		public function widget( $args, $instance ) {
			// if ( is_home() || is_front_page() ) {
				// $title = apply_filters( 'widget_title', $instance['title'] );
				// // before and after widget arguments are defined by themes
				// echo $args['before_widget'];
				// if ( ! empty( $title ) )
				// echo $args['before_title'] . $title . $args['after_title'];

				// This is where you run the code and display the output
				if ( isset( $instance[ 'pages' ] ) ) {
					$pagesValue = $instance[ 'pages' ];
				}
				else {
					$pagesValue = '';
				}

				$ids = explode(' ', $pagesValue);
				foreach ($ids as $id) {
					$selected_pages[] = array(
						'ID' => $id,
						'src' => wp_get_attachment_image( $id, array( 'full', '500' ) )
					);
				}

				?> 
				<div class="swiper-container swiper-home-banner-container">
					<div class="swiper-wrapper">
					<?php foreach ($selected_pages as $selected_page) { ?>
						<div class="swiper-slide">
							<?php echo $selected_page['src']; ?>
						</div>
					<?php } ?>
					</div>
					<div class="swiper-pagination"></div>
				</div>
				<script>
					(function() {
						'use strict';
						if (window.Swiper) {
							new Swiper('.swiper-container', {
								pagination: '.swiper-pagination',
								paginationClickable: true,
								autoplay: 2500,
								speed: 500
							});
						}
					}());
				</script>
				<?php
				
				echo $args['after_widget'];
			// }
		}
				
		// Widget Backend 
		public function form( $instance ) {
			if ( isset( $instance[ 'title' ] ) ) {
				$title = $instance[ 'title' ];
			}
			else {
				$title = __( 'New title', 'storefront' );
			}
			if ( isset( $instance[ 'pages' ] ) ) {
				$pagesValue = $instance[ 'pages' ];
			}
			else {
				$pagesValue = '';
			}
			// Widget admin form
			// var_dump( $pagesValue );
			if ( $pagesValue != '' ) :
				// $selected_pages = get_pages( array( 'include' => $pagesValue ) );
				$ids = explode(' ', $pagesValue);
				foreach ($ids as $id) {
					$selected_pages[] = array(
						'ID' => $id,
						'src' => wp_get_attachment_image( $id, array( '50', '50' ) )
					);
				}
			endif;
				$unselected_pages = get_pages( array( 'exclude' => $pagesValue ) );
			?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>">
					<?php _e( 'Title:' ); ?>
				</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			
			<div>
				<style>
					.image-item {
						width: 50px; 
						height: 50px; 
						border-radius: 5px;background-size: contain; 
						background-position: center; 
						background-repeat: no-repeat; 
						border: 1px solid #ddd; 
						position: relative; 
						float: left; 
						margin-right: 7px;
					}

					.image-item img {
						border-radius: 5px;
					}

					.image-delete {
						display: block; 
						position: absolute; 
						top: -10px; 
						right: -10px; 
						background-color: #fff; 
						border-radius: 50%; 
						cursor: pointer;
					}
				</style>
				
				<label for=""><?php _e( 'Images:' ) ?></label>
				<ul id="<?php echo $this->get_field_id( 'pages-list' ); ?>">
					<?php if ( !empty( $selected_pages ) ) :?>
					<?php
					foreach ($selected_pages as $selected_page) { ?>
						<li class="image-item">
							<?php echo $selected_page['src']; ?>
							<span class="js-remove-selected-page dashicons dashicons-dismiss image-delete" data-id="<?php echo $selected_page['ID']; ?>"></span>
						</li>
					<?php } ?>
					<?php endif; ?>
				</ul>
				<div style="clear: both;"></div>
				
			</div>
			
			<p>
				<input id="<?php echo $this->get_field_id( 'pages' ); ?>" name="<?php echo $this->get_field_name( 'pages' ); ?>" type="hidden" value="<?php echo $pagesValue; ?>" />
				<button type="button" class="button button secondary" id="<?php echo $this->get_field_id( 'add-pages-link' ); ?>"><?php _e( 'Add Image', 'storefront' ) ?></button>
				<script>
					(function() {
						'use strict';
						var media_uploader = null;
						var SEPARATE = ' ';
						var linkId = '#<?php echo $this->get_field_id( 'add-pages-link' ); ?>';
						var valueId = '#<?php echo $this->get_field_id( 'pages' ); ?>';
						var selectId = '#<?php echo $this->get_field_id( 'add-pages' ); ?>';
						var pageListId = '#<?php echo $this->get_field_id( 'pages-list' ); ?>';

						function open_media_uploader_multiple_images()
						{
							media_uploader = wp.media({
								frame:    'post', 
								state:    'insert', 
								multiple: true 
							});

							media_uploader.on('insert', function(){

								var length = media_uploader.state().get('selection').length;
								var images = media_uploader.state().get('selection').models

								for(var iii = 0; iii < length; iii++)
								{
									var image_url = images[iii].changed.url;
									var image_caption = images[iii].changed.caption;
									var image_title = images[iii].changed.title;

									console.log('image', image_url, image_caption, image_title);
									var $select = jQuery(selectId);
									var currValue = jQuery(valueId).val();
									var currValueArray = currValue.split(SEPARATE);

									var newValue = images[iii].id;
								
									if (!newValue || currValueArray.indexOf(newValue.toString()) >= 0) {
										return;
									}

									if (currValue) {
										currValue += SEPARATE + newValue;
									} else {
										currValue = newValue;
									}

									jQuery(valueId).val(currValue);

									jQuery(pageListId).append(jQuery('<li class="image-item" style="background-image: url(' + image_url + ');"><span data-id="' + newValue + '" class="dashicons dashicons-dismiss js-remove-selected-page image-delete"></span></li>'));
								}
							});

							media_uploader.open();
						}

						window.open_media_uploader_multiple_images = open_media_uploader_multiple_images;

						jQuery(document)
						.on('click', linkId, function(e) {
							e.stopPropagation();
							e.preventDefault();
							e.stopImmediatePropagation();
							window.open_media_uploader_multiple_images();

							/*var $select = jQuery(selectId);
							var currValue = jQuery(valueId).val();
							var currValueArray = currValue.split(SEPARATE);

							var newValue = $select.val();
						
							if (!newValue || currValueArray.indexOf(newValue) >= 0) {
								return;
							}

							if (currValue) {
								currValue += SEPARATE + newValue;
							} else {
								currValue = newValue;
							}

							jQuery(valueId).val(currValue);
							
							var li = document.createElement('li');
							li.setAttribute('data-id', newValue);
							li.innerText = $select.find('option:selected').text();

							jQuery(pageListId).append(li);*/
						})
						.on('click', '.js-remove-selected-page', function() {
							var $valueEl = jQuery(valueId);
							
							var id = jQuery(this).data('id');
							var page_list = $valueEl.val();

							var str_removed_id = page_list.replace( id, '' );
							var new_page_list = str_removed_id.replace( SEPARATE + SEPARATE, SEPARATE );

							$valueEl.val( new_page_list.trim() );
							// remove this row
							jQuery(this).closest('li').remove();
						});
					}());
				</script>
			</p>
		   <?php 
		}
			
		// Updating widget replacing old instances with new
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			$instance['pages'] = ( ! empty( $new_instance['pages'] ) ) ? strip_tags( $new_instance['pages'] ) : '';

			return $instance;
		}
	}
?>
