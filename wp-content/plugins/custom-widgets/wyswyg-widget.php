<?php
	// Creating the widget 
	class WYSWYG_Widget extends WP_Widget {

		function __construct() {
			parent::__construct(
				// Base ID of your widget
				'wyswyg_widget', 

				// Widget name will appear in UI
				__('Extra Text', 'storefront'), 

				// Widget description
				array( 'description' => __( 'Create text editor', 'storefront' ), ) 
			);
		}

		// Creating widget front-end
		// This is where the action happens
		public function widget( $args, $instance ) {
			$title = apply_filters( 'widget_title', $instance['title'] );
			// before and after widget arguments are defined by themes
			echo $args['before_widget'];
			if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

			// This is where you run the code and display the output
			if ( isset( $instance[ 'content' ] ) ) {
				$content = $instance[ 'content' ];
			}
			?> 
			<div><?php echo $content; ?></div>
			<?php
			
			echo $args['after_widget'];
		}
				
		// Widget Backend 
		public function form( $instance ) {
			if ( isset( $instance[ 'title' ] ) ) {
				$title = $instance[ 'title' ];
			}
			else {
				$title = __( 'New title', 'storefront' );
			}
			if ( isset( $instance[ 'content' ] ) ) {
				$content = $instance[ 'content' ];
				var_dump( $content );

			}
			// Widget admin form
			?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>">
					<?php _e( 'Title:' ); ?>
				</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'content' ); ?>"><?php _e( 'Content:' ) ?></label>
				<!-- <textarea name="<?php echo $this->get_field_name( 'content' ); ?>" id="<?php echo $this->get_field_id( 'content' ); ?>" cols="30" rows="10" value="<?php echo $content; ?>"></textarea> -->
				<?php 
				$name = $this->get_field_name( 'content' );

				$wp_editor_content   = $content;
				$editor_id = $this->get_field_id( 'content' );
				 
				wp_editor( $wp_editor_content, $editor_id, array( 
					'media_buttons' => false,
					'textarea_name' => $name
				) ); 
				?>
				<script>
					(function() {
						'use strict';
						jQuery(document).ready(function() {
							if (window.tinyMCE) {
								console.log('inited');
								var id = '<?php echo $editor_id; ?>';
								var editor = tinyMCE.get(id);
								console.log('editor', id, editor);
								
								if (tinyMCE.activeEditor) {
									tinyMCE.activeEditor.on('change', function(b) {
										console.log(b);
									});
								}
							}
					});
					}());
				</script>
			</p>
		   <?php 
		}
			
		// Updating widget replacing old instances with new
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			$instance['content'] = ( ! empty( $new_instance['content'] ) ) ? strip_tags( $new_instance['content'] ) : '';

			return $instance;
		}
	}
?>
