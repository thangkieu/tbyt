<?php
	/*
	Plugin Name: Custom Widgets
	Description: Custom widget
	*/
	include 'policy-widget.php';
	include 'slider-widget.php';
	include 'wyswyg-widget.php';

	// Register and load the widget
	function wpb_load_widget() {
		register_widget( 'Policy_Widget' );
		register_widget( 'Slider_Widget' );
		register_widget( 'WYSWYG_Widget' );
	}
	add_action( 'widgets_init', 'wpb_load_widget' );
?>
