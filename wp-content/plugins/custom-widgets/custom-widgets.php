<?php
	/*
	Plugin Name: Custom Widgets
	Description: Custom widget
	*/
	/* Start Adding Functions Below this Line */


	/* Stop Adding Functions Below this Line */

	// Creating the widget 
	class policy_widget extends WP_Widget {

		function __construct() {
			parent::__construct(
				// Base ID of your widget
				'policy_widget', 

				// Widget name will appear in UI
				__('Policy', 'storefront'), 

				// Widget description
				array( 'description' => __( 'Select your pages to add to Policy widget', 'storefront' ), ) 
			);
		}

		// Creating widget front-end
		// This is where the action happens
		public function widget( $args, $instance ) {
			$title = apply_filters( 'widget_title', $instance['title'] );
			// before and after widget arguments are defined by themes
			echo $args['before_widget'];
			if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

			// This is where you run the code and display the output
			if ( isset( $instance[ 'pages' ] ) ) {
				$pagesValue = $instance[ 'pages' ];
			}
			else {
				$pagesValue = '';
			}

			$selected_pages = get_pages( array( 'include' => $pagesValue ) );
			?> 
			<ul>
				<?php foreach ($selected_pages as $selected_page) { ?>
					<li>
						<a href="<?php echo get_page_link( $selected_page ); ?>"><?php echo $selected_page->post_title; ?></a>
					</li>
				<?php } ?>
			</ul>
			<?php
			
			echo $args['after_widget'];
		}
				
		// Widget Backend 
		public function form( $instance ) {
			if ( isset( $instance[ 'title' ] ) ) {
				$title = $instance[ 'title' ];
			}
			else {
				$title = __( 'New title', 'storefront' );
			}
			if ( isset( $instance[ 'pages' ] ) ) {
				$pagesValue = $instance[ 'pages' ];
			}
			else {
				$pagesValue = '';
			}
			// Widget admin form
			// var_dump( $pagesValue );
			if ( $pagesValue != '' ) :
				$selected_pages = get_pages( array( 'include' => $pagesValue ) );
			endif;
			
			$unselected_pages = get_pages( array( 'exclude' => $pagesValue ) );
			?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>">
					<?php _e( 'Title:' ); ?>
				</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<?php if ( !empty( $selected_page ) ) : ?>
			<p>
				<label for=""><?php _e( 'Pages:' ) ?></label>
				<ul id="<?php echo $this->get_field_id( 'pages-list' ); ?>">
					<?php
					foreach ($selected_pages as $selected_page) { ?>
						<li>
							<span><?php echo $selected_page->post_title; ?></span>
							<a class="js-remove-selected-page" data-id="<?php echo $selected_page->ID; ?>" style="float: right; color: #a00;" href="javascript:;"><?php _e( 'Delete' ); ?></a>
						</li>
					<?php } ?>
				</ul>
			</p>
			<?php endif; ?>
			<p>
				<input id="<?php echo $this->get_field_id( 'pages' ); ?>" name="<?php echo $this->get_field_name( 'pages' ); ?>" type="hidden" value="<?php echo $pagesValue; ?>" />
				<label for="<?php echo $this->get_field_id( 'add-pages' ); ?>"><?php _e( 'Add Pages:' ) ?></label>
				<select name="" id="<?php echo $this->get_field_id( 'add-pages' ); ?>" class="widefat">
					<option value="">--<?php echo __( 'Select page'); ?>--</option>
					<?php foreach ($unselected_pages as $page) { ?>
						<option value="<?php echo $page->ID; ?>"><?php echo $page->post_title; ?></option>
					<?php } ?>
				</select>
				<a href="javascript:;" id="<?php echo $this->get_field_id( 'add-pages-link' ); ?>"><?php _e( 'Add', 'storefront' ) ?></a>
				<script>
					(function() {
						'use strict';
						var SEPARATE = ' ';
						var linkId = '#<?php echo $this->get_field_id( 'add-pages-link' ); ?>';
						var valueId = '#<?php echo $this->get_field_id( 'pages' ); ?>';
						var selectId = '#<?php echo $this->get_field_id( 'add-pages' ); ?>';
						var pageListId = '#<?php echo $this->get_field_id( 'pages-list' ); ?>';

						jQuery(document)
						.on('click', linkId, function(e) {
							e.stopPropagation();
							e.preventDefault();
							e.stopImmediatePropagation();
							
							var $select = jQuery(selectId);
							var currValue = jQuery(valueId).val();
							var currValueArray = currValue.split(SEPARATE);

							var newValue = $select.val();
						
							if (!newValue || currValueArray.indexOf(newValue) >= 0) {
								return;
							}

							if (currValue) {
								currValue += SEPARATE + newValue;
							} else {
								currValue = newValue;
							}

							jQuery(valueId).val(currValue);
							
							var li = document.createElement('li');
							li.setAttribute('data-id', newValue);
							li.innerText = $select.find('option:selected').text();

							jQuery(pageListId).append(li);
						})
						.on('click', '.js-remove-selected-page', function() {
							var $valueEl = jQuery(valueId);
							
							var id = jQuery(this).data('id');
							var page_list = $valueEl.val();

							var str_removed_id = page_list.replace( id, '' );
							var new_page_list = str_removed_id.replace( SEPARATE + SEPARATE, SEPARATE );

							$valueEl.val( new_page_list.trim() );
							// remove this row
							jQuery(this).closest('li').remove();
						});
					}());
				</script>
			</p>
		   <?php 
		}
			
		// Updating widget replacing old instances with new
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			$instance['pages'] = ( ! empty( $new_instance['pages'] ) ) ? strip_tags( $new_instance['pages'] ) : '';

			return $instance;
		}
	} // Class policy_widget ends here

	// Register and load the widget
	function wpb_load_widget() {
		register_widget( 'policy_widget' );
	}
	add_action( 'widgets_init', 'wpb_load_widget' );

?>
